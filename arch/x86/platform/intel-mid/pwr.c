// SPDX-License-Identifier: GPL-2.0-only
/*
 * Intel MID Power Management Unit (PWRMU) device driver
 *
 * Copyright (C) 2016, Intel Corporation
 *
 * Author: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
 *
 * Intel MID Power Management Unit device driver handles the South Complex PCI
 * devices such as GPDMA, SPI, I2C, PWM, and so on. By default PCI core
 * modifies bits in PMCSR register in the PCI configuration space. This is not
 * enough on some SoCs like Intel Tangier. In such case PCI core sets a new
 * power state of the device in question through a PM hook registered in struct
 * pci_platform_pm_ops (see drivers/pci/pci-mid.c).
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/mutex.h>
#include <linux/pci.h>
#include <asm/apic.h>

#include <asm/intel-mid.h>
#include <linux/intel_mid_pm.h>

/* Registers */
#define PM_STS			0x00
#define PM_CMD			0x04
#define PM_ICS			0x08
#define PM_WKC(x)		(0x10 + (x) * 4)
#define PM_WKS(x)		(0x18 + (x) * 4)
#define PM_SSC(x)		(0x20 + (x) * 4)
#define PM_SSS(x)		(0x30 + (x) * 4)
#define PM_WSSC(x)		(0x40 + (x) * 4)
#define PM_MSIC			0x58

/* Bits in PM_STS */
#define PM_STS_BUSY		(1 << 8)

/* Bits in PM_CMD */
#define PM_CMD_CMD(x)		((x) << 0)
#define PM_CMD_IOC		(1 << 8)
#define PM_CMD_CM_NOP		(0 << 9)
#define PM_CMD_CM_IMMEDIATE	(1 << 9)
#define PM_CMD_CM_DELAY		(2 << 9)
#define PM_CMD_CM_TRIGGER	(3 << 9)

/* System states */
#define PM_CMD_SYS_STATE_S5	(5 << 16)

/* Trigger variants */
#define PM_CMD_CFG_TRIGGER_NC	(3 << 19)

/* Message to wait for TRIGGER_NC case */
#define TRIGGER_NC_MSG_2	(2 << 22)

/* List of commands */
#define CMD_SET_CFG		0x01

/* Bits in PM_ICS */
#define PM_ICS_INT_STATUS(x)	((x) & 0xff)
#define PM_ICS_IE		(1 << 8)
#define PM_ICS_IP		(1 << 9)
#define PM_ICS_SW_INT_STS	(1 << 10)

/* List of interrupts */
#define INT_INVALID		0
#define INT_CMD_COMPLETE	1
#define INT_CMD_ERR		2
#define INT_WAKE_EVENT		3
#define INT_LSS_POWER_ERR	4
#define INT_S0iX_MSG_ERR	5
#define INT_NO_C6		6
#define INT_TRIGGER_ERR		7
#define INT_INACTIVITY		8

/* South Complex devices */
#define LSS_MAX_SHARED_DEVS	4
#define LSS_MAX_DEVS		64

#define LSS_WS_BITS		1	/* wake state width */
#define LSS_PWS_BITS		2	/* power state width */

/* Supported device IDs */
#define PCI_DEVICE_ID_PENWELL	0x0828
#define PCI_DEVICE_ID_TANGIER	0x11a1

struct mid_pwr_dev {
	struct pci_dev *pdev;
	pci_power_t state;
};

enum sys_state {
	SYS_STATE_S0I0,
	SYS_STATE_S0I1,
	SYS_STATE_S0I2,
	SYS_STATE_S0I3,
	SYS_STATE_S3,
	SYS_STATE_S5,
	SYS_STATE_MAX
};

struct mid_pwr {
	struct device *dev;
	void __iomem *regs;
	int irq;
	bool available;

	struct mutex lock;
	struct mid_pwr_dev lss[LSS_MAX_DEVS][LSS_MAX_SHARED_DEVS];

	bool suspend_started;
	bool shutdown_started;
	bool camera_off;
	bool display_off;

	int s0ix_possible;
	int s0ix_entered;

	enum sys_state  pmu_current_state;
	u32 __iomem *offload_reg;

	u32 apm_base;
	u32 ospm_base;
};

static struct mid_pwr *midpwr;

static u32 mid_pwr_get_state(struct mid_pwr *pwr, int reg)
{
	return readl(pwr->regs + PM_SSS(reg));
}

static void mid_pwr_set_state(struct mid_pwr *pwr, int reg, u32 value)
{
	writel(value, pwr->regs + PM_SSC(reg));
}

static void mid_pwr_set_wake(struct mid_pwr *pwr, int reg, u32 value)
{
	writel(value, pwr->regs + PM_WKC(reg));
}

static void mid_pwr_set_wakeup_state(struct mid_pwr *pwr, int reg, u32 value)
{
	writel(value, pwr->regs + PM_WSSC(reg));
}

static void mid_pwr_interrupt_disable(struct mid_pwr *pwr)
{
	writel(~PM_ICS_IE, pwr->regs + PM_ICS);
}

static bool mid_pwr_is_busy(struct mid_pwr *pwr)
{
	return !!(readl(pwr->regs + PM_STS) & PM_STS_BUSY);
}

/* Wait 500ms that the latest PWRMU command finished */
static int mid_pwr_wait(struct mid_pwr *pwr)
{
	unsigned int count = 500000;
	bool busy;

	do {
		busy = mid_pwr_is_busy(pwr);
		if (!busy)
			return 0;
		udelay(1);
	} while (--count);

	return -EBUSY;
}

static int mid_pwr_wait_for_cmd(struct mid_pwr *pwr, u8 cmd)
{
	writel(PM_CMD_CMD(cmd) | PM_CMD_CM_IMMEDIATE, pwr->regs + PM_CMD);
	return mid_pwr_wait(pwr);
}

static int __update_power_state(struct mid_pwr *pwr, int reg, int bit, int new)
{
	int curstate;
	u32 power;
	int ret;

	/* Check if the device is already in desired state */
	power = mid_pwr_get_state(pwr, reg);
	curstate = (power >> bit) & 3;
	if (curstate == new)
		return 0;

	/* Update the power state */
	mid_pwr_set_state(pwr, reg, (power & ~(3 << bit)) | (new << bit));

	/* Send command to SCU */
	ret = mid_pwr_wait_for_cmd(pwr, CMD_SET_CFG);
	if (ret)
		return ret;

	/* Check if the device is already in desired state */
	power = mid_pwr_get_state(pwr, reg);
	curstate = (power >> bit) & 3;
	if (curstate != new)
		return -EAGAIN;

	return 0;
}

static pci_power_t __find_weakest_power_state(struct mid_pwr_dev *lss,
					      struct pci_dev *pdev,
					      pci_power_t state)
{
	pci_power_t weakest = PCI_D3hot;
	unsigned int j;

	/* Find device in cache or first free cell */
	for (j = 0; j < LSS_MAX_SHARED_DEVS; j++) {
		if (lss[j].pdev == pdev || !lss[j].pdev)
			break;
	}

	/* Store the desired state in cache */
	if (j < LSS_MAX_SHARED_DEVS) {
		lss[j].pdev = pdev;
		lss[j].state = state;
	} else {
		dev_WARN(&pdev->dev, "No room for device in PWRMU LSS cache\n");
		weakest = state;
	}

	/* Find the power state we may use */
	for (j = 0; j < LSS_MAX_SHARED_DEVS; j++) {
		if (lss[j].state < weakest)
			weakest = lss[j].state;
	}

	return weakest;
}

#define D0I0_MASK		0
#define D0I1_MASK		1
#define D0I2_MASK		2
#define D0I3_MASK		3

#define PMU_SDIO0_LSS_00		0
#define PMU_EMMC0_LSS_01		1
#define PMU_AONT_LSS_02			2
#define PMU_HSI_LSS_03			3
#define PMU_SECURITY_LSS_04		4
#define PMU_EMMC1_LSS_05		5
#define PMU_USB_OTG_LSS_06		6
#define PMU_USB_HSIC_LSS_07		7
#define PMU_AUDIO_ENGINE_LSS_08		8
#define PMU_AUDIO_DMA_LSS_09		9
#define PMU_SRAM_LSS_10			10
#define PMU_SRAM_LSS_11			11
#define PMU_SRAM_LSS_12			12
#define PMU_SRAM_LSS_13			13
#define PMU_SDIO2_LSS_14		14
#define PMU_PTI_DAFCA_LSS_15		15
#define PMU_SC_DMA_LSS_16		16
#define PMU_SPIO_LSS_17			17
#define PMU_SPI1_LSS_18			18
#define PMU_SPI2_LSS_19			19
#define PMU_I2C0_LSS_20			20
#define PMU_I2C1_LSS_21			21
#define PMU_MAIN_FABRIC_LSS_22		22
#define PMU_SEC_FABRIC_LSS_23		23
#define PMU_SC_FABRIC_LSS_24		24
#define PMU_AUDIO_RAM_LSS_25		25
#define PMU_SCU_ROM_LSS_26		26
#define PMU_I2C2_LSS_27			27
#define PMU_SSC_LSS_28			28
#define PMU_SECURITY_LSS_29		29
#define PMU_SDIO1_LSS_30		30
#define PMU_SCU_RAM0_LSS_31		31
#define PMU_SCU_RAM1_LSS_32		32
#define PMU_I2C3_LSS_33			33
#define PMU_I2C4_LSS_34			34
#define PMU_I2C5_LSS_35			35
#define PMU_SPI3_LSS_36			36
#define PMU_GPIO1_LSS_37		37
#define PMU_PWR_BUTTON_LSS_38		38
#define PMU_GPIO0_LSS_39		39
#define PMU_KEYBRD_LSS_40		40
#define PMU_UART2_LSS_41		41
#define PMU_ADC_LSS_42			42
#define PMU_CHARGER_LSS_43		43
#define PMU_SEC_TAPC_LSS_44		44
#define PMU_RTC_LSS_45			45
#define PMU_GPI_LSS_46			46
#define PMU_HDMI_VREG_LSS_47		47
#define PMU_RESERVED_LSS_48		48
#define PMU_AUDIO_SLIM1_LSS_49		49
#define PMU_RESET_LSS_50		50
#define PMU_AUDIO_SSP0_LSS_51		51
#define PMU_AUDIO_SSP1_LSS_52		52
#define PMU_IOSF_OCP_BRG_LSS_53		53
#define PMU_GP_DMA_LSS_54		54
#define PMU_SVID_LSS_55			55
#define PMU_SOC_FUSE_LSS_56		56
#define PMU_RSVD3_LSS_57		57
#define PMU_RSVD4_LSS_58		58
#define PMU_RSVD5_LSS_59		59
#define PMU_RSVD6_LSS_60		60
#define PMU_RSVD7_LSS_61		61
#define PMU_RSVD8_LSS_62		62
#define PMU_RSVD9_LSS_63		63

#define SSMSK(mask, lss) ((mask) << ((lss) * 2))

#define S0IX_TARGET_SSS0_MASK ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I3_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I3_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_ENGINE_LSS_08) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_DMA_LSS_09) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define S0IX_TARGET_SSS1_MASK ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))
#define S0IX_TARGET_SSS2_MASK ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I3_MASK, PMU_UART2_LSS_41-32))

#define S0IX_TARGET_SSS3_MASK ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48))

#define S0IX_TARGET_SSS0 ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I2_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I1_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_ENGINE_LSS_08) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_DMA_LSS_09) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define S0IX_TARGET_SSS1 ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))

#define S0IX_TARGET_SSS2 ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I1_MASK, PMU_UART2_LSS_41-32))

#define S0IX_TARGET_SSS3 ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48))

#define LPMP3_TARGET_SSS0_MASK ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I3_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I3_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_ENGINE_LSS_08) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define LPMP3_TARGET_SSS1_MASK ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))

#define LPMP3_TARGET_SSS2_MASK ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I3_MASK, PMU_UART2_LSS_41-32))

#define LPMP3_TARGET_SSS3_MASK ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48))

#define LPMP3_TARGET_SSS0 ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I2_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I1_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I0_MASK, PMU_AUDIO_ENGINE_LSS_08) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define LPMP3_TARGET_SSS1 ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))

#define LPMP3_TARGET_SSS2 ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I1_MASK, PMU_UART2_LSS_41-32))

#define LPMP3_TARGET_SSS3 ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48))

static bool check_s0ix_possible(void)
{
	if (((mid_pwr_get_state(midpwr, 0) & S0IX_TARGET_SSS0_MASK) ==
					S0IX_TARGET_SSS0) &&
		((mid_pwr_get_state(midpwr, 1) & S0IX_TARGET_SSS1_MASK) ==
					S0IX_TARGET_SSS1) &&
		((mid_pwr_get_state(midpwr, 2) & S0IX_TARGET_SSS2_MASK) ==
					S0IX_TARGET_SSS2) &&
		((mid_pwr_get_state(midpwr, 3) & S0IX_TARGET_SSS3_MASK) ==
					S0IX_TARGET_SSS3))
		return true;

	return false;
}

static bool check_lpmp3_possible(void)
{
	if (((mid_pwr_get_state(midpwr, 0) & LPMP3_TARGET_SSS0_MASK) ==
					LPMP3_TARGET_SSS0) &&
		((mid_pwr_get_state(midpwr, 1) & LPMP3_TARGET_SSS1_MASK) ==
					LPMP3_TARGET_SSS1) &&
		((mid_pwr_get_state(midpwr, 2) & LPMP3_TARGET_SSS2_MASK) ==
					LPMP3_TARGET_SSS2) &&
		((mid_pwr_get_state(midpwr, 3) & LPMP3_TARGET_SSS3_MASK) ==
					LPMP3_TARGET_SSS3))
		return true;

	return false;
}

static void pmu_set_s0ix_possible(int state)
{
	/* assume S0ix not possible */
	midpwr->s0ix_possible = 0;

	if (state != PCI_D0) {

		if (likely(check_s0ix_possible()))
			midpwr->s0ix_possible = MID_S0IX_STATE;
		else if (check_lpmp3_possible())
			midpwr->s0ix_possible = MID_LPMP3_STATE;
	}
}

/* North Complex Power management */
#define OSPM_PUNIT_PORT         0x04
#define OSPM_OSPMBA             0x78
#define OSPM_PM_SSC             0x20
#define OSPM_PM_SSS             0x30

#define OSPM_APMBA              0x7a
#define APM_CMD                 0x0
#define APM_STS                 0x04

static int wait_for_nc_pmcmd_complete(int verify_mask, int state_type
					, int reg_type)
{
	int pwr_sts;
	int count = 0;
	u32 addr;

	switch (reg_type) {
	case APM_REG_TYPE:
		addr = midpwr->apm_base + APM_STS;
		break;
	case OSPM_REG_TYPE:
		addr = midpwr->ospm_base + OSPM_PM_SSS;
		break;
	default:
		return -EINVAL;
	}

	while (true) {
		pwr_sts = inl(addr);
		if (state_type == OSPM_ISLAND_DOWN) {
			if ((pwr_sts & verify_mask) == verify_mask)
				break;
			else
				udelay(10);
		} else if (state_type == OSPM_ISLAND_UP) {
			if (pwr_sts  == verify_mask)
				break;
			else
				udelay(10);
		}
		count++;
		if (WARN_ONCE(count > 500000, "Timed out waiting for P-Unit"))
			return -EBUSY;
	}
	return 0;
}


/**
 * pmu_nc_set_power_state - Callback function is used by all the devices
 * in north complex for a platform  specific device power on/shutdown.
 * Following assumptions are made by this function
 *
 * Every new request starts from scratch with no assumptions
 * on previous/pending request to Punit.
 * Caller is responsible to retry if request fails.
 * Avoids multiple requests to Punit if target state is
 * already in the expected state.
 * spin_locks guarantee serialized access to these registers
 * and avoid concurrent access from 2d/3d, VED, VEC, ISP & IPH.
 *
 */
int pmu_nc_set_power_state(int islands, int state_type, int reg_type)
{
	u32 pwr_cnt = 0;
	u32 pwr_mask = 0;
	// unsigned long flags;
	int i, lss, mask;
	int ret = 0;

	// spin_lock_irqsave(&mid_pmu_cxt->nc_ready_lock, flags);

	switch (reg_type) {
	case APM_REG_TYPE:
		pwr_cnt = inl(midpwr->apm_base + APM_STS);
		break;
	case OSPM_REG_TYPE:
		pwr_cnt = inl(midpwr->ospm_base + OSPM_PM_SSS);
		break;
	default:
		ret = -EINVAL;
		goto unlock;
	}

	pwr_mask = pwr_cnt;
	for (i = 0; i < OSPM_MAX_POWER_ISLANDS; i++) {
		lss = islands & (0x1 << i);
		if (lss) {
			mask = 0x3 << (2 * i);
			if (state_type == OSPM_ISLAND_DOWN)
				pwr_mask |= mask;
			else if (state_type == OSPM_ISLAND_UP)
				pwr_mask &= ~mask;
		}
	}

	if (pwr_mask != pwr_cnt) {
		switch (reg_type) {
		case APM_REG_TYPE:
			outl(pwr_mask, midpwr->apm_base + APM_CMD);
			break;
		case OSPM_REG_TYPE:
			outl(pwr_mask, (midpwr->ospm_base + OSPM_PM_SSC));
			break;
		}

		ret =
		wait_for_nc_pmcmd_complete(pwr_mask, state_type, reg_type);
	}

unlock:
	// spin_unlock_irqrestore(&mid_pmu_cxt->nc_ready_lock, flags);
	return ret;
}
EXPORT_SYMBOL(pmu_nc_set_power_state);

#define GFX_LSS_INDEX			1
#define ISP_POS			7

static bool update_nc_device_states(int i, pci_power_t state)
{
	int status = 0;

	/* store the display status */
	if (i == GFX_LSS_INDEX) {
		midpwr->display_off = (state != PCI_D0);
		return true;
	}

	/*Update the Camera status per ISP Driver Suspended/Resumed
	* ISP power islands are also updated accordingly, otherwise Dx state
	* in PMCSR refuses to change.
	*/
	if (i == ISP_POS) {
		status = pmu_nc_set_power_state(APM_ISP_ISLAND | APM_IPH_ISLAND,
			(state != PCI_D0) ?
			OSPM_ISLAND_DOWN : OSPM_ISLAND_UP,
			APM_REG_TYPE);
		if (status)
			return false;
		midpwr->camera_off = (state != PCI_D0);
		return true;
	}

	return false;
}

static int __set_power_state(struct mid_pwr *pwr, struct pci_dev *pdev,
			     pci_power_t state, int id, int reg, int bit)
{
	const char *name;
	int ret;

	state = __find_weakest_power_state(pwr->lss[id], pdev, state);
	name = pci_power_name(state);

	/*If the LSS corresponds to northcomplex device, update
	  *the status and return*/
	update_nc_device_states(id, state);

	ret = __update_power_state(pwr, reg, bit, (__force int)state);
	if (ret) {
		dev_warn(&pdev->dev, "Can't set power state %s: %d\n", name, ret);
		return ret;
	}
	pmu_set_s0ix_possible(state);

	dev_vdbg(&pdev->dev, "Set power state %s\n", name);
	return 0;
}

static int mid_pwr_set_power_state(struct mid_pwr *pwr, struct pci_dev *pdev,
				   pci_power_t state)
{
	int id, reg, bit;
	int ret;

	id = intel_mid_pwr_get_lss_id(pdev);
	if (id < 0)
		return id;

	reg = (id * LSS_PWS_BITS) / 32;
	bit = (id * LSS_PWS_BITS) % 32;

	/* We support states between PCI_D0 and PCI_D3hot */
	if (state < PCI_D0)
		state = PCI_D0;
	if (state > PCI_D3hot)
		state = PCI_D3hot;

	mutex_lock(&pwr->lock);
	ret = __set_power_state(pwr, pdev, state, id, reg, bit);
	mutex_unlock(&pwr->lock);
	return ret;
}

int intel_mid_pci_set_power_state(struct pci_dev *pdev, pci_power_t state)
{
	struct mid_pwr *pwr = midpwr;
	int ret = 0;

	might_sleep();

	if (pwr && pwr->available)
		ret = mid_pwr_set_power_state(pwr, pdev, state);
	dev_vdbg(&pdev->dev, "set_power_state() returns %d\n", ret);

	return 0;
}

pci_power_t intel_mid_pci_get_power_state(struct pci_dev *pdev)
{
	struct mid_pwr *pwr = midpwr;
	int id, reg, bit;
	u32 power;

	if (!pwr || !pwr->available)
		return PCI_UNKNOWN;

	id = intel_mid_pwr_get_lss_id(pdev);
	if (id < 0)
		return PCI_UNKNOWN;

	reg = (id * LSS_PWS_BITS) / 32;
	bit = (id * LSS_PWS_BITS) % 32;
	power = mid_pwr_get_state(pwr, reg);
	return (__force pci_power_t)((power >> bit) & 3);
}

void intel_mid_pwr_power_off(void)
{
	struct mid_pwr *pwr = midpwr;
	u32 cmd = PM_CMD_SYS_STATE_S5 |
		  PM_CMD_CMD(CMD_SET_CFG) |
		  PM_CMD_CM_TRIGGER |
		  PM_CMD_CFG_TRIGGER_NC |
		  TRIGGER_NC_MSG_2;

	/* Send command to SCU */
	writel(cmd, pwr->regs + PM_CMD);
	mid_pwr_wait(pwr);
}

int intel_mid_pwr_get_lss_id(struct pci_dev *pdev)
{
	int vndr;
	u8 id;

	/*
	 * Mapping to PWRMU index is kept in the Logical SubSystem ID byte of
	 * Vendor capability.
	 */
	vndr = pci_find_capability(pdev, PCI_CAP_ID_VNDR);
	if (!vndr)
		return -EINVAL;

	/* Read the Logical SubSystem ID byte */
	pci_read_config_byte(pdev, vndr + INTEL_MID_PWR_LSS_OFFSET, &id);
	if (!(id & INTEL_MID_PWR_LSS_TYPE))
		return -ENODEV;

	id &= ~INTEL_MID_PWR_LSS_TYPE;
	if (id >= LSS_MAX_DEVS)
		return -ERANGE;

	return id;
}

static bool pmu_initialized;

void pmu_set_s0ix_complete(void)
{
	if (unlikely(midpwr->s0ix_entered))
		writel(0, midpwr->regs + PM_MSIC);
}
EXPORT_SYMBOL(pmu_set_s0ix_complete);

bool pmu_is_s0ix_in_progress(void)
{
	bool state = false;

	if (pmu_initialized && midpwr->s0ix_entered)
		state = true;

	return state;
}
EXPORT_SYMBOL(pmu_is_s0ix_in_progress);

static inline bool nc_device_state(void)
{
	return !midpwr->display_off || !midpwr->camera_off;
}

int extended_cstate_mode = MID_S0IX_STATE;

/*
 *Decide which state the platfrom can go to based on user and
 *platfrom inputs
*/
int get_final_state(unsigned long *eax)
{
	int ret = 0;
	int possible = midpwr->s0ix_possible;

	switch (extended_cstate_mode) {
	case MID_S0I1_STATE:
	case MID_S0I3_STATE:
	case MID_I1I3_STATE:
		/* user asks s0i1/s0i3 then only
		 * do s0i1/s0i3, dont do lpmp3
		 */
		if (possible == MID_S0IX_STATE)
			ret = extended_cstate_mode & possible;
		break;

	case MID_LPMP3_STATE:
		/* user asks lpmp3 then only
		 * do lpmp3
		 */
		if (possible == MID_LPMP3_STATE)
			ret = MID_LPMP3_STATE;
		break;

	case MID_LPI1_STATE:
	case MID_LPI3_STATE:
		/* user asks lpmp3/i1/i3 then only
		 * do lpmp3/i1/i3
		 */
		if (possible == MID_LPMP3_STATE)
			ret = MID_LPMP3_STATE;
		else if (possible == MID_S0IX_STATE)
			ret = extended_cstate_mode >> REMOVE_LP_FROM_LPIX;
		break;

	case MID_S0IX_STATE:
		ret = possible;
		break;
	}

	if ((ret == MID_S0IX_STATE) &&
			(*eax == MID_LPMP3_STATE))
		ret = MID_S0I1_STATE;
	else if ((ret <= *eax ||
			(ret == MID_S0IX_STATE)))
		ret = ret & *eax;
	else
		ret = 0;

	return ret;
}

int get_target_platform_state(unsigned long *eax)
{
	int ret = 0;

	if (unlikely(!pmu_initialized))
		goto ret;

	/* dont do s0ix if suspend in progress */
	if (unlikely(midpwr->suspend_started))
		goto ret;

	/* dont do s0ix if shutdown in progress */
	if (unlikely(midpwr->shutdown_started))
		goto ret;

	if (nc_device_state())
		goto ret;

	ret = get_final_state(eax);

ret:
	*eax = C6_HINT;
	return ret;
}
EXPORT_SYMBOL(get_target_platform_state);

struct platform_pmu_ops {
	int (*init)(void);
	void (*prepare)(int);
	bool (*enter)(int);
	void (*wakeup)(void);
	void (*remove)(void);
	pci_power_t (*pci_choose_state) (int);
};

#define S0I3_SSS0 ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_AONT_LSS_02) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I2_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I1_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I1_MASK, PMU_USB_HSIC_LSS_07) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_ENGINE_LSS_08) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_DMA_LSS_09) | \
	SSMSK(D0I3_MASK, PMU_SRAM_LSS_12) | \
	SSMSK(D0I3_MASK, PMU_SRAM_LSS_13) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define S0I3_SSS1 ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_SPI2_LSS_19-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_RAM_LSS_25-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))

#define S0I3_SSS2 ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I3_MASK, PMU_GPIO1_LSS_37-32) | \
	SSMSK(D0I3_MASK, PMU_PWR_BUTTON_LSS_38-32) | \
	SSMSK(D0I3_MASK, PMU_KEYBRD_LSS_40-32) | \
	SSMSK(D0I1_MASK, PMU_UART2_LSS_41-32))

#define S0I3_SSS3 ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SLIM1_LSS_49-48) | \
	SSMSK(D0I3_MASK, PMU_RESET_LSS_50-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48) | \
	SSMSK(D0I3_MASK, PMU_GP_DMA_LSS_54-48))

#define S0I1_SSS0 S0I3_SSS0
#define S0I1_SSS1 S0I3_SSS1
#define S0I1_SSS2 S0I3_SSS2
#define S0I1_SSS3 S0I3_SSS3

#define LPMP3_SSS0 ( \
	SSMSK(D0I3_MASK, PMU_SDIO0_LSS_00) | \
	SSMSK(D0I3_MASK, PMU_EMMC0_LSS_01) | \
	SSMSK(D0I3_MASK, PMU_AONT_LSS_02) | \
	SSMSK(D0I3_MASK, PMU_HSI_LSS_03) | \
	SSMSK(D0I2_MASK, PMU_SECURITY_LSS_04) | \
	SSMSK(D0I3_MASK, PMU_EMMC1_LSS_05) | \
	SSMSK(D0I1_MASK, PMU_USB_OTG_LSS_06) | \
	SSMSK(D0I1_MASK, PMU_USB_HSIC_LSS_07) | \
	SSMSK(D0I3_MASK, PMU_SDIO2_LSS_14))

#define LPMP3_SSS1 ( \
	SSMSK(D0I3_MASK, PMU_SPI1_LSS_18-16) | \
	SSMSK(D0I3_MASK, PMU_SPI2_LSS_19-16) | \
	SSMSK(D0I3_MASK, PMU_I2C0_LSS_20-16) | \
	SSMSK(D0I3_MASK, PMU_I2C1_LSS_21-16) | \
	SSMSK(D0I3_MASK, PMU_I2C2_LSS_27-16) | \
	SSMSK(D0I3_MASK, PMU_SDIO1_LSS_30-16))

#define LPMP3_SSS2 ( \
	SSMSK(D0I3_MASK, PMU_I2C3_LSS_33-32) | \
	SSMSK(D0I3_MASK, PMU_I2C4_LSS_34-32) | \
	SSMSK(D0I3_MASK, PMU_I2C5_LSS_35-32) | \
	SSMSK(D0I3_MASK, PMU_SPI3_LSS_36-32) | \
	SSMSK(D0I3_MASK, PMU_GPIO1_LSS_37-32) | \
	SSMSK(D0I3_MASK, PMU_PWR_BUTTON_LSS_38-32) | \
	SSMSK(D0I3_MASK, PMU_KEYBRD_LSS_40-32) | \
	SSMSK(D0I1_MASK, PMU_UART2_LSS_41-32))

#define LPMP3_SSS3 ( \
	SSMSK(D0I3_MASK, PMU_AUDIO_SLIM1_LSS_49-48) | \
	SSMSK(D0I3_MASK, PMU_RESET_LSS_50-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP0_LSS_51-48) | \
	SSMSK(D0I3_MASK, PMU_AUDIO_SSP1_LSS_52-48) | \
	SSMSK(D0I3_MASK, PMU_GP_DMA_LSS_54-48))

void pmu_stat_start(enum sys_state type)
{
}

void pmu_stat_end(void)
{
}

#define S5_VALUE	0x309D2601
#define S0I1_VALUE	0X30992601
#define LPMP3_VALUE	0X40492601
#define S0I3_VALUE	0X309B2601

u32 get_s0ix_val_set_pm_ssc(int s0ix_state)
{
	u32 s0ix_value;

	switch (s0ix_state) {
	case MID_S0I1_STATE:
		mid_pwr_set_state(midpwr, 0, S0I1_SSS0);
		mid_pwr_set_state(midpwr, 1, S0I1_SSS1);
		mid_pwr_set_state(midpwr, 2, S0I1_SSS2);
		mid_pwr_set_state(midpwr, 3, S0I1_SSS3);
		pmu_stat_start(SYS_STATE_S0I1);
		s0ix_value = S0I1_VALUE;
		break;
	case MID_LPMP3_STATE:
		mid_pwr_set_state(midpwr, 0, LPMP3_SSS0);
		mid_pwr_set_state(midpwr, 1, LPMP3_SSS1);
		mid_pwr_set_state(midpwr, 2, LPMP3_SSS2);
		mid_pwr_set_state(midpwr, 3, LPMP3_SSS3);
		pmu_stat_start(SYS_STATE_S0I2);
		s0ix_value = LPMP3_VALUE;
		break;
	case MID_S0I3_STATE:
		mid_pwr_set_state(midpwr, 0, S0I3_SSS0);
		mid_pwr_set_state(midpwr, 1, S0I3_SSS1);
		mid_pwr_set_state(midpwr, 2, S0I3_SSS2);
		mid_pwr_set_state(midpwr, 3, S0I3_SSS3);
		pmu_stat_start(SYS_STATE_S0I3);
		s0ix_value = S0I3_VALUE;
		break;
	case MID_S3_STATE:
		mid_pwr_set_state(midpwr, 0, S0I3_SSS0);
		mid_pwr_set_state(midpwr, 1, S0I3_SSS1);
		mid_pwr_set_state(midpwr, 2, S0I3_SSS2);
		mid_pwr_set_state(midpwr, 3, S0I3_SSS3);
		pmu_stat_start(SYS_STATE_S3);
		s0ix_value = S0I3_VALUE;
		break;
	default:
		// pmu_dump_logs();
		BUG_ON(1);
	}
	return s0ix_value;
}

#define PMU_MISC_SET_TIMEOUT	50 /* 50usec timeout */

/* Definition for C6 Offload MSR Address */
#define MSR_C6OFFLOAD_CTL_REG			0x120

#define MSR_C6OFFLOAD_SET_LOW			1
#define MSR_C6OFFLOAD_SET_HIGH			0

/* Accessor function for pci_devs start */
static inline void pmu_stat_clear(void)
{
	midpwr->pmu_current_state = SYS_STATE_S0I0;
}

/* To CLEAR C6 offload Bit(LSB) in MSR 120 */
static inline void clear_c6offload_bit(void)
{
	u32 msr_low, msr_high;

	rdmsr(MSR_C6OFFLOAD_CTL_REG, msr_low, msr_high);
	msr_low = msr_low & ~MSR_C6OFFLOAD_SET_LOW;
	msr_high = msr_high & ~MSR_C6OFFLOAD_SET_HIGH;
	wrmsr(MSR_C6OFFLOAD_CTL_REG, msr_low, msr_high);
}

#define PMU_C6OFFLOAD_ACCESS_TIMEOUT 1500 /* 1.5msecs timeout */
#define C6OFFLOAD_BIT_MASK			0x2
#define C6OFFLOAD_BIT				0x2

/* To SET C6 offload Bit(LSB) in MSR 120 */
static inline void set_c6offload_bit(void)
{
	u32 msr_low, msr_high;

	rdmsr(MSR_C6OFFLOAD_CTL_REG, msr_low, msr_high);
	msr_low = msr_low | MSR_C6OFFLOAD_SET_LOW;
	msr_high = msr_high | MSR_C6OFFLOAD_SET_HIGH;
	wrmsr(MSR_C6OFFLOAD_CTL_REG, msr_low, msr_high);
}


static bool mfld_pmu_enter(int s0ix_state)
{
	u32 s0ix_value;
	u32 ssw_val;
	int num_retry = PMU_MISC_SET_TIMEOUT;

	s0ix_value = get_s0ix_val_set_pm_ssc(s0ix_state);

	clear_c6offload_bit();

	/* issue a command to SCU */
	writel(s0ix_value, midpwr->regs + PM_CMD);

	// pmu_log_command(s0ix_value, NULL);

	do {
		if (readl(midpwr->regs + PM_MSIC))
			break;
		udelay(1);
	} while (--num_retry);

	if (!num_retry && !readl(midpwr->regs + PM_MSIC))
		WARN(1, "%s: pm_msic not set.\n", __func__);

	num_retry = PMU_C6OFFLOAD_ACCESS_TIMEOUT;

	/* At this point we have committed an S0ix command
	 * will have to wait for the SCU s0ix complete
	 * intertupt to proceed further.
	 */
	midpwr->s0ix_entered = s0ix_state;

	if (s0ix_value == S0I3_VALUE) {
		do {
			ssw_val = readl(midpwr->offload_reg);
			if ((ssw_val & C6OFFLOAD_BIT_MASK) ==  C6OFFLOAD_BIT) {
				set_c6offload_bit();
				break;
			}

			udelay(1);
		} while (--num_retry);

		if (unlikely(!num_retry)) {
			WARN(1, "mid_pmu: error cpu offload bit not set.\n");
			pmu_stat_clear();
			return false;
		}
	}

	return true;
}

static void mfld_pmu_wakeup(void)
{

	/* Wakeup allother CPU's */
	if (midpwr->s0ix_entered)
		apic->send_IPI_allbutself(RESCHEDULE_VECTOR);

	clear_c6offload_bit();
}

static void mfld_pmu_remove(void)
{
	/* Freeing up memory allocated for PMU1 & PMU2 */
	iounmap(midpwr->offload_reg);
	midpwr->offload_reg = NULL;

}

#define C6_OFFLOAD_REG_ADDR	0xffd01ffc

static int mfld_pmu_init(void)
{
	int ret = PMU_SUCCESS;

	/* Map the memory of offload_reg */
	midpwr->offload_reg =
				ioremap(C6_OFFLOAD_REG_ADDR, 4);
	if (midpwr->offload_reg == NULL) {
		dev_dbg(midpwr->dev,
		"Unable to map the offload_reg address space\n");
		ret = PMU_FAILED;
		goto out_err;
	}

out_err:
	return ret;
}

static struct platform_pmu_ops mfld_pmu_ops = {
	.init	 = mfld_pmu_init,
	.enter	 = mfld_pmu_enter,
	.wakeup = mfld_pmu_wakeup,
	.remove = mfld_pmu_remove,
	// .pci_choose_state = mfld_pmu_choose_state,
};
static struct platform_pmu_ops *pmu_ops = &mfld_pmu_ops;

/*This function is used for programming the wake capable devices*/
static void pmu_prepare_wake(int s0ix_state)
{
	/* Re-program the sub systems state on wakeup as the current SSS*/

	mid_pwr_set_wakeup_state(midpwr, 0, mid_pwr_get_state(midpwr, 0));
	mid_pwr_set_wakeup_state(midpwr, 1, mid_pwr_get_state(midpwr, 1));
	mid_pwr_set_wakeup_state(midpwr, 2, mid_pwr_get_state(midpwr, 2));
	mid_pwr_set_wakeup_state(midpwr, 3, mid_pwr_get_state(midpwr, 3));
}

int mid_s0ix_enter(int s0ix_state)
{
	int ret = 0;

	if (unlikely(!pmu_ops || !pmu_ops->enter))
		goto ret;

	/* check if we can acquire scu_ready_sem
	 * if we are not able to then do a c6 */
	// if (down_trylock(&mid_pmu_cxt->scu_ready_sem))
	// 	goto ret;

	/* If PMU is busy, we'll retry on next C6 */
	if (unlikely(mid_pwr_is_busy(midpwr))) {
		// up(&mid_pmu_cxt->scu_ready_sem);
		// pr_debug("mid_pmu_cxt->scu_read_sem is up\n");
		goto ret;
	}

	pmu_prepare_wake(s0ix_state);

	/* no need to proceed if schedule pending */
	if (unlikely(need_resched())) {
		pmu_stat_clear();
		// up(&mid_pmu_cxt->scu_ready_sem);
		goto ret;
	}

	/* entry function for pmu driver ops */
	if (pmu_ops->enter(s0ix_state))
		ret = s0ix_state;

ret:
	return ret;
}

static irqreturn_t mid_pwr_irq_handler(int irq, void *dev_id)
{
	struct mid_pwr *pwr = dev_id;
	u32 ics;

	ics = readl(pwr->regs + PM_ICS);
	if (!(ics & PM_ICS_IP))
		return IRQ_NONE;

	writel(ics | PM_ICS_IP, pwr->regs + PM_ICS);

	dev_warn(pwr->dev, "Unexpected IRQ: %#x\n", PM_ICS_INT_STATUS(ics));
	if (pmu_ops->wakeup)
		pmu_ops->wakeup();

	midpwr->s0ix_entered = 0;

	/* S0ix case release it */
	// up(&mid_pmu_cxt->scu_ready_sem);

	return IRQ_HANDLED;
}

struct mid_pwr_device_info {
	int (*set_initial_state)(struct mid_pwr *pwr);
};

/*
 * Access to message bus through these 2 registers
 * in CUNIT(0:0:0) PCI configuration space.
 * MSGBUS_CTRL_REG(0xD0):
 *   31:24	= message bus opcode
 *   23:16	= message bus port
 *   15:8	= message bus address
 *   7:4	= message bus byte enables
 * MSGBUS_DTAT_REG(0xD4):
 *   hold the data for write or read
 */
#define PCI_ROOT_MSGBUS_CTRL_REG	0xD0
#define PCI_ROOT_MSGBUS_DATA_REG	0xD4
#define PCI_ROOT_MSGBUS_READ		0x10
#define PCI_ROOT_MSGBUS_WRITE		0x11
#define PCI_ROOT_MSGBUS_DWORD_ENABLE	0xf0

u32 intel_mid_msgbus_read32_raw(u32 cmd)
{
	struct pci_dev *pci_root;
	// unsigned long irq_flags;
	u32 data;

	pci_root = pci_get_domain_bus_and_slot(0, 0, PCI_DEVFN(0, 0));

	// spin_lock_irqsave(&msgbus_lock, irq_flags);
	pci_write_config_dword(pci_root, PCI_ROOT_MSGBUS_CTRL_REG, cmd);
	pci_read_config_dword(pci_root, PCI_ROOT_MSGBUS_DATA_REG, &data);
	// spin_unlock_irqrestore(&msgbus_lock, irq_flags);

	pci_dev_put(pci_root);

	return data;
}
EXPORT_SYMBOL(intel_mid_msgbus_read32_raw);

u32 intel_mid_msgbus_read32(u8 port, u8 addr)
{
	u32 cmd = (PCI_ROOT_MSGBUS_READ << 24) | (port << 16) |
		(addr << 8) | PCI_ROOT_MSGBUS_DWORD_ENABLE;

	return intel_mid_msgbus_read32_raw(cmd);
}
EXPORT_SYMBOL(intel_mid_msgbus_read32);

static int mid_pwr_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	struct mid_pwr_device_info *info = (void *)id->driver_data;
	struct device *dev = &pdev->dev;
	struct mid_pwr *pwr;
	int ret;
	u32 data;

	ret = pcim_enable_device(pdev);
	if (ret < 0) {
		dev_err(&pdev->dev, "error: could not enable device\n");
		return ret;
	}

	ret = pcim_iomap_regions(pdev, 1 << 0, pci_name(pdev));
	if (ret) {
		dev_err(&pdev->dev, "I/O memory remapping failed\n");
		return ret;
	}

	pwr = devm_kzalloc(dev, sizeof(*pwr), GFP_KERNEL);
	if (!pwr)
		return -ENOMEM;

	pwr->dev = dev;
	pwr->regs = pcim_iomap_table(pdev)[0];
	pwr->irq = pdev->irq;

	mutex_init(&pwr->lock);

	/* Disable interrupts */
	mid_pwr_interrupt_disable(pwr);

	if (info && info->set_initial_state) {
		ret = info->set_initial_state(pwr);
		if (ret)
			dev_warn(dev, "Can't set initial state: %d\n", ret);
	}

	ret = devm_request_irq(dev, pdev->irq, mid_pwr_irq_handler,
			       IRQF_NO_SUSPEND, pci_name(pdev), pwr);
	if (ret)
		return ret;

	pwr->available = true;
	midpwr = pwr;

	/* platform specific initialization */
	if (pmu_ops->init)
		pmu_ops->init();

	data = intel_mid_msgbus_read32(OSPM_PUNIT_PORT, OSPM_APMBA);
	pwr->apm_base = data & 0xffff;

	data = intel_mid_msgbus_read32(OSPM_PUNIT_PORT, OSPM_OSPMBA);
	pwr->ospm_base = data & 0xffff;

	pci_set_drvdata(pdev, pwr);
	return 0;
}

static void mid_pwr_remove(struct pci_dev *pdev) {
	if (pmu_ops->remove)
		pmu_ops->remove();
}

static int mid_set_initial_state(struct mid_pwr *pwr, const u32 *states)
{
	unsigned int i, j;
	int ret;

	/*
	 * Enable wake events.
	 *
	 * PWRMU supports up to 32 sources for wake up the system. Ungate them
	 * all here.
	 */
	mid_pwr_set_wake(pwr, 0, 0xffffffff);
	mid_pwr_set_wake(pwr, 1, 0xffffffff);

	/*
	 * Power off South Complex devices.
	 *
	 * There is a map (see a note below) of 64 devices with 2 bits per each
	 * on 32-bit HW registers. The following calls set all devices to one
	 * known initial state, i.e. PCI_D3hot. This is done in conjunction
	 * with PMCSR setting in arch/x86/pci/intel_mid_pci.c.
	 *
	 * NOTE: The actual device mapping is provided by a platform at run
	 * time using vendor capability of PCI configuration space.
	 */
	mid_pwr_set_state(pwr, 0, states[0]);
	mid_pwr_set_state(pwr, 1, states[1]);
	mid_pwr_set_state(pwr, 2, states[2]);
	mid_pwr_set_state(pwr, 3, states[3]);

	/* Send command to SCU */
	ret = mid_pwr_wait_for_cmd(pwr, CMD_SET_CFG);
	if (ret)
		return ret;

	for (i = 0; i < LSS_MAX_DEVS; i++) {
		for (j = 0; j < LSS_MAX_SHARED_DEVS; j++)
			pwr->lss[i][j].state = PCI_D3hot;
	}

	return 0;
}

static int pnw_set_initial_state(struct mid_pwr *pwr)
{
	/* On Penwell SRAM must stay powered on */
	static const u32 states[] = {
		0xf00fffff,		/* PM_SSC(0) */
		0xffffffff,		/* PM_SSC(1) */
		0xffffffff,		/* PM_SSC(2) */
		0xffffffff,		/* PM_SSC(3) */
	};
	return mid_set_initial_state(pwr, states);
}

static int tng_set_initial_state(struct mid_pwr *pwr)
{
	static const u32 states[] = {
		0xffffffff,		/* PM_SSC(0) */
		0xffffffff,		/* PM_SSC(1) */
		0xffffffff,		/* PM_SSC(2) */
		0xffffffff,		/* PM_SSC(3) */
	};
	return mid_set_initial_state(pwr, states);
}

static const struct mid_pwr_device_info pnw_info = {
	.set_initial_state = pnw_set_initial_state,
};

static const struct mid_pwr_device_info tng_info = {
	.set_initial_state = tng_set_initial_state,
};

/* This table should be in sync with the one in drivers/pci/pci-mid.c */
static const struct pci_device_id mid_pwr_pci_ids[] = {
	{ PCI_VDEVICE(INTEL, PCI_DEVICE_ID_PENWELL), (kernel_ulong_t)&pnw_info },
	{ PCI_VDEVICE(INTEL, PCI_DEVICE_ID_TANGIER), (kernel_ulong_t)&tng_info },
	{}
};

static struct pci_driver mid_pwr_pci_driver = {
	.name		= "intel_mid_pwr",
	.probe		= mid_pwr_probe,
	.id_table	= mid_pwr_pci_ids,
	.remove     = mid_pwr_remove,
};

builtin_pci_driver(mid_pwr_pci_driver);
