/**************************************************************************
 * Copyright (c) 2007-2011, Intel Corporation.
 * All Rights Reserved.
 * Copyright (c) 2008, Tungsten Graphics Inc.  Cedar Park, TX., USA.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 **************************************************************************/

#ifndef _PSB_DRM_H_
#define _PSB_DRM_H_

#define PSB_NUM_PIPE 3

#define PSB_GPU_ACCESS_READ         (1ULL << 32)
#define PSB_GPU_ACCESS_WRITE        (1ULL << 33)
#define PSB_GPU_ACCESS_MASK         (PSB_GPU_ACCESS_READ | PSB_GPU_ACCESS_WRITE)

#define PSB_BO_FLAG_COMMAND         (1ULL << 52)

/*
 * Feedback components:
 */

struct drm_psb_sizes_arg {
	u32 ta_mem_size;
	u32 mmu_size;
	u32 pds_size;
	u32 rastgeom_size;
	u32 tt_size;
	u32 vram_size;
};

struct drm_psb_dpst_lut_arg {
	uint8_t lut[256];
	int output_id;
};

struct gct_timing_desc_block {
	uint16_t clock;
	uint16_t hactive:12;
	uint16_t hblank:12;
	uint16_t hsync_start:10;
	uint16_t hsync_end:10;
	uint16_t hsync_polarity:1;
	uint16_t h_reversed:3;
	uint16_t vactive:12;
	uint16_t vblank:12;
	uint16_t vsync_start:6;
	uint16_t vsync_end:6;
	uint16_t vsync_polarity:1;
	uint16_t v_reversed:3;
} __packed;

struct gct_display_desc_block {
	uint8_t type:2;
	uint8_t pxiel_format:4;
	uint8_t mode:2;
	uint8_t frame_rate:6;
	uint8_t reserved:2;
} __attribute__((packed));

struct gct_dsi_desc_block {
	uint8_t lane_count:2;
	uint8_t lane_frequency:3;
	uint8_t transfer_mode:2;
	uint8_t hs_clock_behavior:1;
	uint8_t duo_display_support:1;
	uint8_t ecc_caps:1;
	uint8_t bdirect_support:1;
	uint8_t reversed:5;
} __packed;

struct gct_bkl_desc_block {
	uint16_t frequency;
	uint8_t max_brightness:7;
	uint8_t polarity:1;
} __packed;

struct gct_r20_clock_desc {
	uint8_t pre_divisor:2;
	uint16_t divisor:9;
	uint8_t post_divisor:4;
	uint8_t pll_bypass:1;
	uint8_t cck_clock_divisor:1;
	uint8_t reserved:7;
} __packed;

struct gct_r20_panel_info {
	uint16_t width;
	uint16_t height;
} __packed;

struct gct_r20_panel_mode {
	uint8_t mode:1;
	uint16_t reserved:15;
} __packed;

struct gct_r20_dsi_desc {
	uint8_t num_dsi_lanes:2;
	uint16_t reserved:14;
} __packed;

struct gct_r20_panel_desc {
	uint8_t panel_name[16];
	struct gct_timing_desc_block timing;
	struct gct_r20_clock_desc clock_desc;
	struct gct_r20_panel_info panel_info;
	struct gct_r20_panel_mode panel_mode;
	struct gct_r20_dsi_desc dsi_desc;
	uint32_t early_init_seq;
	uint32_t late_init_seq;
} __packed;

struct gct_r11_panel_desc {
	uint8_t panel_name[16];
	struct gct_timing_desc_block timing;
	struct gct_display_desc_block display;
	struct gct_dsi_desc_block dsi;
	struct gct_bkl_desc_block bkl;
	uint32_t early_init_seq;
	uint32_t late_init_seq;
} __packed;

struct gct_r10_panel_desc {
	struct gct_timing_desc_block timing;
	struct gct_display_desc_block display;
	struct gct_dsi_desc_block dsi;
	struct gct_bkl_desc_block bkl;
	uint32_t early_init_seq;
	uint32_t late_init_seq;
	uint8_t reversed[4];
} __packed;

#define PSB_DC_CRTC_SAVE 0x01
#define PSB_DC_CRTC_RESTORE 0x02
#define PSB_DC_OUTPUT_SAVE 0x04
#define PSB_DC_OUTPUT_RESTORE 0x08
#define PSB_DC_CRTC_MASK 0x03
#define PSB_DC_OUTPUT_MASK 0x0C

struct drm_psb_dc_state_arg {
	u32 flags;
	u32 obj_id;
};

struct drm_psb_mode_operation_arg {
	u32 obj_id;
	u16 operation;
	struct drm_mode_modeinfo mode;
	void *data;
};

struct drm_psb_stolen_memory_arg {
	u32 base;
	u32 size;
};

/*Display Register Bits*/
#define REGRWBITS_PFIT_CONTROLS			(1 << 0)
#define REGRWBITS_PFIT_AUTOSCALE_RATIOS		(1 << 1)
#define REGRWBITS_PFIT_PROGRAMMED_SCALE_RATIOS	(1 << 2)
#define REGRWBITS_PIPEASRC			(1 << 3)
#define REGRWBITS_PIPEBSRC			(1 << 4)
#define REGRWBITS_VTOTAL_A			(1 << 5)
#define REGRWBITS_VTOTAL_B			(1 << 6)
#define REGRWBITS_DSPACNTR	(1 << 8)
#define REGRWBITS_DSPBCNTR	(1 << 9)
#define REGRWBITS_DSPCCNTR	(1 << 10)
#define REGRWBITS_SPRITE_UPDATE         (1 << 11)
#define REGRWBITS_PIPEASTAT             (1 << 12)
#define REGRWBITS_INT_MASK              (1 << 13)
#define REGRWBITS_INT_ENABLE            (1 << 14)
#define REGRWBITS_DISPLAY_ALL           (1 << 15)

/*Overlay Register Bits*/
#define OV_REGRWBITS_OVADD			(1 << 0)
#define OV_REGRWBITS_OGAM_ALL			(1 << 1)

#define OVC_REGRWBITS_OVADD                  (1 << 2)
#define OVC_REGRWBITS_OGAM_ALL			(1 << 3)

#define OV_REGRWBITS_WAIT_FLIP          (1 << 4)
#define OVC_REGRWBITS_WAIT_FLIP         (1 << 5)
#define OVSTATUS_REGRBIT_OVR_UPDT       (1 << 6)

/*sprite update fields*/
#define SPRITE_UPDATE_SURFACE           (0x00000001UL)
#define SPRITE_UPDATE_CONTROL           (0x00000002UL)
#define SPRITE_UPDATE_POSITION          (0x00000004UL)
#define SPRITE_UPDATE_SIZE              (0x00000008UL)
#define SPRITE_UPDATE_WAIT_VBLANK       (0X00000010UL)
#define SPRITE_UPDATE_CONSTALPHA        (0x00000020UL)
#define SPRITE_UPDATE_ALL               (0x0000003fUL)

/*vsync operation*/
#define VSYNC_ENABLE                    (1 << 0)
#define VSYNC_DISABLE                   (1 << 1)
#define VSYNC_WAIT                      (1 << 2)
#define GET_VSYNC_COUNT                 (1 << 3)

/* Polyphase filter coefficients */
#define N_HORIZ_Y_TAPS 5
#define N_VERT_Y_TAPS 3
#define N_HORIZ_UV_TAPS 3
#define N_VERT_UV_TAPS 3
#define N_PHASES 17
#define MAX_TAPS 5

struct overlay_ctrl_blk {
	uint32_t OBUF_0Y;
	uint32_t OBUF_1Y;
	uint32_t OBUF_0U;
	uint32_t OBUF_0V;
	uint32_t OBUF_1U;
	uint32_t OBUF_1V;
	uint32_t OSTRIDE;
	uint32_t YRGB_VPH;
	uint32_t UV_VPH;
	uint32_t HORZ_PH;
	uint32_t INIT_PHS;
	uint32_t DWINPOS;
	uint32_t DWINSZ;
	uint32_t SWIDTH;
	uint32_t SWIDTHSW;
	uint32_t SHEIGHT;
	uint32_t YRGBSCALE;
	uint32_t UVSCALE;
	uint32_t OCLRC0;
	uint32_t OCLRC1;
	uint32_t DCLRKV;
	uint32_t DCLRKM;
	uint32_t SCHRKVH;
	uint32_t SCHRKVL;
	uint32_t SCHRKEN;
	uint32_t OCONFIG;
	uint32_t OCMD;
	uint32_t RESERVED1;
	uint32_t OSTART_0Y;
	uint32_t OSTART_1Y;
	uint32_t OSTART_0U;
	uint32_t OSTART_0V;
	uint32_t OSTART_1U;
	uint32_t OSTART_1V;
	uint32_t OTILEOFF_0Y;
	uint32_t OTILEOFF_1Y;
	uint32_t OTILEOFF_0U;
	uint32_t OTILEOFF_0V;
	uint32_t OTILEOFF_1U;
	uint32_t OTILEOFF_1V;
	uint32_t FASTHSCALE;
	uint32_t UVSCALEV;

	uint32_t RESERVEDC[(0x200 - 0xA8) / 4];
	uint16_t Y_VCOEFS[N_VERT_Y_TAPS * N_PHASES];
	uint16_t RESERVEDD[0x100 / 2 - N_VERT_Y_TAPS * N_PHASES];
	uint16_t Y_HCOEFS[N_HORIZ_Y_TAPS * N_PHASES];
	uint16_t RESERVEDE[0x200 / 2 - N_HORIZ_Y_TAPS * N_PHASES];
	uint16_t UV_VCOEFS[N_VERT_UV_TAPS * N_PHASES];
	uint16_t RESERVEDF[0x100 / 2 - N_VERT_UV_TAPS * N_PHASES];
	uint16_t UV_HCOEFS[N_HORIZ_UV_TAPS * N_PHASES];
	uint16_t RESERVEDG[0x100 / 2 - N_HORIZ_UV_TAPS * N_PHASES];
};

struct intel_overlay_context {
	uint32_t index;
	uint32_t pipe;
	uint32_t ovadd;
};

struct intel_sprite_context {
	uint32_t update_mask;
	/*plane index 0-A, 1-B, 2-C,etc*/
	uint32_t index;
	uint32_t pipe;

	uint32_t cntr;
	uint32_t linoff;
	uint32_t stride;
	uint32_t pos;
	uint32_t size;
	uint32_t keyminval;
	uint32_t keymask;
	uint32_t surf;
	uint32_t keymaxval;
	uint32_t tileoff;
	uint32_t contalpa;
};

/* dependent macros*/
#define INTEL_SPRITE_PLANE_NUM          3
#define INTEL_OVERLAY_PLANE_NUM         2
#define INTEL_DISPLAY_PLANE_NUM         5
/* Medfield */
#define INTEL_MDFLD_SPRITE_PLANE_NUM    3
#define INTEL_MDFLD_OVERLAY_PLANE_NUM   2
#define INTEL_MDFLD_CURSOR_PLANE_NUM    3
#define INTEL_MDFLD_DISPLAY_PLANE_NUM   8
#define INTEL_MDFLD_DISPLAY_PIPE_NUM    3
/* Clovertrail+ */
#define INTEL_CTP_SPRITE_PLANE_NUM      2
#define INTEL_CTP_OVERLAY_PLANE_NUM     1
#define INTEL_CTP_CURSOR_PLANE_NUM      2
#define INTEL_CTP_DISPLAY_PLANE_NUM     5
#define INTEL_CTP_DISPLAY_PIPE_NUM      2

#define INVALID_INDEX                   0xffffffff

struct mdfld_plane_contexts {
	uint32_t active_primaries;
	uint32_t active_sprites;
	uint32_t active_overlays;
	struct intel_sprite_context primary_contexts[INTEL_SPRITE_PLANE_NUM];
	struct intel_sprite_context sprite_contexts[INTEL_SPRITE_PLANE_NUM];
	struct intel_overlay_context overlay_contexts[INTEL_OVERLAY_PLANE_NUM];
};

struct drm_psb_vsync_set_arg {
	uint32_t vsync_operation_mask;

	struct {
		uint32_t pipe;
		int vsync_pipe;
		int vsync_count;
		uint64_t timestamp;
	} vsync;
};

struct drm_psb_dc_info {
	uint32_t pipe_count;

	uint32_t primary_plane_count;
	uint32_t sprite_plane_count;
	uint32_t overlay_plane_count;
	uint32_t cursor_plane_count;
};

struct drm_psb_register_rw_arg {
	u32 b_force_hw_on;

	u32 display_read_mask;
	u32 display_write_mask;

	struct {
		u32 pfit_controls;
		u32 pfit_autoscale_ratios;
		u32 pfit_programmed_scale_ratios;
		u32 pipeasrc;
		u32 pipebsrc;
		u32 vtotal_a;
		u32 vtotal_b;
		u32 dspcntr_a;
		u32 dspcntr_b;
		u32 pipestat_a;
		u32 int_mask;
		u32 int_enable;
	} display;

	u32 overlay_read_mask;
	u32 overlay_write_mask;

	struct {
		u32 OVADD;
		u32 OGAMC0;
		u32 OGAMC1;
		u32 OGAMC2;
		u32 OGAMC3;
		u32 OGAMC4;
		u32 OGAMC5;
		u32 IEP_ENABLED;
		u32 IEP_BLE_MINMAX;
		u32 IEP_BSSCC_CONTROL;
		u32 index;
		u32 b_wait_vblank;
		u32 b_wms;
		u32 buffer_handle;
		u32 backbuf_index;
		unsigned long backbuf_addr;
	} overlay;

	u32 vsync_operation_mask;

	struct {
		u32 pipe;
		int vsync_pipe;
		int vsync_count;
		u64 timestamp;
	} vsync;

	u32 sprite_enable_mask;
	u32 sprite_disable_mask;

	struct {
		u32 dspa_control;
		u32 dspa_key_value;
		u32 dspa_key_mask;
		u32 dspc_control;
		u32 dspc_stride;
		u32 dspc_position;
		u32 dspc_linear_offset;
		u32 dspc_size;
		u32 dspc_surface;
	} sprite;

	u32 subpicture_enable_mask;
	u32 subpicture_disable_mask;
	struct {
		u32 CursorADDR;
		u32 xPos;
		u32 yPos;
		u32 CursorSize;
	} cursor;
	u32 cursor_enable_mask;
	u32 cursor_disable_mask;

	u32 plane_enable_mask;
	u32 plane_disable_mask;

	struct {
		u32 type;
		u32 index;
		u32 ctx;
	} plane;
};

enum {
	PSB_GTT_MAP_TYPE_MEMINFO = 0,
	PSB_GTT_MAP_TYPE_BCD,
	PSB_GTT_MAP_TYPE_BCD_INFO,
	PSB_GTT_MAP_TYPE_VIRTUAL,
};

#define MAX_SLICES_PER_PICTURE 72
struct  psb_msvdx_mb_region {
	uint32_t start;
	uint32_t end;
};

typedef struct drm_psb_msvdx_decode_status {
	uint32_t num_region;
	struct psb_msvdx_mb_region mb_regions[MAX_SLICES_PER_PICTURE];
} drm_psb_msvdx_decode_status_t;

/* Controlling the kernel modesetting buffers */

#define DRM_PSB_SIZES           0x07
#define DRM_PSB_FUSE_REG	0x08
#define DRM_PSB_DC_STATE	0x0A
#define DRM_PSB_ADB		0x0B
#define DRM_PSB_MODE_OPERATION	0x0C
#define DRM_PSB_STOLEN_MEMORY	0x0D
#define DRM_PSB_REGISTER_RW	0x0E

/*
 * NOTE: Add new commands here, but increment
 * the values below and increment their
 * corresponding defines where they're
 * defined elsewhere.
 */

#define DRM_PSB_GEM_CREATE	0x10
#define DRM_PSB_2D_OP		0x11
#define DRM_PSB_GEM_MMAP	0x12
#define DRM_PSB_DPST		0x1B
#define DRM_PSB_GAMMA		0x1C
#define DRM_PSB_DPST_BL		0x1D
#define DRM_PSB_GET_PIPE_FROM_CRTC_ID 0x1F
#define DRM_PSB_HDMI_FB_CMD             0x23

/* HDCP IOCTLs */
#define DRM_PSB_QUERY_HDCP              0x24
#define DRM_PSB_VALIDATE_HDCP_KSV       0x25
#define DRM_PSB_GET_HDCP_STATUS         0x26
#define DRM_PSB_ENABLE_HDCP             0x27
#define DRM_PSB_DISABLE_HDCP            0x28
#define DRM_PSB_GET_HDCP_LINK_STATUS    0x2b
#define DRM_PSB_ENABLE_HDCP_REPEATER    0x2c
#define DRM_PSB_DISABLE_HDCP_REPEATER   0x2d
#define DRM_PSB_HDCP_REPEATER_PRESENT   0x2e

/* CSC IOCTLS */
#define DRM_PSB_CSC_GAMMA_SETTING       0x29
#define DRM_PSB_SET_CSC                 0x2a

/* IED session */
#define DRM_PSB_ENABLE_IED_SESSION      0x30
#define DRM_PSB_DISABLE_IED_SESSION     0x31

/* VSYNC IOCTLS */
#define DRM_PSB_VSYNC_SET               0x32

/* HDCP */
#define DRM_PSB_HDCP_DISPLAY_IED_OFF    0x33
#define DRM_PSB_HDCP_DISPLAY_IED_ON     0x34
#define DRM_PSB_QUERY_HDCP_DISPLAY_IED_CAPS 0x35

/* DPST LEVEL */
#define DRM_PSB_DPST_LEVEL              0x36

/* GET DC INFO IOCTLS */
#define DRM_PSB_GET_DC_INFO             0x37


/****BEGIN HDMI TEST IOCTLS ****/
#define DRM_PSB_HDMITEST                0x38

/* read an hdmi test register */
#define HT_READ                         1
/* write an hdmi test register */
#define HT_WRITE                        2
/* force power island on */
#define HT_FORCEON                      4

typedef struct tagHDMITESTREGREADWRITE {
	/* register offset */
	unsigned int reg;
	/* input/output value */
	unsigned int data;
	/* OR'ed combo of HT_xx flags */
	int mode;
} drm_psb_hdmireg_t, *drm_psb_hdmireg_p;

/**** END HDMI TEST IOCTLS ****/



/* Do not use IOCTL between 0x40 and 0x4F */
/* These will be reserved for OEM to use */
/* OEM IOCTLs */
#define DRM_OEM_RESERVED_START          0x40
#define DRM_OEM_RESERVED_END            0x4F

struct drm_psb_csc_matrix {
	int pipe;
	int64_t matrix[9];
};

struct drm_psb_dev_info_arg {
	uint32_t num_use_attribute_registers;
};
#define DRM_PSB_DEVINFO                 0x01
#define PSB_MODE_OPERATION_MODE_VALID	0x01
#define PSB_MODE_OPERATION_SET_DC_BASE  0x02

struct drm_psb_get_pipe_from_crtc_id_arg {
	/** ID of CRTC being requested **/
	u32 crtc_id;

	/** pipe of requested CRTC **/
	u32 pipe;
};
#define DRM_PSB_DISP_SAVE_HDMI_FB_HANDLE        1
#define DRM_PSB_DISP_GET_HDMI_FB_HANDLE         2
#define DRM_PSB_DISP_INIT_HDMI_FLIP_CHAIN       1
#define DRM_PSB_DISP_QUEUE_BUFFER               2
#define DRM_PSB_DISP_DEQUEUE_BUFFER             3
#define DRM_PSB_DISP_PLANEB_DISABLE             4
#define DRM_PSB_DISP_PLANEB_ENABLE              5
#define DRM_PSB_HDMI_OSPM_ISLAND_DOWN           6
#define DRM_PSB_HDMI_NOTIFY_HOTPLUG_TO_AUDIO    7

/*csc gamma setting*/
typedef enum {
	GAMMA,
	CSC,
	GAMMA_INITIA,
	GAMMA_SETTING,
	GAMMA_REG_SETTING,
	CSC_INITIA,
	CSC_CHROME_SETTING,
	CSC_SETTING,
	CSC_REG_SETTING
} setting_type;

typedef enum {
	/* gamma 0.5 */
	GAMMA_05 = 1,
	/* gamma 2.0 */
	GAMMA_20,
	/* gamma 0.5 + 2.0*/
	GAMMA_05_20,
	/* gamma 2.0 + 0.5*/
	GAMMA_20_05,
	/* gamma 1.0 */
	GAMMA_10
} gamma_mode;

#define CSC_REG_COUNT                   6
#define CHROME_COUNT                    16
#define CSC_COUNT                       9

struct csc_setting {
	uint32_t pipe;
	setting_type type;
	bool enable_state;
	uint32_t data_len;
	union {
		int csc_reg_data[CSC_REG_COUNT];
		int chrome_data[CHROME_COUNT];
		int64_t csc_data[CSC_COUNT];
	} data;
};
#define GAMMA_10_BIT_TABLE_COUNT        129

struct gamma_setting {
	uint32_t pipe;
	setting_type type;
	bool enable_state;
	gamma_mode initia_mode;
	uint32_t data_len;
	uint32_t gamma_tableX100[GAMMA_10_BIT_TABLE_COUNT];
};
struct drm_psb_csc_gamma_setting {
	setting_type type;
	union {
		struct csc_setting csc_data;
		struct gamma_setting gamma_data;
	} data;
};
struct drm_psb_buffer_data {
	void *h_buffer;
};
struct drm_psb_flip_chain_data {
	void **h_buffer_array;
	unsigned int size;
};
struct drm_psb_disp_ctrl {
	uint32_t cmd;
	union {
		uint32_t data;
		struct drm_psb_buffer_data buf_data;
		struct drm_psb_flip_chain_data flip_chain_data;
	} u;
};

/* Merrifield driver specific interface */

#define S3D_MIPIA_DISPLAY               0
#define S3D_HDMI_DISPLAY                1
#define S3D_MIPIC_DISPLAY               2
#define S3D_WIDI_DISPLAY                0xFF

struct drm_psb_s3d_query {
	uint32_t s3d_display_type;
	uint32_t is_s3d_supported;
	uint32_t s3d_format;
	uint32_t mode_resolution_x;
	uint32_t mode_resolution_y;
	uint32_t mode_refresh_rate;
	uint32_t is_interleaving;
};

struct drm_psb_s3d_premodeset {
	uint32_t s3d_buffer_format;
};


typedef enum intel_dc_plane_types {
	DC_UNKNOWN_PLANE = 0,
	DC_SPRITE_PLANE = 1,
	DC_OVERLAY_PLANE,
	DC_PRIMARY_PLANE,
	DC_PLANE_MAX,
} DC_MRFLD_PLANE_TYPE;

#define SPRITE_UPDATE_SURFACE           (0x00000001UL)
#define SPRITE_UPDATE_CONTROL           (0x00000002UL)
#define SPRITE_UPDATE_POSITION          (0x00000004UL)
#define SPRITE_UPDATE_SIZE              (0x00000008UL)
#define SPRITE_UPDATE_WAIT_VBLANK       (0X00000010UL)
#define SPRITE_UPDATE_CONSTALPHA        (0x00000020UL)
#define SPRITE_UPDATE_ALL               (0x0000003fUL)
#define MRFLD_PRIMARY_COUNT             3

typedef struct intel_dc_overlay_ctx {
	uint32_t index;
	uint32_t pipe;
	uint32_t ovadd;
} DC_MRFLD_OVERLAY_CONTEXT;

typedef struct intel_dc_sprite_ctx {
	uint32_t update_mask;
	/* plane index 0-A, 1-B, 2-C,etc */
	uint32_t index;
	uint32_t pipe;

	uint32_t cntr;
	uint32_t linoff;
	uint32_t stride;
	uint32_t pos;
	uint32_t size;
	uint32_t keyminval;
	uint32_t keymask;
	uint32_t surf;
	uint32_t keymaxval;
	uint32_t tileoff;
	uint32_t contalpa;
} DC_MRFLD_SPRITE_CONTEXT;

typedef struct intel_dc_primary_ctx {
	uint32_t update_mask;
	/* plane index 0-A, 1-B, 2-C,etc */
	uint32_t index;
	uint32_t pipe;
	uint32_t cntr;
	uint32_t linoff;
	uint32_t stride;
	uint32_t pos;
	uint32_t size;
	uint32_t keyminval;
	uint32_t keymask;
	uint32_t surf;
	uint32_t keymaxval;
	uint32_t tileoff;
	uint32_t contalpa;
} DC_MRFLD_PRIMARY_CONTEXT;

typedef struct intel_dc_plane_zorder {
	/* 3 primary planes */
	uint32_t forceBottom[3];
	/* 1 sprite plane */
	uint32_t abovePrimary;
} DC_MRFLD_DC_PLANE_ZORDER;

typedef struct intel_dc_plane_ctx {
	enum intel_dc_plane_types type;
	struct intel_dc_plane_zorder zorder;
	union {
		struct intel_dc_overlay_ctx ov_ctx;
		struct intel_dc_sprite_ctx sp_ctx;
		struct intel_dc_primary_ctx prim_ctx;
	} ctx;
} DC_MRFLD_SURF_CUSTOM;

/* FIXME: move this into a medfield header once we are sure it isn't needed for an
   ioctl  */
struct psb_drm_dpu_rect {  
	int x, y;             
	int width, height;    
};  

struct drm_psb_drv_dsr_off_arg {
	int screen;
	struct psb_drm_dpu_rect damage_rect;
};

struct drm_psb_gem_create {
	__u64 size;
	__u32 handle;
	__u32 flags;
#define PSB_GEM_CREATE_STOLEN		1	/* Stolen memory can be used */
};

#define PSB_2D_OP_BUFLEN		16

struct drm_psb_2d_op {
	__u32 src;		/* Handles, only src supported right now */
	__u32 dst;
	__u32 mask;
	__u32 pat;
	__u32 size;		/* In dwords of command */
	__u32 spare;		/* And bumps array to u64 align */
	__u32 cmd[PSB_2D_OP_BUFLEN];
};

struct drm_psb_gem_mmap {
	__u32 handle;
	__u32 pad;
	/**
	 * Fake offset to use for subsequent mmap call
	 *
	 * This is a fixed-size type for 32/64 compatibility.
	 */
	__u64 offset;
};

#endif
