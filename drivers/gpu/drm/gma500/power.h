/**************************************************************************
 * Copyright (c) 2009-2011, Intel Corporation.
 * All Rights Reserved.

 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Authors:
 *    Benjamin Defnet <benjamin.r.defnet@intel.com>
 *    Rajesh Poornachandran <rajesh.poornachandran@intel.com>
 * Massively reworked
 *    Alan Cox <alan@linux.intel.com>
 */
#ifndef _PSB_POWERMGMT_H_
#define _PSB_POWERMGMT_H_

#include <linux/pci.h>
#include <drm/drm_crtc.h>

/* Register Type definitions */
#define OSPM_REG_TYPE          0x0
#define APM_REG_TYPE           0x1
#define OSPM_MAX_POWER_ISLANDS 16
#define OSPM_ISLAND_UP         0x0
#define OSPM_ISLAND_DOWN 0x1

/* North complex power islands definitions for OSPM block*/
#define OSPM_DISPLAY_A_ISLAND  0x2
#define OSPM_DISPLAY_B_ISLAND  0x80
#define OSPM_DISPLAY_C_ISLAND  0x100
#define OSPM_MIPI_ISLAND 0x200

#define OSPM_GRAPHICS_ISLAND	APM_GRAPHICS_ISLAND
#ifndef CONFIG_DRM_VXD_BYT
#define OSPM_VIDEO_DEC_ISLAND	APM_VIDEO_DEC_ISLAND
#endif
#define OSPM_VIDEO_ENC_ISLAND	APM_VIDEO_ENC_ISLAND
#define OSPM_GL3_CACHE_ISLAND	APM_GL3_CACHE_ISLAND
#define OSPM_DISPLAY_ISLAND	0x40

#ifdef CONFIG_MDFD_GL3
#define OSPM_ALL_ISLANDS	((OSPM_GRAPHICS_ISLAND) |\
				(OSPM_VIDEO_ENC_ISLAND) |\
				(OSPM_VIDEO_DEC_ISLAND) |\
				(OSPM_GL3_CACHE_ISLAND) |\
				(OSPM_DISPLAY_ISLAND))
#else
#define OSPM_ALL_ISLANDS	((OSPM_GRAPHICS_ISLAND) |\
				(OSPM_VIDEO_ENC_ISLAND) |\
				(OSPM_VIDEO_DEC_ISLAND) |\
				(OSPM_DISPLAY_ISLAND))
#endif
/* IPC message and command defines used to enable/disable mipi panel voltages */
#define IPC_MSG_PANEL_ON_OFF    0xE9
#define IPC_CMD_PANEL_ON        1
#define IPC_CMD_PANEL_OFF       0

/* Panel presence */
#define DISPLAY_A 0x1
#define DISPLAY_B 0x2
#define DISPLAY_C 0x4

extern int lastFailedBrightness;

struct mdfld_dsi_config;
void mdfld_save_display(struct drm_device *dev);
void mdfld_dsi_dbi_set_power(struct drm_encoder *encoder, bool on);

void ospm_post_init(struct drm_device *dev);
void ospm_subsystem_no_gating(struct drm_device *dev, int subsystem);
void ospm_subsystem_power_gate(struct drm_device *dev, int subsystem);

void gma_power_init(struct drm_device *dev);
void gma_power_uninit(struct drm_device *dev);

/*
 * The kernel bus power management  will call these functions
 */
int gma_power_suspend(struct device *dev);
int gma_power_resume(struct device *dev);

/*
 * These are the functions the driver should use to wrap all hw access
 * (i.e. register reads and writes)
 */
bool gma_power_begin(struct drm_device *dev, bool force);
void gma_power_end(struct drm_device *dev);

/*
 * Power up/down different hw component rails/islands
 */
void mdfld_save_display(struct drm_device *dev);
void ospm_power_island_down(struct drm_device *dev);
int ospm_power_island_up(struct drm_device *dev);
void ospm_suspend_graphics(void);
void ospm_power_graphics_island_down(int hw_islands);
void ospm_power_graphics_island_up(int hw_islands);

/*
 * GFX-Runtime PM callbacks
 */
int psb_runtime_suspend(struct device *dev);
int psb_runtime_resume(struct device *dev);
int psb_runtime_idle(struct device *dev);
int ospm_runtime_pm_allow(struct drm_device *dev);
void ospm_runtime_pm_forbid(struct drm_device *dev);
void acquire_ospm_lock(void);
void release_ospm_lock(void);

#endif /*_PSB_POWERMGMT_H_*/
