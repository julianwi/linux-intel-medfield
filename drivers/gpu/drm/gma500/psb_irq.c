// SPDX-License-Identifier: GPL-2.0-only
/**************************************************************************
 * Copyright (c) 2007, Intel Corporation.
 * All Rights Reserved.
 *
 * Intel funded Tungsten Graphics (http://www.tungstengraphics.com) to
 * develop this driver.
 *
 **************************************************************************/

#include <drm/drm_drv.h>
#include <drm/drm_vblank.h>

#include "mdfld_output.h"
#include "power.h"
#include "psb_drv.h"
#include "psb_intel_reg.h"
#include "psb_irq.h"
#include "psb_reg.h"

#include "mdfld_dsi_dbi_dpu.h"
#include "mdfld_dsi_pkg_sender.h"
#include "mdfld_dsi_dbi_dsr.h"

#include <linux/time.h>
#include <linux/sched/clock.h>

extern int drm_psb_smart_vsync;
extern atomic_t g_videoenc_access_count;
extern atomic_t g_videodec_access_count;
extern struct workqueue_struct *te_wq;
extern struct workqueue_struct *vsync_wq;

/*
 * inline functions
 */

static inline u32 gma_pipestat(int pipe)
{
	if (pipe == 0)
		return PIPEASTAT;
	if (pipe == 1)
		return PIPEBSTAT;
	if (pipe == 2)
		return PIPECSTAT;
	BUG();
}

static inline u32 gma_pipe_event(int pipe)
{
	if (pipe == 0)
		return _PSB_PIPEA_EVENT_FLAG;
	if (pipe == 1)
		return _MDFLD_PIPEB_EVENT_FLAG;
	if (pipe == 2)
		return _MDFLD_PIPEC_EVENT_FLAG;
	BUG();
}

static inline u32 gma_pipeconf(int pipe)
{
	if (pipe == 0)
		return PIPEACONF;
	if (pipe == 1)
		return PIPEBCONF;
	if (pipe == 2)
		return PIPECCONF;
	BUG();
}

void gma_enable_pipestat(struct drm_psb_private *dev_priv, int pipe, u32 mask)
{
	if ((dev_priv->pipestat[pipe] & mask) != mask) {
		u32 reg = gma_pipestat(pipe);
		dev_priv->pipestat[pipe] |= mask;
		/* Enable the interrupt, clear any pending status */
		if (gma_power_begin(&dev_priv->dev, false)) {
			u32 writeVal = PSB_RVDC32(reg);
			writeVal |= (mask | (mask >> 16));
			PSB_WVDC32(writeVal, reg);
			(void) PSB_RVDC32(reg);
			gma_power_end(&dev_priv->dev);
		}
	}
}

void gma_disable_pipestat(struct drm_psb_private *dev_priv, int pipe, u32 mask)
{
	if ((dev_priv->pipestat[pipe] & mask) != 0) {
		u32 reg = gma_pipestat(pipe);
		dev_priv->pipestat[pipe] &= ~mask;
		if (gma_power_begin(&dev_priv->dev, false)) {
			u32 writeVal = PSB_RVDC32(reg);
			writeVal &= ~mask;
			PSB_WVDC32(writeVal, reg);
			(void) PSB_RVDC32(reg);
			gma_power_end(&dev_priv->dev);
		}
	}
}

void mid_enable_pipe_event(struct drm_psb_private *dev_priv, int pipe)
{
	if (gma_power_begin(&dev_priv->dev, false)) {
		u32 pipe_event = gma_pipe_event(pipe);
		dev_priv->vdc_irq_mask |= pipe_event;
		PSB_WVDC32(~dev_priv->vdc_irq_mask, PSB_INT_MASK_R);
		PSB_WVDC32(dev_priv->vdc_irq_mask, PSB_INT_ENABLE_R);
		gma_power_end(&dev_priv->dev);
	}
}

static void mid_disable_pipe_event(struct drm_psb_private *dev_priv, int pipe)
{
	if (dev_priv->pipestat[pipe] == 0) {
		if (gma_power_begin(&dev_priv->dev, false)) {
			u32 pipe_event = gma_pipe_event(pipe);
			dev_priv->vdc_irq_mask &= ~pipe_event;
			PSB_WVDC32(~dev_priv->vdc_irq_mask, PSB_INT_MASK_R);
			PSB_WVDC32(dev_priv->vdc_irq_mask, PSB_INT_ENABLE_R);
			gma_power_end(&dev_priv->dev);
		}
	}
}
/*
 * to sync the mipi and hdmi irq to ensure they flush the same buffer
 *
 */
static int mipi_hdmi_vsync_check(struct drm_device *dev, uint32_t pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct mdfld_dsi_config *dsi_config = dev_priv->dsi_configs[0];
	static int pipe_surf[2];
	int pipea_stat = 0;
	int pipeb_stat = 0;
	int pipeb_ctl = 0;
	int pipeb_cntr = 0;
	unsigned long irqflags;

	/*check whether need to sync*/
	if (dev_priv->vsync_te_working[0] == false ||
		dev_priv->vsync_te_working[1] == false)
		return 1;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);
	if (dev_priv->bhdmiconnected && dsi_config->dsi_hw_context.panel_on) {
		if (gma_power_begin(&dev_priv->dev, false)) {
			pipea_stat = REG_READ(gma_pipestat(0));
			pipeb_stat = REG_READ(gma_pipestat(1));
			pipeb_ctl = REG_READ(HDMIB_CONTROL);
			pipeb_cntr = REG_READ(DSPBCNTR);
			pipe_surf[pipe] = REG_READ(DSPASURF);
			gma_power_end(dev);
		} else {
			spin_unlock_irqrestore(&dev_priv->irqmask_lock,
							irqflags);
			return 1;
		}

		if ((pipea_stat & PIPE_VBLANK_INTERRUPT_ENABLE)
		    && (pipeb_ctl & HDMIB_PORT_EN)
		    && (pipeb_cntr & DISPLAY_PLANE_ENABLE)
		    && (pipeb_stat & PIPE_VBLANK_INTERRUPT_ENABLE)) {
			if (pipe_surf[0] == pipe_surf[1]) {
				pipe_surf[0] = MAX_NUM;
				pipe_surf[1] = MAX_NUM;
			} else {
				spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);
				return 0;
			}
		}
	}
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);

	return 1;
}

/* Function to check if HDMI and MIPI are presenting the same buffer.
 * If MIPI is faster than HDMI (different refresh rates), then throttle
 * the MIPI buffers by simply dropping them.
 * This function should be called from the local display's Vblank/TE
 * interrupt handler. This version of function is required to handle
 * the sync check on CTP, on which local display works on the TE interrupt
 * handler.
 */
static int mipi_te_hdmi_vsync_check(struct drm_device *dev, uint32_t pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct mdfld_dsi_config *dsi_config = dev_priv->dsi_configs[0];
	int pipea_stat, pipeb_stat, pipeb_ctl, pipeb_cntr, pipeb_config;
	static int pipe_surf[2];
	unsigned long irqflags;

	/*check whether need to sync*/
	if (dev_priv->vsync_te_working[0] == false ||
		dev_priv->vsync_te_working[1] == false)
		return 1;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);
	if (dev_priv->bhdmiconnected && dsi_config->dsi_hw_context.panel_on) {
		if (gma_power_begin(dev, false)) {
			pipea_stat = REG_READ(gma_pipestat(0));
			pipeb_stat = REG_READ(gma_pipestat(1));
			pipeb_ctl = REG_READ(HDMIB_CONTROL);
			pipeb_cntr = REG_READ(DSPBCNTR);
			pipeb_config = REG_READ(PIPEBCONF);
			pipe_surf[pipe] = REG_READ(DSPASURF);
			gma_power_end(dev);
		} else {
			spin_unlock_irqrestore(&dev_priv->irqmask_lock,
					irqflags);
			return 1;
		}

		if ((pipea_stat & PIPE_TE_ENABLE)
			&& (pipeb_config & PIPEBCONF_ENABLE)
			&& (pipeb_ctl & HDMIB_PORT_EN)
			&& (pipeb_cntr & DISPLAY_PLANE_ENABLE)
			&& (pipeb_stat & PIPE_VBLANK_INTERRUPT_ENABLE)) {
			if (pipe_surf[0] == pipe_surf[1]) {
				pipe_surf[0] = MAX_NUM;
				pipe_surf[1] = MAX_NUM;
			} else {
				spin_unlock_irqrestore(&dev_priv->irqmask_lock,
						       irqflags);
				return 0;
			}
		}
	}
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);

	return 1;
}

/**
 * Display controller interrupt handler for vsync/vblank.
 *
 */
static void mid_vblank_handler(struct drm_device *dev, uint32_t pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
/*doesn't use it for now, leave code here for reference*/
/*if (pipe == 1)
		psb_flip_hdmi(dev, pipe);*/
	drm_handle_vblank(dev, pipe);

	if (is_cmd_mode_panel(dev)) {
		if (!mipi_te_hdmi_vsync_check(dev, pipe))
			return;
	} else {
		if (!mipi_hdmi_vsync_check(dev, pipe))
			return;
	}

	if (dev_priv->psb_vsync_handler)
		(*dev_priv->psb_vsync_handler)(dev, pipe);

	mutex_lock(&dev_priv->vsync_lock);
	if (pipe == 0 && !dev_priv->vsync_enabled &&
			 ++dev_priv->vblank_disable_cnt > 10) {
		struct drm_crtc *crtc = drm_crtc_from_index(dev, 0);
		gma_crtc_disable_vblank(crtc);
	}
	mutex_unlock(&dev_priv->vsync_lock);
}

static void mdfld_vsync_event(struct drm_device *dev, uint32_t pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);

	if (dev_priv)
		wake_up_interruptible(&dev_priv->vsync_queue);
}

void mdfld_vsync_event_work(struct work_struct *work)
{
	struct drm_psb_private *dev_priv =
		container_of(work, struct drm_psb_private, vsync_event_work);
	int pipe = dev_priv->vsync_pipe;
	struct drm_device *dev = &dev_priv->dev;

	dev_priv->vsync_te_worker_ts[pipe] = cpu_clock(0);

	mid_vblank_handler(dev, pipe);

	/*report vsync event*/
	mdfld_vsync_event(dev, pipe);
}

void mdfld_te_handler_work(struct work_struct *work)
{
	struct drm_psb_private *dev_priv =
		container_of(work, struct drm_psb_private, te_work);
	int pipe = 0;
	struct drm_device *dev = &dev_priv->dev;

	dev_priv->vsync_te_worker_ts[pipe] = cpu_clock(0);

	/*report vsync event*/
	mdfld_vsync_event(dev, pipe);

	drm_handle_vblank(dev, pipe);

	if (dev_priv->b_async_flip_enable) {
		if (mipi_te_hdmi_vsync_check(dev, pipe)) {
			if (dev_priv->psb_vsync_handler != NULL)
				(*dev_priv->psb_vsync_handler)(dev, pipe);
		}

		mdfld_dsi_dsr_report_te(dev_priv->dsi_configs[0]);
	} else {
#ifdef CONFIG_MDFD_DSI_DPU
		mdfld_dpu_update_panel(dev);
#else
		mdfld_dbi_update_panel(dev, pipe);
#endif
		if (dev_priv->psb_vsync_handler != NULL)
			(*dev_priv->psb_vsync_handler)(dev, pipe);
	}
}

enum hrtimer_restart intel_dbi_fifo_query_timer_func(struct hrtimer *timer)
{
	struct drm_psb_private *dev_priv = container_of(timer,
							struct drm_psb_private,
							dbi_fifo_query_hrtimer);
	if (dev_priv && (atomic_cmpxchg(&dev_priv->dbi_fifo_query, 0, 1) == 0)) {
		wake_up_interruptible(&dev_priv->dbi_fifo_query_queue);
	}

	 return HRTIMER_NORESTART;
}

#define MS_TO_NS(x)    (x * 1E6L)

static void intel_dbi_fifo_query_timer_start(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long flags;
	ktime_t ktime;
	unsigned long delay_in_ms = 13;

	if (!dev_priv)
		return;

	spin_lock_irqsave(&dev_priv->dbi_fifo_query_timer_lock, flags);

	if (!hrtimer_is_queued(&dev_priv->dbi_fifo_query_hrtimer)) {
		ktime = ktime_set(0, MS_TO_NS(delay_in_ms));
		hrtimer_start(&dev_priv->dbi_fifo_query_hrtimer, ktime,
			HRTIMER_MODE_REL);
	}

	spin_unlock_irqrestore(&dev_priv->dbi_fifo_query_timer_lock, flags);
}


static void update_te_counter(struct drm_device *dev, uint32_t pipe)
{
	struct mdfld_dsi_pkg_sender *sender;
	struct mdfld_dsi_dbi_output **dbi_outputs;
	struct mdfld_dsi_dbi_output *dbi_output;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct mdfld_dbi_dsr_info *dsr_info = dev_priv->dbi_dsr_info;

	if (!dsr_info)
		return;

	dbi_outputs = dsr_info->dbi_outputs;
	dbi_output = pipe ? dbi_outputs[1] : dbi_outputs[0];
	sender = mdfld_dsi_encoder_get_pkg_sender(&dbi_output->base);
	mdfld_dsi_report_te(sender);
}
static void get_use_cases_control_info(void)
{
	static int last_use_cases_control = -1;
	if (drm_psb_use_cases_control != last_use_cases_control) {
		last_use_cases_control = drm_psb_use_cases_control;
		DRM_INFO("new update of use cases\n");
		if (!(drm_psb_use_cases_control & PSB_SUSPEND_ENABLE))
			DRM_INFO("BIT0 suspend/resume  disabled\n");
		if (!(drm_psb_use_cases_control & PSB_BRIGHTNESS_ENABLE))
			DRM_INFO("BIT1 brighness setting disabled\n");
		if (!(drm_psb_use_cases_control & PSB_ESD_ENABLE))
			DRM_INFO("BIT2 ESD  disabled\n");
		if (!(drm_psb_use_cases_control & PSB_DPMS_ENABLE))
			DRM_INFO("BIT3 DPMS  disabled\n");
		if (!(drm_psb_use_cases_control & PSB_DSR_ENABLE))
			DRM_INFO("BIT4 DSR disabled\n");
		if (!(drm_psb_use_cases_control & PSB_VSYNC_OFF_ENABLE))
			DRM_INFO("BIT5 VSYNC off  disabled\n");
	}
}

/*
 * Display controller interrupt handler for pipe event.
 */
#define WAIT_STATUS_CLEAR_LOOP_COUNT 0xffff
static void gma_pipe_event_handler(struct drm_device *dev, uint32_t pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);

	uint32_t pipe_stat_val = 0;
	uint32_t pipe_stat_val_raw;
	uint32_t pipe_stat_reg = gma_pipestat(pipe);
	uint32_t pipe_enable = dev_priv->pipestat[pipe];
	uint32_t pipe_status = dev_priv->pipestat[pipe] >> 16;
	uint32_t i = 0;
	unsigned long irq_flags;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irq_flags);

	pipe_stat_val = PSB_RVDC32(pipe_stat_reg);
	pipe_stat_val_raw = pipe_stat_val;
	pipe_stat_val &= pipe_enable | pipe_status;
	pipe_stat_val &= pipe_stat_val >> 16;

	/* clear the 2nd level interrupt status bits.
	 * We must keep spinlock to protect disable / enable status
	 * safe in the same register (assuming that other do that also)
	 * Clear only bits that we are going to serve this time.
	 */
	/**
	* FIXME: shouldn't use while loop here. However, the interrupt
	* status 'sticky' bits cannot be cleared by setting '1' to that
	* bit once...
	*/
	for (i = 0; i < WAIT_STATUS_CLEAR_LOOP_COUNT; i++) {
		PSB_WVDC32(pipe_stat_val_raw, pipe_stat_reg);
		(void) PSB_RVDC32(pipe_stat_reg);

		if ((PSB_RVDC32(pipe_stat_reg) & pipe_stat_val) == 0)
			break;
	}
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irq_flags);

	if (i == WAIT_STATUS_CLEAR_LOOP_COUNT)
		dev_err(dev->dev,
	"%s, can't clear the status bits in pipe_stat_reg, its value = 0x%x.\n",
			__func__, PSB_RVDC32(pipe_stat_reg));

	/* if pipe is HDMI, but hdmi is plugged out, should not handle
	* HDMI reg any more here */
	if (pipe == 1 && !dev_priv->bhdmiconnected)
		return;

	if (pipe_stat_val & PIPE_VBLANK_STATUS) {
		struct drm_crtc *crtc = drm_crtc_from_index(dev, pipe);
		struct gma_crtc *gma_crtc = to_gma_crtc(crtc);
		unsigned long flags;
		dev_priv->vsync_te_irq_ts[pipe] = cpu_clock(0);
		dev_priv->vsync_te_working[pipe] = true;
		dev_priv->vsync_pipe = pipe;
		atomic_inc(&dev_priv->vblank_count[pipe]);
		queue_work(vsync_wq, &dev_priv->vsync_event_work);

		spin_lock_irqsave(&dev->event_lock, flags);
		if (gma_crtc->page_flip_event) {
			drm_crtc_send_vblank_event(crtc,
						   gma_crtc->page_flip_event);
			gma_crtc->page_flip_event = NULL;
			drm_crtc_vblank_put(crtc);
		}
		spin_unlock_irqrestore(&dev->event_lock, flags);
	}

	if (pipe_stat_val & PIPE_TE_STATUS) {
		/*update te sequence on this pipe*/
		struct drm_crtc *crtc = drm_crtc_from_index(dev, pipe);
		struct gma_crtc *gma_crtc = to_gma_crtc(crtc);
		unsigned long flags;
		intel_dbi_fifo_query_timer_start(dev);
		dev_priv->vsync_te_irq_ts[pipe] = cpu_clock(0);
		dev_priv->vsync_te_working[pipe] = true;
		update_te_counter(dev, pipe);
		atomic_inc(&dev_priv->vblank_count[pipe]);
		queue_work(te_wq, &dev_priv->te_work);

		spin_lock_irqsave(&dev->event_lock, flags);
		if (gma_crtc->page_flip_event) {
			drm_crtc_send_vblank_event(crtc,
						   gma_crtc->page_flip_event);
			gma_crtc->page_flip_event = NULL;
			drm_crtc_vblank_put(crtc);
		}
		spin_unlock_irqrestore(&dev->event_lock, flags);
	}

	get_use_cases_control_info();
}

/*
 * Display controller interrupt handler.
 */
static void gma_vdc_interrupt(struct drm_device *dev, uint32_t vdc_stat)
{
	if (vdc_stat & _PSB_PIPEA_EVENT_FLAG)
		gma_pipe_event_handler(dev, 0);

	if (vdc_stat & _MDFLD_PIPEB_EVENT_FLAG)
		gma_pipe_event_handler(dev, 1);

	if (vdc_stat & _MDFLD_PIPEC_EVENT_FLAG) {
		gma_pipe_event_handler(dev, 2);
	}

	if (vdc_stat & _MDFLD_MIPIA_FLAG) {
		/* mid_mipi_event_handler(dev, 0); */
	}

	if (vdc_stat & _MDFLD_MIPIC_FLAG) {
		/* mid_mipi_event_handler(dev, 2); */
	}
}

/*
 * SGX interrupt handler
 */
static void gma_sgx_interrupt(struct drm_device *dev, u32 stat_1, u32 stat_2)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	u32 val, addr;

	if (stat_1 & _PSB_CE_TWOD_COMPLETE)
		val = PSB_RSGX32(PSB_CR_2D_BLIT_STATUS);

	if (stat_2 & _PSB_CE2_BIF_REQUESTER_FAULT) {
		val = PSB_RSGX32(PSB_CR_BIF_INT_STAT);
		addr = PSB_RSGX32(PSB_CR_BIF_FAULT);
		if (val) {
			if (val & _PSB_CBI_STAT_PF_N_RW)
				DRM_ERROR("SGX MMU page fault:");
			else
				DRM_ERROR("SGX MMU read / write protection fault:");

			if (val & _PSB_CBI_STAT_FAULT_CACHE)
				DRM_ERROR("\tCache requestor");
			if (val & _PSB_CBI_STAT_FAULT_TA)
				DRM_ERROR("\tTA requestor");
			if (val & _PSB_CBI_STAT_FAULT_VDM)
				DRM_ERROR("\tVDM requestor");
			if (val & _PSB_CBI_STAT_FAULT_2D)
				DRM_ERROR("\t2D requestor");
			if (val & _PSB_CBI_STAT_FAULT_PBE)
				DRM_ERROR("\tPBE requestor");
			if (val & _PSB_CBI_STAT_FAULT_TSP)
				DRM_ERROR("\tTSP requestor");
			if (val & _PSB_CBI_STAT_FAULT_ISP)
				DRM_ERROR("\tISP requestor");
			if (val & _PSB_CBI_STAT_FAULT_USSEPDS)
				DRM_ERROR("\tUSSEPDS requestor");
			if (val & _PSB_CBI_STAT_FAULT_HOST)
				DRM_ERROR("\tHost requestor");

			DRM_ERROR("\tMMU failing address is 0x%08x.\n",
				  (unsigned int)addr);
		}
	}

	/* Clear bits */
	PSB_WSGX32(stat_1, PSB_CR_EVENT_HOST_CLEAR);
	PSB_WSGX32(stat_2, PSB_CR_EVENT_HOST_CLEAR2);
	PSB_RSGX32(PSB_CR_EVENT_HOST_CLEAR2);
}

static irqreturn_t gma_irq_handler(int irq, void *arg)
{
	struct drm_device *dev = arg;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	uint32_t vdc_stat, dsp_int = 0, sgx_int = 0, hotplug_int = 0;
	u32 sgx_stat_1, sgx_stat_2;
	int handled = 0;
	unsigned long irq_flags;
	//struct saved_history_record *precord = NULL;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irq_flags);

	vdc_stat = PSB_RVDC32(PSB_INT_IDENTITY_R);

	if (vdc_stat & _MDFLD_DISP_ALL_IRQ_FLAG)
		dsp_int = 1;

	if (vdc_stat & _PSB_IRQ_SGX_FLAG)
		sgx_int = 1;
	if (vdc_stat & _PSB_IRQ_DISP_HOTSYNC)
		hotplug_int = 1;

	vdc_stat &= dev_priv->vdc_irq_mask;
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irq_flags);

	/*
		Ignore interrupt if sub-system is already
		powered gated; nothing needs to be done,
		when HW is already power-gated
		- saftey check to avoid illegal HW access.
	*/
	if (dsp_int) {
		if (gma_power_begin(dev, false)) {
			gma_vdc_interrupt(dev, vdc_stat);
			handled = 1;
			gma_power_end(dev);
		} else
			DRM_INFO("get dsp int while it's off\n");
	}

	if (sgx_int) {
		sgx_stat_1 = PSB_RSGX32(PSB_CR_EVENT_STATUS);
		sgx_stat_2 = PSB_RSGX32(PSB_CR_EVENT_STATUS2);
		gma_sgx_interrupt(dev, sgx_stat_1, sgx_stat_2);
		handled = 1;
	}

	PSB_WVDC32(vdc_stat, PSB_INT_IDENTITY_R);
	(void) PSB_RVDC32(PSB_INT_IDENTITY_R);
	rmb();

	if (!handled)
		return IRQ_NONE;

	return IRQ_HANDLED;
}

void gma_irq_preinstall(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	PSB_WVDC32(0xFFFFFFFF, PSB_HWSTAM);
	PSB_WVDC32(0x00000000, PSB_INT_MASK_R);
	PSB_WVDC32(0x00000000, PSB_INT_ENABLE_R);
	PSB_WSGX32(0x00000000, PSB_CR_EVENT_HOST_ENABLE);
	PSB_RSGX32(PSB_CR_EVENT_HOST_ENABLE);

	if (dev->vblank[0].enabled)
		dev_priv->vdc_irq_mask |= _PSB_VSYNC_PIPEA_FLAG;
	if (dev->vblank[1].enabled)
		dev_priv->vdc_irq_mask |= _PSB_VSYNC_PIPEB_FLAG;

	/* FIXME: Handle Medfield irq mask
	if (dev->vblank[1].enabled)
		dev_priv->vdc_irq_mask |= _MDFLD_PIPEB_EVENT_FLAG;
	if (dev->vblank[2].enabled)
		dev_priv->vdc_irq_mask |= _MDFLD_PIPEC_EVENT_FLAG;
	*/

	/* Revisit this area - want per device masks ? */
	if (dev_priv->ops->hotplug)
		dev_priv->vdc_irq_mask |= _PSB_IRQ_DISP_HOTSYNC;
	dev_priv->vdc_irq_mask |= _PSB_IRQ_ASLE | _PSB_IRQ_SGX_FLAG;

	/* This register is safe even if display island is off */
	PSB_WVDC32(~dev_priv->vdc_irq_mask, PSB_INT_MASK_R);
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);
}

void gma_irq_postinstall(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;
	unsigned int i;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	/* Enable 2D and MMU fault interrupts */
	PSB_WSGX32(_PSB_CE2_BIF_REQUESTER_FAULT, PSB_CR_EVENT_HOST_ENABLE2);
	PSB_WSGX32(_PSB_CE_TWOD_COMPLETE, PSB_CR_EVENT_HOST_ENABLE);
	PSB_RSGX32(PSB_CR_EVENT_HOST_ENABLE); /* Post */

	/* This register is safe even if display island is off */
	PSB_WVDC32(dev_priv->vdc_irq_mask, PSB_INT_ENABLE_R);
	PSB_WVDC32(0xFFFFFFFF, PSB_HWSTAM);

	for (i = 0; i < dev->num_crtcs; ++i) {
		if (dev->vblank[i].enabled)
			gma_enable_pipestat(dev_priv, i, PIPE_VBLANK_INTERRUPT_ENABLE);
		else
			gma_disable_pipestat(dev_priv, i, PIPE_VBLANK_INTERRUPT_ENABLE);
	}

	if (dev_priv->ops->hotplug_enable)
		dev_priv->ops->hotplug_enable(dev, true);

	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);
}

int gma_irq_install(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct pci_dev *pdev = to_pci_dev(dev->dev);
	int ret;

	if (dev_priv->use_msi && pci_enable_msi(pdev)) {
		dev_warn(dev->dev, "Enabling MSI failed!\n");
		dev_priv->use_msi = false;
	}

	if (pdev->irq == IRQ_NOTCONNECTED)
		return -ENOTCONN;

	gma_irq_preinstall(dev);

	/* PCI devices require shared interrupts. */
	ret = request_irq(pdev->irq, gma_irq_handler, IRQF_SHARED, dev->driver->name, dev);
	if (ret)
		return ret;

	gma_irq_postinstall(dev);

	return 0;
}

void gma_irq_uninstall(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct pci_dev *pdev = to_pci_dev(dev->dev);
	unsigned long irqflags;
	unsigned int i;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	if (dev_priv->ops->hotplug_enable)
		dev_priv->ops->hotplug_enable(dev, false);

	PSB_WVDC32(0xFFFFFFFF, PSB_HWSTAM);

	for (i = 0; i < dev->num_crtcs; ++i) {
		if (dev->vblank[i].enabled)
			gma_disable_pipestat(dev_priv, i, PIPE_VBLANK_INTERRUPT_ENABLE);
	}

	dev_priv->vdc_irq_mask &= _PSB_IRQ_SGX_FLAG |
				  _PSB_IRQ_MSVDX_FLAG |
				  _LNC_IRQ_TOPAZ_FLAG;

	/* These two registers are safe even if display island is off */
	PSB_WVDC32(~dev_priv->vdc_irq_mask, PSB_INT_MASK_R);
	PSB_WVDC32(dev_priv->vdc_irq_mask, PSB_INT_ENABLE_R);

	wmb();

	/* This register is safe even if display island is off */
	PSB_WVDC32(PSB_RVDC32(PSB_INT_IDENTITY_R), PSB_INT_IDENTITY_R);
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);

	free_irq(pdev->irq, dev);
	if (dev_priv->use_msi)
		pci_disable_msi(pdev);
}

int gma_crtc_enable_vblank(struct drm_crtc *crtc)
{
	struct drm_device *dev = crtc->dev;
	unsigned int pipe = crtc->index;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;
	uint32_t reg_val = 0;
	uint32_t pipeconf_reg = gma_pipeconf(pipe);

	if (IS_MDFLD(dev) && (dev_priv->platform_rev_id != MDFLD_PNW_A0) &&
	    is_cmd_mode_panel(dev) && (pipe != 1))
		return mdfld_enable_te(dev, pipe);

	if (IS_MDFLD(dev) && (dev_priv->platform_rev_id != MDFLD_PNW_A0) &&
	    is_cmd_mode_panel(dev) && (pipe != 1))
		return mdfld_enable_te(dev, pipe);

	if (gma_power_begin(dev, false)) {
		reg_val = REG_READ(pipeconf_reg);
		gma_power_end(dev);
	}

	if (!(reg_val & PIPEACONF_ENABLE)) {
		dev_err(dev->dev, "%s: Pipe config hasn't been enabled for pipe %d\n",
				__func__, pipe);
		return 0;
	}

	dev_priv->b_vblank_enable = true;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	mid_enable_pipe_event(dev_priv, pipe);
	gma_enable_pipestat(dev_priv, pipe, PIPE_VBLANK_INTERRUPT_ENABLE);

	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);

	return 0;
}

void gma_crtc_disable_vblank(struct drm_crtc *crtc)
{
	struct drm_device *dev = crtc->dev;
	unsigned int pipe = crtc->index;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;

	if (!(drm_psb_use_cases_control & PSB_VSYNC_OFF_ENABLE))
		return;

	if (IS_MDFLD(dev) && (dev_priv->platform_rev_id != MDFLD_PNW_A0) &&
	    is_cmd_mode_panel(dev) && (pipe != 1))
		mdfld_disable_te(dev, pipe);

	dev_priv->b_vblank_enable = false;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	dev_priv->vsync_te_working[pipe] = false;
	mid_disable_pipe_event(dev_priv, pipe);
	gma_disable_pipestat(dev_priv, pipe, PIPE_VBLANK_INTERRUPT_ENABLE);

	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);
}

/*
 * It is used to enable TE interrupt
 */
int mdfld_enable_te(struct drm_device *dev, int pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;
	uint32_t reg_val = 0;
	uint32_t pipeconf_reg = gma_pipeconf(pipe);

	if (gma_power_begin(dev, false)) {
		reg_val = REG_READ(pipeconf_reg);
		gma_power_end(dev);
	}

	if (!(reg_val & PIPEACONF_ENABLE))
		return -EINVAL;

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	mid_enable_pipe_event(dev_priv, pipe);
	gma_enable_pipestat(dev_priv, pipe, PIPE_TE_ENABLE);

	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);

	return 0;
}

/*
 * It is used to disable TE interrupt
 */
void mdfld_disable_te(struct drm_device *dev, int pipe)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	unsigned long irqflags;
	struct mdfld_dsi_pkg_sender *sender;
	struct mdfld_dsi_config *dsi_config = dev_priv->dsi_configs[0];

	spin_lock_irqsave(&dev_priv->irqmask_lock, irqflags);

	mid_disable_pipe_event(dev_priv, pipe);
	gma_disable_pipestat(dev_priv, pipe,
    		(PIPE_TE_ENABLE | PIPE_DPST_EVENT_ENABLE));

	if (dsi_config) {
		/*
		 * reset te_seq, which make sure te_seq is really
		 * increased by next te enable.
		 * reset te_seq to 1 instead of 0 will make sure
		 * that last_screen_update and te_seq are alwasys
		 * unequal when exiting from DSR.
		 */
		sender = mdfld_dsi_get_pkg_sender(dsi_config);
		atomic64_set(&sender->last_screen_update, 0);
		atomic64_set(&sender->te_seq, 1);
		dev_priv->vsync_te_working[pipe] = false;
		atomic_set(&dev_priv->mipi_flip_abnormal, 0);
	}
	spin_unlock_irqrestore(&dev_priv->irqmask_lock, irqflags);
}

/* Called from drm generic code, passed a 'crtc', which
 * we use as a pipe index
 */
u32 gma_crtc_get_vblank_counter(struct drm_crtc *crtc)
{
	struct drm_device *dev = crtc->dev;
	unsigned int pipe = crtc->index;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	uint32_t high_frame = PIPEAFRAMEHIGH;
	uint32_t low_frame = PIPEAFRAMEPIXEL;
	uint32_t pipeconf_reg = PIPEACONF;
	uint32_t reg_val = 0;
	uint32_t high1 = 0, high2 = 0, low = 0, count = 0;

	switch (pipe) {
	case 0:
		break;
	case 1:
		high_frame = PIPEBFRAMEHIGH;
		low_frame = PIPEBFRAMEPIXEL;
		pipeconf_reg = PIPEBCONF;
		break;
	case 2:
		high_frame = PIPECFRAMEHIGH;
		low_frame = PIPECFRAMEPIXEL;
		pipeconf_reg = PIPECCONF;
		break;
	default:
		dev_err(dev->dev, "%s, invalid pipe.\n", __func__);
		return 0;
	}

	if (!gma_power_begin(dev, false))
		return 0;

	if (pipe == 1 && dev_priv->bhdmiconnected == false) {
		dev_err(dev->dev, "trying to get vblank count for power off pipe %d\n",
									pipe);
		goto err_gma_power_end;
	}

	reg_val = REG_READ(pipeconf_reg);

	if (!(reg_val & PIPEACONF_ENABLE)) {
		dev_err(dev->dev, "trying to get vblank count for disabled pipe %u\n",
			pipe);
		goto err_gma_power_end;
	}

	/*
	 * High & low register fields aren't synchronized, so make sure
	 * we get a low value that's stable across two reads of the high
	 * register.
	 */
	do {
		high1 = ((REG_READ(high_frame) & PIPE_FRAME_HIGH_MASK) >>
			 PIPE_FRAME_HIGH_SHIFT);
		low =  ((REG_READ(low_frame) & PIPE_FRAME_LOW_MASK) >>
			PIPE_FRAME_LOW_SHIFT);
		high2 = ((REG_READ(high_frame) & PIPE_FRAME_HIGH_MASK) >>
			 PIPE_FRAME_HIGH_SHIFT);
	} while (high1 != high2);

	count = (high1 << 8) | low;

err_gma_power_end:
	gma_power_end(dev);

	return count;
}

