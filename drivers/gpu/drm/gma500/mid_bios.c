// SPDX-License-Identifier: GPL-2.0-only
/**************************************************************************
 * Copyright (c) 2011, Intel Corporation.
 * All Rights Reserved.
 *
 **************************************************************************/

/* TODO
 * - Split functions by vbt type
 * - Make them all take drm_device
 * - Check ioremap failures
 */

#include <drm/drm.h>
#include <linux/platform_device.h>

#include "mid_bios.h"
#include "psb_drv.h"
#include "psb_drm.h"
#include "mdfld_dsi_output.h"

static void mid_get_fuse_settings(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct pci_dev *pdev = to_pci_dev(dev->dev);
	struct pci_dev *pci_root =
		pci_get_domain_bus_and_slot(pci_domain_nr(pdev->bus),
					    0, 0);
	uint32_t fuse_value = 0;
	uint32_t fuse_value_tmp = 0;

#define FB_REG06 0xD0810600
#define FB_MIPI_DISABLE  (1 << 11)
#define FB_REG09 0xD0810900
#define FB_SKU_MASK  0x7000
#define FB_SKU_SHIFT 12
#define FB_SKU_100 0
#define FB_SKU_100L 1
#define FB_SKU_83 2
	if (pci_root == NULL) {
		WARN_ON(1);
		return;
	}


	pci_write_config_dword(pci_root, 0xD0, FB_REG06);
	pci_read_config_dword(pci_root, 0xD4, &fuse_value);

	/* FB_MIPI_DISABLE doesn't mean LVDS on with Medfield */
	if (IS_MRST(dev))
		dev_priv->iLVDS_enable = fuse_value & FB_MIPI_DISABLE;

	DRM_INFO("internal display is %s\n",
		 dev_priv->iLVDS_enable ? "LVDS display" : "MIPI display");

	 /* Prevent runtime suspend at start*/
	 if (dev_priv->iLVDS_enable) {
		dev_priv->is_lvds_on = true;
		dev_priv->is_mipi_on = false;
	} else {
		dev_priv->is_mipi_on = true;
		dev_priv->is_lvds_on = false;
	}

	dev_priv->video_device_fuse = fuse_value;

	pci_write_config_dword(pci_root, 0xD0, FB_REG09);
	pci_read_config_dword(pci_root, 0xD4, &fuse_value);

	dev_dbg(dev->dev, "SKU values is 0x%x.\n", fuse_value);
	fuse_value_tmp = (fuse_value & FB_SKU_MASK) >> FB_SKU_SHIFT;

	dev_priv->fuse_reg_value = fuse_value;

	switch (fuse_value_tmp) {
	case FB_SKU_100:
		dev_priv->core_freq = 200;
		break;
	case FB_SKU_100L:
		dev_priv->core_freq = 100;
		break;
	case FB_SKU_83:
		dev_priv->core_freq = 166;
		break;
	default:
		dev_warn(dev->dev, "Invalid SKU values, SKU value = 0x%08x\n",
								fuse_value_tmp);
		dev_priv->core_freq = 0;
	}
	dev_dbg(dev->dev, "LNC core clk is %dMHz.\n", dev_priv->core_freq);
	pci_dev_put(pci_root);
}

/*
 *	Get the revison ID, B0:D2:F0;0x08
 */
static void mid_get_pci_revID(struct drm_psb_private *dev_priv)
{
	uint32_t platform_rev_id = 0;
	struct pci_dev *pdev = to_pci_dev(dev_priv->dev.dev);
	int domain = pci_domain_nr(pdev->bus);
	struct pci_dev *pci_gfx_root =
		pci_get_domain_bus_and_slot(domain, 0, PCI_DEVFN(2, 0));

	if (pci_gfx_root == NULL) {
		WARN_ON(1);
		return;
	}
	pci_read_config_dword(pci_gfx_root, 0x08, &platform_rev_id);
	dev_priv->platform_rev_id = (uint8_t) platform_rev_id;
	pci_dev_put(pci_gfx_root);
	dev_dbg(dev_priv->dev.dev, "platform_rev_id is %x\n", dev_priv->platform_rev_id);
}

struct mid_vbt_header {
	u32 signature;
	u8 revision;
} __packed;

/* The same for r0 and r1 */
struct vbt_r0 {
	struct mid_vbt_header vbt_header;
	u8 size;
	u8 checksum;
} __packed;

struct vbt_r10 {
	struct mid_vbt_header vbt_header;
	u8 checksum;
	u16 size;
	u8 panel_count;
	u8 primary_panel_idx;
	u8 secondary_panel_idx;
	u8 __reserved[5];
} __packed;

static int read_vbt_r0(u32 addr, struct vbt_r0 *vbt)
{
	void __iomem *vbt_virtual;

	vbt_virtual = ioremap(addr, sizeof(*vbt));
	if (vbt_virtual == NULL)
		return -1;

	memcpy_fromio(vbt, vbt_virtual, sizeof(*vbt));
	iounmap(vbt_virtual);

	return 0;
}

static int read_vbt_r10(u32 addr, struct vbt_r10 *vbt)
{
	void __iomem *vbt_virtual;

	vbt_virtual = ioremap(addr, sizeof(*vbt));
	if (!vbt_virtual)
		return -1;

	memcpy_fromio(vbt, vbt_virtual, sizeof(*vbt));
	iounmap(vbt_virtual);

	return 0;
}

static int mid_get_vbt_data_r0(struct drm_psb_private *dev_priv, u32 addr)
{
	struct vbt_r0 vbt;
	void __iomem *gct_virtual;
	struct gct_r0 gct;
	u8 bpi;

	if (read_vbt_r0(addr, &vbt))
		return -1;

	gct_virtual = ioremap(addr + sizeof(vbt), vbt.size - sizeof(vbt));
	if (!gct_virtual)
		return -1;
	memcpy_fromio(&gct, gct_virtual, sizeof(gct));
	iounmap(gct_virtual);

	bpi = gct.PD.BootPanelIndex;
	dev_priv->gct_data.bpi = bpi;
	dev_priv->gct_data.pt = gct.PD.PanelType;
	dev_priv->gct_data.DTD = gct.panel[bpi].DTD;
	dev_priv->gct_data.Panel_Port_Control =
		gct.panel[bpi].Panel_Port_Control;
	dev_priv->gct_data.Panel_MIPI_Display_Descriptor =
		gct.panel[bpi].Panel_MIPI_Display_Descriptor;

	return 0;
}

static int mid_get_vbt_data_r1(struct drm_psb_private *dev_priv, u32 addr)
{
	struct vbt_r0 vbt;
	void __iomem *gct_virtual;
	struct gct_r1 gct;
	u8 bpi;

	if (read_vbt_r0(addr, &vbt))
		return -1;

	gct_virtual = ioremap(addr + sizeof(vbt), vbt.size - sizeof(vbt));
	if (!gct_virtual)
		return -1;
	memcpy_fromio(&gct, gct_virtual, sizeof(gct));
	iounmap(gct_virtual);

	bpi = gct.PD.BootPanelIndex;
	dev_priv->gct_data.bpi = bpi;
	dev_priv->gct_data.pt = gct.PD.PanelType;
	dev_priv->gct_data.DTD = gct.panel[bpi].DTD;
	dev_priv->gct_data.Panel_Port_Control =
		gct.panel[bpi].Panel_Port_Control;
	dev_priv->gct_data.Panel_MIPI_Display_Descriptor =
		gct.panel[bpi].Panel_MIPI_Display_Descriptor;

	return 0;
}

static int mid_get_vbt_data_r10(struct drm_psb_private *dev_priv, u32 addr)
{
	struct vbt_r10 vbt;
	void __iomem *gct_virtual;
	struct gct_r10 *gct;
	struct oaktrail_timing_info *dp_ti = &dev_priv->gct_data.DTD;
	struct gct_r10_timing_info *ti;
	int ret = -1;

	if (read_vbt_r10(addr, &vbt))
		return -1;

	gct = kmalloc_array(vbt.panel_count, sizeof(*gct), GFP_KERNEL);
	if (!gct)
		return -ENOMEM;

	gct_virtual = ioremap(addr + sizeof(vbt),
			sizeof(*gct) * vbt.panel_count);
	if (!gct_virtual)
		goto out;
	memcpy_fromio(gct, gct_virtual, sizeof(*gct));
	iounmap(gct_virtual);

	dev_priv->gct_data.bpi = vbt.primary_panel_idx;
	dev_priv->gct_data.Panel_MIPI_Display_Descriptor =
		gct[vbt.primary_panel_idx].Panel_MIPI_Display_Descriptor;

	ti = &gct[vbt.primary_panel_idx].DTD;
	dp_ti->pixel_clock = ti->pixel_clock;
	dp_ti->hactive_hi = ti->hactive_hi;
	dp_ti->hactive_lo = ti->hactive_lo;
	dp_ti->hblank_hi = ti->hblank_hi;
	dp_ti->hblank_lo = ti->hblank_lo;
	dp_ti->hsync_offset_hi = ti->hsync_offset_hi;
	dp_ti->hsync_offset_lo = ti->hsync_offset_lo;
	dp_ti->hsync_pulse_width_hi = ti->hsync_pulse_width_hi;
	dp_ti->hsync_pulse_width_lo = ti->hsync_pulse_width_lo;
	dp_ti->vactive_hi = ti->vactive_hi;
	dp_ti->vactive_lo = ti->vactive_lo;
	dp_ti->vblank_hi = ti->vblank_hi;
	dp_ti->vblank_lo = ti->vblank_lo;
	dp_ti->vsync_offset_hi = ti->vsync_offset_hi;
	dp_ti->vsync_offset_lo = ti->vsync_offset_lo;
	dp_ti->vsync_pulse_width_hi = ti->vsync_pulse_width_hi;
	dp_ti->vsync_pulse_width_lo = ti->vsync_pulse_width_lo;

	ret = 0;
out:
	kfree(gct);
	return ret;
}

static bool mid_get_vbt_data(struct drm_psb_private *dev_priv)
{
	struct drm_device *dev = &dev_priv->dev;
	struct pci_dev *pdev = to_pci_dev(dev->dev);
	u32 addr;
	u8 __iomem *vbt_virtual;
	u8 primary_panel;
	u8 number_desc = 0;
	u8 panel_name[PANEL_NAME_MAX_LEN+1] = {0};
	struct intel_mid_vbt *pVBT = &dev_priv->vbt_data;
	void *panel_desc;
	struct pci_dev *pci_gfx_root =
		pci_get_domain_bus_and_slot(pci_domain_nr(pdev->bus),
					    0, PCI_DEVFN(2, 0));
	mdfld_dsi_encoder_t mipi_mode;
	int ret = 0, len = 0;
	struct platform_device *platform_dev;
	struct oaktrail_timing_info *dp_ti = &dev_priv->gct_data.DTD;
	struct gct_r10_timing_info ti;
	void *pGCT;
	u16 new_size;
	u8 bpi;

	if (!pci_gfx_root) {
		DRM_ERROR("Invalid root\n");
		return false;
	}

	/* Get the address of the platform config vbt */
	pci_read_config_dword(pci_gfx_root, 0xFC, &addr);
	pci_dev_put(pci_gfx_root);

	/**
	 * if addr is 0,
	 * that means FW doesn't support VBT
	 */
	if (!addr) {
		pVBT->size = 0;
		return false;
	}

	/* get the virtual address of the vbt */
	vbt_virtual = ioremap(addr, sizeof(*pVBT));
	if (!vbt_virtual) {
		DRM_ERROR("fail to ioremap addr:0x%x\n",
			  addr);
		return false;
	}
	memcpy(pVBT, vbt_virtual, sizeof(*pVBT));
	iounmap(vbt_virtual);

	if (strncmp(pVBT->signature, "$GCT", 4)) {
		DRM_ERROR("wrong GCT signature\n");
		return false;
	}

	dev_dbg(dev->dev, "GCT revision is %02x\n", pVBT->revision);

	number_desc = pVBT->num_of_panel_desc;
	primary_panel = pVBT->primary_panel_idx;
	dev_priv->gct_data.bpi = primary_panel; /*save boot panel id*/

	/**
	 * current we just need parse revision 0x11 and 0x20
	 */
	switch (pVBT->revision) {
	   case 0x10:
			   /*header definition changed from rev 01 (v2) to rev 10h. */
			   /*so, some values have changed location*/
			   new_size = pVBT->checksum; /*checksum contains lo size byte*/
			   /*LSB of mrst_gct contains hi size byte*/
			   new_size |= ((0xff & (unsigned int)pVBT->panel_descs)) << 8;

			   pVBT->checksum = pVBT->size; /*size contains the checksum*/
			   if (new_size > 0xff)
							   pVBT->size = 0xff; /*restrict size to 255*/
			   else
							   pVBT->size = new_size;

			   /* number of descriptors defined in the GCT */
			   number_desc = ((0xff00 & (unsigned int)pVBT->panel_descs)) >> 8;
			   bpi = ((0xff0000 & (unsigned int)pVBT->panel_descs)) >> 16;
			   pVBT->panel_descs = NULL;
			   pVBT->panel_descs = \
											   ioremap(addr + GCT_R10_HEADER_SIZE,
															   GCT_R10_DISPLAY_DESC_SIZE * number_desc);
			   pGCT = pVBT->panel_descs;
			   pGCT = (u8 *)pGCT + (bpi * GCT_R10_DISPLAY_DESC_SIZE);
			   dev_priv->gct_data.bpi = bpi; /*save boot panel id*/

			   /*copy the GCT display timings into a temp structure*/
			   memcpy(&ti, pGCT, sizeof(struct gct_r10_timing_info));

			   /*now copy the temp struct into the dev_priv->gct_data*/
			   dp_ti->pixel_clock = ti.pixel_clock;
			   dp_ti->hactive_hi = ti.hactive_hi;
			   dp_ti->hactive_lo = ti.hactive_lo;
			   dp_ti->hblank_hi = ti.hblank_hi;
			   dp_ti->hblank_lo = ti.hblank_lo;
			   dp_ti->hsync_offset_hi = ti.hsync_offset_hi;
			   dp_ti->hsync_offset_lo = ti.hsync_offset_lo;
			   dp_ti->hsync_pulse_width_hi = ti.hsync_pulse_width_hi;
			   dp_ti->hsync_pulse_width_lo = ti.hsync_pulse_width_lo;
			   dp_ti->vactive_hi = ti.vactive_hi;
			   dp_ti->vactive_lo = ti.vactive_lo;
			   dp_ti->vblank_hi = ti.vblank_hi;
			   dp_ti->vblank_lo = ti.vblank_lo;
			   dp_ti->vsync_offset_hi = ti.vsync_offset_hi;

			   /*mov the MIPI_Display_Descriptor data from GCT to dev priv*/
			   dev_priv->gct_data.Panel_MIPI_Display_Descriptor =
							   *((u8 *)pGCT + 0x0d);
			   dev_priv->gct_data.Panel_MIPI_Display_Descriptor |=
							   (*((u8 *)pGCT + 0x0e)) << 8;
			   mipi_mode =
							   (dev_priv->gct_data.Panel_MIPI_Display_Descriptor &
							   0x40) ? MDFLD_DSI_ENCODER_DPI : MDFLD_DSI_ENCODER_DBI;
			   break;
	case 0x11:
		/* number of descriptors defined in the GCT */
		pVBT->panel_descs =
			ioremap(addr + GCT_R11_HEADER_SIZE,
				GCT_R11_DISPLAY_DESC_SIZE * number_desc);
		panel_desc = (u8 *)pVBT->panel_descs +
			(primary_panel * GCT_R11_DISPLAY_DESC_SIZE);

		if (!panel_desc) {
			DRM_ERROR("Invalid desc\n");
			return false;
		}

		strncpy(panel_name, panel_desc, PANEL_NAME_MAX_LEN);

		mipi_mode =
		((struct gct_r11_panel_desc *)panel_desc)->display.mode ? \
			MDFLD_DSI_ENCODER_DPI : MDFLD_DSI_ENCODER_DBI;

		break;
	case 0x20:
		pVBT->panel_descs =
			ioremap(addr + GCT_R20_HEADER_SIZE,
				GCT_R20_DISPLAY_DESC_SIZE * number_desc);
		panel_desc = (u8 *)pVBT->panel_descs +
			(primary_panel * GCT_R20_DISPLAY_DESC_SIZE);

		if (!panel_desc) {
			DRM_ERROR("Invalid desc\n");
			return false;
		}

		strncpy(panel_name, panel_desc, PANEL_NAME_MAX_LEN);

		mipi_mode =
		((struct gct_r20_panel_desc *)panel_desc)->panel_mode.mode ?\
			MDFLD_DSI_ENCODER_DPI : MDFLD_DSI_ENCODER_DBI;
		break;
	default:
		dev_err(dev->dev, "Unknown revision of GCT!\n");
		pVBT->size = 0;
		return false;
	}
#ifdef CONFIG_SUPPORT_SMD_QHD_AMOLED_COMMAND_MODE_DISPLAY
	strncpy(panel_name,"SMD QHD Amoled",16);
	mipi_mode = MDFLD_DSI_ENCODER_DBI;
#endif

	len = strnlen(panel_name, PANEL_NAME_MAX_LEN);
	if (len) {
		strncpy(dev_priv->panel_info.name, panel_name, len);
		dev_priv->panel_info.mode = mipi_mode;
	} else {
		DRM_ERROR("%s: detect panel info from gct error\n",
				__func__);
		return false;
	}

	platform_dev = platform_device_alloc(panel_name, -1);
	if (!platform_dev) {
		DRM_ERROR("%s: fail to alloc platform device\n", __func__);
		return false;
	}
	ret = platform_device_add(platform_dev);
	if (ret) {
		DRM_ERROR("%s: fail to add platform device\n", __func__);
		return false;
	}

	DRM_INFO("%s: panel name: %s, mipi_mode = %d\n", __func__,
			panel_name, mipi_mode);

	return true;
}

int mid_chip_setup(struct drm_device *dev)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	mid_get_fuse_settings(dev);
	mid_get_vbt_data(dev_priv);
	mid_get_pci_revID(dev_priv);
	return 0;
}
