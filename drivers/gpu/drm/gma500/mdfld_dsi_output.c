/*
 * Copyright © 2010 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors:
 * jim liu <jim.liu@intel.com>
 * Jackie Li<yaodong.li@intel.com>
 */

#include <linux/delay.h>
#include <linux/moduleparam.h>
#include <linux/pm_runtime.h>
#include <linux/gpio/consumer.h>

#include <asm/intel_scu_ipc.h>

#include "mdfld_dsi_dpi.h"
#include "mdfld_dsi_dbi.h"
#include "mdfld_dsi_output.h"
#include "mdfld_dsi_pkg_sender.h"
#include "mdfld_output.h"
#include <linux/freezer.h>
#include "psb_drv.h"
#include "mdfld_dsi_esd.h"
#include "mdfld_dsi_dbi_dsr.h"
#include "mdfld_mot_cmn.h"

/* get the LABC from command line. */
static int LABC_control = 1;

#ifdef MODULE
module_param(LABC_control, int, 0644);
#else

static int __init parse_LABC_control(char *arg)
{
	/* LABC control can be passed in as a cmdline parameter */
	/* to enable this feature add LABC=1 to cmdline */
	/* to disable this feature add LABC=0 to cmdline */
	if (!arg)
		return -EINVAL;

	if (!strcasecmp(arg, "0"))
		LABC_control = 0;
	else if (!strcasecmp(arg, "1"))
		LABC_control = 1;

	return 0;
}
early_param("LABC", parse_LABC_control);
#endif

/*
 * Check and see if the generic control or data buffer is empty and ready.
 */
void mdfld_dsi_gen_fifo_ready(struct drm_device *dev, u32 gen_fifo_stat_reg,
							u32 fifo_stat)
{
	u32 GEN_BF_time_out_count;

	/* Check MIPI Adatper command registers */
	for (GEN_BF_time_out_count = 0;
			GEN_BF_time_out_count < GEN_FB_TIME_OUT;
			GEN_BF_time_out_count++) {
		if ((REG_READ(gen_fifo_stat_reg) & fifo_stat) == fifo_stat)
			break;
		udelay(100);
	}

	if (GEN_BF_time_out_count == GEN_FB_TIME_OUT)
		DRM_ERROR("mdfld_dsi_gen_fifo_ready, Timeout. gen_fifo_stat_reg = 0x%x.\n",
					gen_fifo_stat_reg);
}

/*
 * Manage the DSI MIPI keyboard and display brightness.
 * FIXME: this is exported to OSPM code. should work out an specific
 * display interface to OSPM.
 */

void mdfld_dsi_brightness_init(struct mdfld_dsi_config *dsi_config, int pipe)
{
	struct mdfld_dsi_pkg_sender *sender =
				mdfld_dsi_get_pkg_sender(dsi_config);
	struct drm_device *dev;
	struct drm_psb_private *dev_priv;
	u32 gen_ctrl_val;

	if (!sender) {
		DRM_ERROR("No sender found\n");
		return;
	}

	dev = sender->dev;
	dev_priv = to_drm_psb_private(dev);

	/* Set default display backlight value to 85% (0xd8)*/
	mdfld_dsi_send_mcs_short(sender, write_display_brightness, 0xd8, 1,
				true);

	/* Set minimum brightness setting of CABC function to 20% (0x33)*/
	mdfld_dsi_send_mcs_short(sender, write_cabc_min_bright, 0x33, 1, true);

	/* Enable backlight or/and LABC */
	gen_ctrl_val = BRIGHT_CNTL_BLOCK_ON | DISPLAY_DIMMING_ON |
								BACKLIGHT_ON;
	if (LABC_control == 1)
		gen_ctrl_val |= DISPLAY_DIMMING_ON | DISPLAY_BRIGHTNESS_AUTO
								| GAMMA_AUTO;

	if (LABC_control == 1)
		gen_ctrl_val |= AMBIENT_LIGHT_SENSE_ON;

	dev_priv->mipi_ctrl_display = gen_ctrl_val;

	mdfld_dsi_send_mcs_short(sender, write_ctrl_display, (u8)gen_ctrl_val,
				1, true);

	mdfld_dsi_send_mcs_short(sender, write_ctrl_cabc, UI_IMAGE, 1, true);
}

void mdfld_dsi_brightness_control(struct drm_device *dev, int pipe, int level)
{
	struct mdfld_dsi_pkg_sender *sender;
	struct drm_psb_private *dev_priv;
	struct mdfld_dsi_config *dsi_config;
	u32 gen_ctrl_val = 0;
	int p_type = TMD_VID;

	if (!dev || (pipe != 0 && pipe != 2)) {
		DRM_ERROR("Invalid parameter\n");
		return;
	}

	p_type = mdfld_get_panel_type(dev, 0);

	dev_priv = to_drm_psb_private(dev);

	if (pipe)
		dsi_config = dev_priv->dsi_configs[1];
	else
		dsi_config = dev_priv->dsi_configs[0];

	sender = mdfld_dsi_get_pkg_sender(dsi_config);

	if (!sender) {
		DRM_ERROR("No sender found\n");
		return;
	}

	gen_ctrl_val = (level * 0xff / MDFLD_DSI_BRIGHTNESS_MAX_LEVEL) & 0xff;

	dev_dbg(sender->dev->dev, "pipe = %d, gen_ctrl_val = %d.\n",
							pipe, gen_ctrl_val);

	if (p_type == TMD_VID) {
		/* Set display backlight value */
		mdfld_dsi_send_mcs_short(sender, tmd_write_display_brightness,
					(u8)gen_ctrl_val, 1, true);
	} else {
		/* Set display backlight value */
		mdfld_dsi_send_mcs_short(sender, write_display_brightness,
					(u8)gen_ctrl_val, 1, true);

		/* Enable backlight control */
		if (level == 0)
			gen_ctrl_val = 0;
		else
			gen_ctrl_val = dev_priv->mipi_ctrl_display;

		mdfld_dsi_send_mcs_short(sender, write_ctrl_display,
					(u8)gen_ctrl_val, 1, true);
	}
}

static int mdfld_dsi_get_panel_status(struct mdfld_dsi_config *dsi_config,
				u8 dcs, u32 *data, bool hs)
{
	struct mdfld_dsi_pkg_sender *sender
		= mdfld_dsi_get_pkg_sender(dsi_config);

	if (!sender || !data) {
		DRM_ERROR("Invalid parameter\n");
		return -EINVAL;
	}

	return mdfld_dsi_read_mcs(sender, dcs, data, 1, hs);
}

int mdfld_dsi_get_power_mode(struct mdfld_dsi_config *dsi_config, u32 *mode,
			bool hs)
{
	if (!dsi_config || !mode) {
		DRM_ERROR("Invalid parameter\n");
		return -EINVAL;
	}

	return mdfld_dsi_get_panel_status(dsi_config, 0x0a, mode, hs);
}

/*
 * NOTE: this function was used by OSPM.
 * TODO: will be removed later, should work out display interfaces for OSPM
 */
void mdfld_dsi_controller_init(struct mdfld_dsi_config *dsi_config, int pipe)
{
	if (!dsi_config || ((pipe != 0) && (pipe != 2))) {
		DRM_ERROR("Invalid parameters\n");
		return;
	}

	mdfld_dsi_dpi_controller_init(dsi_config, pipe);
}

static void mdfld_dsi_connector_save(struct drm_connector *connector)
{
}

static void mdfld_dsi_connector_restore(struct drm_connector *connector)
{
}

/* FIXME: start using the force parameter */
static enum drm_connector_status
mdfld_dsi_connector_detect(struct drm_connector *connector, bool force)
{
	struct mdfld_dsi_connector *dsi_connector
		= mdfld_dsi_connector(connector);

	dsi_connector->status = connector_status_connected;

	return dsi_connector->status;
}

static int mdfld_dsi_connector_set_property(struct drm_connector *connector,
				struct drm_property *property,
				uint64_t value)
{
	struct drm_encoder *encoder = connector->encoder;

	if (!strcmp(property->name, "scaling mode") && encoder) {
		struct gma_crtc *gma_crtc = to_gma_crtc(encoder->crtc);
		bool centerechange;
		uint64_t val;

		if (!gma_crtc)
			goto set_prop_error;

		switch (value) {
		case DRM_MODE_SCALE_FULLSCREEN:
			break;
		case DRM_MODE_SCALE_NO_SCALE:
			break;
		case DRM_MODE_SCALE_ASPECT:
			break;
		default:
			goto set_prop_error;
		}

		if (drm_object_property_get_value(&connector->base, property, &val))
			goto set_prop_error;

		if (val == value)
			goto set_prop_done;

		if (drm_object_property_set_value(&connector->base,
							property, value))
			goto set_prop_error;

		centerechange = (val == DRM_MODE_SCALE_NO_SCALE) ||
			(value == DRM_MODE_SCALE_NO_SCALE);

		if (gma_crtc->saved_mode.hdisplay != 0 &&
		    gma_crtc->saved_mode.vdisplay != 0) {
			if (centerechange) {
				if (!drm_crtc_helper_set_mode(encoder->crtc,
						&gma_crtc->saved_mode,
						encoder->crtc->x,
						encoder->crtc->y,
						encoder->crtc->primary->fb))
					goto set_prop_error;
			} else {
				const struct drm_encoder_helper_funcs *funcs =
						encoder->helper_private;
				funcs->mode_set(encoder,
					&gma_crtc->saved_mode,
					&gma_crtc->saved_adjusted_mode);
			}
		}
	} else if (!strcmp(property->name, "backlight") && encoder) {
		if (drm_object_property_set_value(&connector->base, property,
									value))
			goto set_prop_error;
		else
			gma_backlight_set(encoder->dev, value);
	}
set_prop_done:
	return 0;
set_prop_error:
	return -1;
}

static void mdfld_dsi_connector_destroy(struct drm_connector *connector)
{
	struct mdfld_dsi_connector *dsi_connector =
					mdfld_dsi_connector(connector);
	struct mdfld_dsi_pkg_sender *sender;

	if (!dsi_connector)
		return;
	drm_connector_unregister(connector);
	drm_connector_cleanup(connector);
	sender = dsi_connector->pkg_sender;
	mdfld_dsi_pkg_sender_destroy(sender);
	kfree(dsi_connector);
}

static int mdfld_dsi_connector_get_modes(struct drm_connector *connector)
{
	struct mdfld_dsi_connector *dsi_connector =
				mdfld_dsi_connector(connector);
	struct mdfld_dsi_config *dsi_config =
				mdfld_dsi_get_config(dsi_connector);
	struct drm_display_mode *fixed_mode = dsi_config->fixed_mode;
	struct drm_display_mode *dup_mode = NULL;
	struct drm_device *dev = connector->dev;

	if (fixed_mode) {
		dev_dbg(dev->dev, "fixed_mode %dx%d\n",
				fixed_mode->hdisplay, fixed_mode->vdisplay);
		dup_mode = drm_mode_duplicate(dev, fixed_mode);
		drm_mode_probed_add(connector, dup_mode);
		return 1;
	}
	DRM_ERROR("Didn't get any modes!\n");
	return 0;
}

static enum drm_mode_status mdfld_dsi_connector_mode_valid(struct drm_connector *connector,
						struct drm_display_mode *mode)
{
	struct mdfld_dsi_connector *dsi_connector =
					mdfld_dsi_connector(connector);
	struct mdfld_dsi_config *dsi_config =
					mdfld_dsi_get_config(dsi_connector);
	struct drm_display_mode *fixed_mode = dsi_config->fixed_mode;

	if (mode->flags & DRM_MODE_FLAG_DBLSCAN)
		return MODE_NO_DBLESCAN;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE)
		return MODE_NO_INTERLACE;

	/**
	 * FIXME: current DC has no fitting unit, reject any mode setting
	 * request
	 * Will figure out a way to do up-scaling(panel fitting) later.
	 **/
	if (fixed_mode) {
		if (mode->hdisplay != fixed_mode->hdisplay)
			return MODE_PANEL;

		if (mode->vdisplay != fixed_mode->vdisplay)
			return MODE_PANEL;
	}

	return MODE_OK;
}

static struct drm_encoder *mdfld_dsi_connector_best_encoder(
				struct drm_connector *connector)
{
	struct mdfld_dsi_connector *dsi_connector =
				mdfld_dsi_connector(connector);
	struct mdfld_dsi_config *dsi_config =
				mdfld_dsi_get_config(dsi_connector);
	struct mdfld_dsi_encoder *encoder = NULL;
	if (!dsi_config) {
		DRM_ERROR("Invalid config\n");
		return NULL;
	}
	
	pr_debug("config type %d\n", dsi_config->type);
	
	if(dsi_config->type == MDFLD_DSI_ENCODER_DBI) 
		encoder = dsi_config->encoders[MDFLD_DSI_ENCODER_DBI];
	else if (dsi_config->type == MDFLD_DSI_ENCODER_DPI) 
		encoder = dsi_config->encoders[MDFLD_DSI_ENCODER_DPI];
	
	pr_debug("get encoder %p\n", encoder);
	
	if(!encoder) {
		DRM_ERROR("Invalid encoder for type %d\n", dsi_config->type);
		return NULL;
	}
	
	dsi_config->encoder = encoder;
		
	return &encoder->base.base;
}

/*DSI connector funcs*/
static const struct drm_connector_funcs mdfld_dsi_connector_funcs = {
	.dpms = drm_helper_connector_dpms,
	.detect = mdfld_dsi_connector_detect,
	.fill_modes = drm_helper_probe_single_connector_modes,
	.set_property = mdfld_dsi_connector_set_property,
	.destroy = mdfld_dsi_connector_destroy,
};

/*DSI connector helper funcs*/
static const struct drm_connector_helper_funcs
	mdfld_dsi_connector_helper_funcs = {
	.get_modes = mdfld_dsi_connector_get_modes,
	.mode_valid = mdfld_dsi_connector_mode_valid,
	.best_encoder = mdfld_dsi_connector_best_encoder,
};

static int mdfld_dsi_get_default_config(struct drm_device *dev,
				struct mdfld_dsi_config *config, int pipe)
{
	if (!dev || !config) {
		DRM_ERROR("Invalid parameters");
		return -EINVAL;
	}

	config->bpp = 24;
	config->type = get_panel_mode(dev);
	if (mdfld_get_panel_type(dev, pipe) == TC35876X)
		config->lane_count = 4;
	else
		config->lane_count = 2;
	config->lane_config = MDFLD_DSI_DATA_LANE_2_2;

	config->channel_num = 0;
	if (mdfld_get_panel_type(dev, pipe) == TMD_VID)
		config->video_mode = MDFLD_DSI_VIDEO_NON_BURST_MODE_SYNC_PULSE;
	else if (mdfld_get_panel_type(dev, pipe) == TC35876X)
		config->video_mode =
				MDFLD_DSI_VIDEO_NON_BURST_MODE_SYNC_EVENTS;
	else
		config->video_mode = MDFLD_DSI_VIDEO_BURST_MODE;
	
	return 0;
}

static int mdfld_dsi_regs_init(struct mdfld_dsi_config *dsi_config,
				int pipe)
{
	struct mdfld_dsi_hw_registers *regs;
	u32 reg_offset;

	if (!dsi_config) {
		DRM_ERROR("dsi_config is null\n");
		return -EINVAL;
	}

	regs = &dsi_config->regs;

	regs->vgacntr_reg = VGACNTRL;
	regs->dpll_reg = MRST_DPLL_A;
	regs->fp_reg = MRST_FPA0;

	regs->ovaadd_reg = OV_OVADD;
	regs->ovcadd_reg = OVC_OVADD;

	if (pipe == 0) {
		regs->dspcntr_reg = DSPACNTR;
		regs->dspsize_reg = DSPASIZE;
		regs->dspsurf_reg = DSPASURF;
		regs->dsplinoff_reg = DSPALINOFF;
		regs->dsppos_reg = DSPAPOS;
		regs->dspstride_reg = DSPASTRIDE;
		regs->color_coef_reg = PIPEA_COLOR_COEF0;
		regs->htotal_reg = HTOTAL_A;
		regs->hblank_reg = HBLANK_A;
		regs->hsync_reg = HSYNC_A;
		regs->vtotal_reg = VTOTAL_A;
		regs->vblank_reg = VBLANK_A;
		regs->vsync_reg = VSYNC_A;
		regs->pipesrc_reg = PIPEASRC;
		regs->pipeconf_reg = PIPEACONF;
		regs->pipestat_reg = PIPEASTAT;
		regs->mipi_reg = MIPI;
		regs->palette_reg = PALETTE_A;
		regs->gamma_red_max_reg = GAMMA_RED_MAX_A;
		regs->gamma_green_max_reg = GAMMA_GREEN_MAX_A;
		regs->gamma_blue_max_reg = GAMMA_BLUE_MAX_A;
		regs->te_delay_reg = MIPI_TE_COUNT;
		reg_offset = 0;
	} else if (pipe == 2) {
		regs->dspcntr_reg = DSPCCNTR;
		regs->dspsize_reg = DSPCSIZE;
		regs->dspsurf_reg = DSPCSURF;
		regs->dsplinoff_reg = DSPCLINOFF;
		regs->dsppos_reg = DSPCPOS;
		regs->dspstride_reg = DSPCSTRIDE;
		regs->color_coef_reg = PIPEC_COLOR_COEF0;
		regs->htotal_reg = HTOTAL_C;
		regs->hblank_reg = HBLANK_C;
		regs->hsync_reg = HSYNC_C;
		regs->vtotal_reg = VTOTAL_C;
		regs->vblank_reg = VBLANK_C;
		regs->vsync_reg = VSYNC_C;
		regs->pipesrc_reg = PIPECSRC;
		regs->pipeconf_reg = PIPECCONF;
		regs->pipestat_reg = PIPECSTAT;
		regs->mipi_reg = MIPI_C;
		regs->palette_reg = PALETTE_C;
		regs->gamma_red_max_reg = GAMMA_RED_MAX_C;
		regs->gamma_green_max_reg = GAMMA_GREEN_MAX_C;
		regs->gamma_blue_max_reg = GAMMA_BLUE_MAX_C;

		regs->te_delay_reg = MIPIC_TE_COUNT;
		reg_offset = MIPIC_REG_OFFSET;
	} else {
		DRM_ERROR("Wrong pipe\n");
		return -EINVAL;
	}

	regs->device_ready_reg = MIPI_DEVICE_READY_REG(0) + reg_offset;
	regs->intr_stat_reg = MIPI_INTR_STAT_REG(0) + reg_offset;
	regs->intr_en_reg = MIPI_INTR_EN_REG(0) + reg_offset;
	regs->dsi_func_prg_reg = MIPI_DSI_FUNC_PRG_REG(0) + reg_offset;
	regs->hs_tx_timeout_reg = MIPI_HS_TX_TIMEOUT_REG(0) + reg_offset;
	regs->lp_rx_timeout_reg = MIPI_LP_RX_TIMEOUT_REG(0) + reg_offset;
	regs->turn_around_timeout_reg =
		MIPI_TURN_AROUND_TIMEOUT_REG(0) + reg_offset;
	regs->device_reset_timer_reg =
		MIPI_DEVICE_RESET_TIMER_REG(0) + reg_offset;
	regs->dpi_resolution_reg = MIPI_DPI_RESOLUTION_REG(0) + reg_offset;
	regs->dbi_fifo_throttle_reg =
		MIPI_DBI_FIFO_THROTTLE_REG(0) + reg_offset;
	regs->hsync_count_reg = MIPI_HSYNC_COUNT_REG(0) + reg_offset;
	regs->hbp_count_reg = MIPI_HBP_COUNT_REG(0) + reg_offset;
	regs->hfp_count_reg = MIPI_HFP_COUNT_REG(0) + reg_offset;
	regs->hactive_count_reg = MIPI_HACTIVE_COUNT_REG(0) + reg_offset;
	regs->vsync_count_reg = MIPI_VSYNC_COUNT_REG(0) + reg_offset;
	regs->vbp_count_reg = MIPI_VBP_COUNT_REG(0) + reg_offset;
	regs->vfp_count_reg = MIPI_VFP_COUNT_REG(0) + reg_offset;
	regs->high_low_switch_count_reg =
		MIPI_HIGH_LOW_SWITCH_COUNT_REG(0) + reg_offset;
	regs->dpi_control_reg = MIPI_DPI_CONTROL_REG(0) + reg_offset;
	regs->dpi_data_reg = MIPI_DPI_DATA_REG(0) + reg_offset;
	regs->init_count_reg = MIPI_INIT_COUNT_REG(0) + reg_offset;
	regs->max_return_pack_size_reg =
		MIPI_MAX_RETURN_PACK_SIZE_REG(0) + reg_offset;
	regs->video_mode_format_reg =
		MIPI_VIDEO_MODE_FORMAT_REG(0) + reg_offset;
	regs->eot_disable_reg = MIPI_EOT_DISABLE_REG(0) + reg_offset;
	regs->lp_byteclk_reg = MIPI_LP_BYTECLK_REG(0) + reg_offset;
	regs->lp_gen_data_reg = MIPI_LP_GEN_DATA_REG(0) + reg_offset;
	regs->hs_gen_data_reg = MIPI_HS_GEN_DATA_REG(0) + reg_offset;
	regs->lp_gen_ctrl_reg = MIPI_LP_GEN_CTRL_REG(0) + reg_offset;
	regs->hs_gen_ctrl_reg = MIPI_HS_GEN_CTRL_REG(0) + reg_offset;
	regs->gen_fifo_stat_reg = MIPI_GEN_FIFO_STAT_REG(0) + reg_offset;
	regs->hs_ls_dbi_enable_reg =
		MIPI_HS_LS_DBI_ENABLE_REG(0) + reg_offset;
	regs->dphy_param_reg = MIPI_DPHY_PARAM_REG(0) + reg_offset;
	regs->dbi_bw_ctrl_reg = MIPI_DBI_BW_CTRL_REG(0) + reg_offset;
	regs->clk_lane_switch_time_cnt_reg =
		MIPI_CLK_LANE_SWITCH_TIME_CNT_REG(0) + reg_offset;

	regs->mipi_control_reg = MIPI_CONTROL_REG + reg_offset;
	regs->mipi_data_addr_reg = MIPI_DATA_ADD_REG(0) + reg_offset;
	regs->mipi_data_len_reg = MIPI_DATA_LEN_REG(0) + reg_offset;
	regs->mipi_cmd_addr_reg = MIPI_CMD_ADD_REG(0) + reg_offset;
	regs->mipi_cmd_len_reg = MIPI_CMD_LEN_REG(0) + reg_offset;
	regs->histogram_intr_ctrl_reg = HISTOGRAM_INT_CONTROL;
	regs->histogram_logic_ctrl_reg = HISTOGRAM_LOGIC_CONTROL;
	regs->aimg_enhance_bin_reg = HISTOGRAM_BIN_DATA;
	regs->lvds_port_ctrl_reg = LVDS_PORT_CTRL;

	return 0;
}

/*
 * Returns the panel fixed mode from configuration. 
 */
struct drm_display_mode *
mdfld_dsi_get_configuration_mode(struct mdfld_dsi_config * dsi_config, int pipe)
{
	struct drm_device *dev = dsi_config->dev;
	struct drm_display_mode *mode;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	struct oaktrail_timing_info *ti = &dev_priv->gct_data.DTD;
	bool use_gct = false;
	if (IS_CTP(dev))
		use_gct = true;

	mode = kzalloc(sizeof(*mode), GFP_KERNEL);
	if (!mode)
		return NULL;

	if (use_gct) {
		pr_debug("gct find MIPI panel. \n");

		mode->hdisplay = (ti->hactive_hi << 8) | ti->hactive_lo;
		mode->vdisplay = (ti->vactive_hi << 8) | ti->vactive_lo;
		mode->hsync_start = mode->hdisplay + \
				((ti->hsync_offset_hi << 8) | \
				ti->hsync_offset_lo);
		mode->hsync_end = mode->hsync_start + \
				((ti->hsync_pulse_width_hi << 8) | \
				ti->hsync_pulse_width_lo);
		mode->htotal = mode->hdisplay + ((ti->hblank_hi << 8) | \
								ti->hblank_lo);
		mode->vsync_start = \
			mode->vdisplay + ((ti->vsync_offset_hi << 8) | \
						ti->vsync_offset_lo);
		mode->vsync_end = \
			mode->vsync_start + ((ti->vsync_pulse_width_hi << 8) | \
						ti->vsync_pulse_width_lo);
		mode->vtotal = mode->vdisplay + \
				((ti->vblank_hi << 8) | ti->vblank_lo);
		mode->clock = ti->pixel_clock * 10;

		pr_debug("hdisplay is %d\n", mode->hdisplay);
		pr_debug("vdisplay is %d\n", mode->vdisplay);
		pr_debug("HSS is %d\n", mode->hsync_start);
		pr_debug("HSE is %d\n", mode->hsync_end);
		pr_debug("htotal is %d\n", mode->htotal);
		pr_debug("VSS is %d\n", mode->vsync_start);
		pr_debug("VSE is %d\n", mode->vsync_end);
		pr_debug("vtotal is %d\n", mode->vtotal);
		pr_debug("clock is %d\n", mode->clock);
	} else {
		if(dsi_config->type == MDFLD_DSI_ENCODER_DPI) { 
			mode->hdisplay = 864;
			mode->vdisplay = 480;
			mode->hsync_start = 873;
			mode->hsync_end = 876;
			mode->htotal = 887;
			mode->vsync_start = 487;
			mode->vsync_end = 490;
			mode->vtotal = 499;
			mode->clock = 33264;
		} else if(dsi_config->type == MDFLD_DSI_ENCODER_DBI) {
			mode->hdisplay = 864;
			mode->vdisplay = 480;
			mode->hsync_start = 872;
			mode->hsync_end = 876;
			mode->htotal = 884;
			mode->vsync_start = 482;
			mode->vsync_end = 494;
			mode->vtotal = 486;
			mode->clock = 25777;
			
		}
	}

	drm_mode_set_name(mode);
	drm_mode_set_crtcinfo(mode, 0);
	
	mode->type |= DRM_MODE_TYPE_PREFERRED;

	return mode;
}

int mdfld_dsi_panel_reset(struct drm_device *ddev, int pipe)
{
	struct device *dev = ddev->dev;
	struct gpio_desc *gpiod;

	/*
	 * Raise the GPIO reset line for the corresponding pipe to HIGH,
	 * this is probably because it is active low so this takes the
	 * respective pipe out of reset. (We have no code to put it back
	 * into reset in this driver.)
	 */
	switch (pipe) {
	case 0:
		gpiod = gpiod_get(dev, "dsi-pipe0-reset", GPIOD_OUT_HIGH);
		if (IS_ERR(gpiod))
			return PTR_ERR(gpiod);
		break;
	case 2:
		gpiod = gpiod_get(dev, "dsi-pipe2-reset", GPIOD_OUT_HIGH);
		if (IS_ERR(gpiod))
			return PTR_ERR(gpiod);
		break;
	default:
		DRM_DEV_ERROR(dev, "Invalid output pipe\n");
		return -EINVAL;
	}
	gpiod_put(gpiod);

	/* Flush posted writes on the device */
	gpiod = gpiod_get(dev, "dsi-pipe0-reset", GPIOD_ASIS);
	if (IS_ERR(gpiod))
		return PTR_ERR(gpiod);
	gpiod_get_value(gpiod);
	gpiod_put(gpiod);

	return 0;
}

/*
 * MIPI output init
 * @dev drm device
 * @pipe pipe number. 0 or 2
 * 
 * Do the initialization of a MIPI output, including create DRM mode objects
 * initialization of DSI output on @pipe
 */
void mdfld_dsi_output_init(struct drm_device *dev,
			   int pipe,
			   const struct panel_funcs *p_vid_funcs)
{
	struct mdfld_dsi_config *dsi_config;
	struct mdfld_dsi_connector *dsi_connector;
	struct gma_connector *psb_connector;
	struct drm_connector *connector;
	struct mdfld_dsi_encoder *encoder;
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);

	dev_dbg(dev->dev, "init DSI output on pipe %d\n", pipe);

	if (pipe != 0 && pipe != 2) {
		DRM_ERROR("Invalid parameter\n");
		return;
	}

	/*create a new connector*/
	dsi_connector = kzalloc(sizeof(struct mdfld_dsi_connector), GFP_KERNEL);
	if (!dsi_connector) {
		DRM_ERROR("No memory");
		return;
	}

	dsi_connector->pipe =  pipe;

	dsi_config = kzalloc(sizeof(struct mdfld_dsi_config),
			GFP_KERNEL);
	if (!dsi_config) {
		DRM_ERROR("cannot allocate memory for DSI config\n");
		goto dsi_init_err0;
	}
	mdfld_dsi_get_default_config(dev, dsi_config, pipe);

	/*init DSI regs*/
	mdfld_dsi_regs_init(dsi_config, pipe);

	/*init DSI HW context lock*/
	mutex_init(&dsi_config->context_lock);
	
	dsi_connector->private = dsi_config;
	
	dsi_config->pipe = pipe;
	dsi_config->dev = dev;
	dsi_config->connector = dsi_connector;

	if (pipe)
		dev_priv->dsi_configs[1] = dsi_config;
	else
		dev_priv->dsi_configs[0] = dsi_config;

	/*init drm connector object*/
	psb_connector = &dsi_connector->base;

	connector = &psb_connector->base;
	dsi_connector->base.save = mdfld_dsi_connector_save;
	dsi_connector->base.restore = mdfld_dsi_connector_restore;
	/* Revisit type if MIPI/HDMI bridges ever appear on Medfield */
	drm_connector_init(dev, connector, &mdfld_dsi_connector_funcs,
						DRM_MODE_CONNECTOR_LVDS);
	drm_connector_helper_add(connector, &mdfld_dsi_connector_helper_funcs);

	connector->display_info.subpixel_order = SubPixelHorizontalRGB;
	connector->display_info.width_mm = 0;
	connector->display_info.height_mm = 0;
	connector->interlace_allowed = false;
	connector->doublescan_allowed = false;

	/*attach properties*/
	drm_object_attach_property(&connector->base,
				dev->mode_config.scaling_mode_property,
				DRM_MODE_SCALE_FULLSCREEN);
	drm_object_attach_property(&connector->base,
				dev_priv->backlight_property,
				MDFLD_DSI_BRIGHTNESS_MAX_LEVEL);

	/*init DSI package sender on this output*/
	if (mdfld_dsi_pkg_sender_init(dsi_connector, pipe)) {
		DRM_ERROR("Package Sender initialization failed on pipe %d\n",
									pipe);
		goto dsi_init_err0;
	}

	/*init panel error detector*/
	if (mdfld_dsi_error_detector_init(dev, dsi_connector)) {
		DRM_ERROR("%s: failed to init dsi_error detector", __func__);
		goto dsi_init_err1;
	}

	/*create DBI & DPI encoders*/
	if (dsi_config->type == MDFLD_DSI_ENCODER_DBI) {
		encoder = mdfld_dsi_dbi_init(dev, dsi_connector);
		if(!encoder) {
			DRM_ERROR("Create DBI encoder failed\n");
			goto dsi_init_err2;
		}
		encoder->private = dsi_config;
		dsi_config->encoders[MDFLD_DSI_ENCODER_DBI] = encoder;
		encoder->base.type = (pipe == 0) ? INTEL_OUTPUT_MIPI :
			INTEL_OUTPUT_MIPI2;
		psb_connector->encoder = &encoder->base;

		if (pipe == 2)
			dev_priv->encoder2 = encoder;
	
		if (pipe == 0)
			dev_priv->encoder0 = encoder;
	} else if (dsi_config->type == MDFLD_DSI_ENCODER_DPI) {
		encoder = mdfld_dsi_dpi_init(dev, dsi_connector, p_vid_funcs);
		if(!encoder) {
			DRM_ERROR("Create DPI encoder failed\n");
			goto dsi_init_err2;
		}
		encoder->private = dsi_config;
		dsi_config->encoders[MDFLD_DSI_ENCODER_DPI] = encoder;
		encoder->base.type = (pipe == 0) ? INTEL_OUTPUT_MIPI :
			INTEL_OUTPUT_MIPI2;
		psb_connector->encoder = &encoder->base;

		if (pipe == 2)
			dev_priv->encoder2 = encoder;

		if (pipe == 0)
			dev_priv->encoder0 = encoder;
	}

	drm_connector_register(connector);

#ifdef CONFIG_CTP_DPST
	/* DPST: TODO - get appropriate connector */
	if (dev_priv->dpst_lvds_connector == 0)
		dev_priv->dpst_lvds_connector = connector;
#endif

	/*init dsr*/
	if (mdfld_dsi_dsr_init(dsi_config))
		DRM_INFO("%s: Failed to initialize DSR\n", __func__);

	pr_debug("successfully\n");
	return;

	/*TODO: add code to destroy outputs on error*/
dsi_init_err2:
	mdfld_dsi_error_detector_exit(dsi_connector);
dsi_init_err1:
	/*destroy sender*/
	mdfld_dsi_pkg_sender_destroy(dsi_connector->pkg_sender);

	drm_connector_cleanup(connector);

	if (dsi_config) {
		kfree(dsi_config);
		if (pipe)
			dev_priv->dsi_configs[1] = NULL;
		else
			dev_priv->dsi_configs[0] = NULL;
	}
dsi_init_err0:
	if (dsi_connector)
		kfree(dsi_connector);

	return;
}
