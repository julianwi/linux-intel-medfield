/* SPDX-License-Identifier: GPL-2.0-only */
/**************************************************************************
 * Copyright (c) 2007-2011, Intel Corporation.
 * All Rights Reserved.
 *
 **************************************************************************/

#ifndef _PSB_DRV_H_
#define _PSB_DRV_H_

#include <linux/kref.h>
#include <linux/mm_types.h>

#include <drm/drm_device.h>

#include "gtt.h"
#include "intel_bios.h"
#include "mmu.h"
#include "oaktrail.h"
#include "opregion.h"
#include "power.h"
#include "psb_intel_drv.h"
#include "psb_reg.h"
#include "psb_drm.h"

#define DRIVER_AUTHOR "Alan Cox <alan@linux.intel.com> and others"

#define DRIVER_NAME "gma500"
#define DRIVER_DESC "DRM driver for the Intel GMA500, GMA600, GMA3600, GMA3650"
#define DRIVER_DATE "20140314"

#define DRIVER_MAJOR 1
#define DRIVER_MINOR 0
#define DRIVER_PATCHLEVEL 0

/* Append new drm mode definition here, align with libdrm definition */
#define DRM_MODE_SCALE_NO_SCALE   	4

/* sys interface variables */
extern bool gbdispstatus;
extern int drm_psb_debug;
extern int drm_psb_enable_cabc ;
extern int gfxrtdelay;
extern int drm_psb_te_timer_delay;
extern int drm_psb_enable_gamma;
extern int drm_psb_adjust_contrast;
extern int drm_psb_adjust_brightness;
extern int drm_psb_enable_color_conversion;
extern u32 DISP_PLANEB_STATUS;
extern int drm_psb_use_cases_control;
extern int dpst_level;

#define IS_PSB(drm) ((to_pci_dev((drm)->dev)->device & 0xfffe) == 0x8108)
#define IS_MRST(drm) ((to_pci_dev((drm)->dev)->device & 0xfff0) == 0x4100)
#define IS_MFLD(drm) ((to_pci_dev((drm)->dev)->device & 0xfff8) == 0x0130)
#define IS_CDV(drm) ((to_pci_dev((drm)->dev)->device & 0xfff0) == 0x0be0)

/* Hardware offsets */
#define PSB_VDC_OFFSET		 0x00000000
#define PSB_VDC_SIZE		 0x000080000
#define MRST_MMIO_SIZE		 0x0000C0000
#define MDFLD_MMIO_SIZE          0x000100000
#define PSB_SGX_SIZE		 0x8000
#define PSB_SGX_OFFSET		 0x00040000
#define MRST_SGX_OFFSET		 0x00080000

/* PCI resource identifiers */
#define PSB_MMIO_RESOURCE	 0
#define PSB_AUX_RESOURCE	 0
#define PSB_GATT_RESOURCE	 2
#define PSB_GTT_RESOURCE	 3

/* PCI configuration */
#define PSB_GMCH_CTRL		 0x52
#define PSB_BSM			 0x5C
#define _PSB_GMCH_ENABLED	 0x4
#define PSB_PGETBL_CTL		 0x2020
#define _PSB_PGETBL_ENABLED	 0x00000001
#define PSB_SGX_2D_SLAVE_PORT	 0x4000
#define PSB_LPC_GBA		 0x44

/* TODO: To get rid of */
#define PSB_TT_PRIV0_LIMIT	 (256*1024*1024)
#define PSB_TT_PRIV0_PLIMIT	 (PSB_TT_PRIV0_LIMIT >> PAGE_SHIFT)

/* SGX side MMU definitions (these can probably go) */

/* Flags for external memory type field */
#define PSB_MMU_CACHED_MEMORY	  0x0001	/* Bind to MMU only */
#define PSB_MMU_RO_MEMORY	  0x0002	/* MMU RO memory */
#define PSB_MMU_WO_MEMORY	  0x0004	/* MMU WO memory */

/* PTE's and PDE's */
#define PSB_PDE_MASK		  0x003FFFFF
#define PSB_PDE_SHIFT		  22
#define PSB_PTE_SHIFT		  12

/* Cache control */
#define PSB_PTE_VALID		  0x0001	/* PTE / PDE valid */
#define PSB_PTE_WO		  0x0002	/* Write only */
#define PSB_PTE_RO		  0x0004	/* Read only */
#define PSB_PTE_CACHED		  0x0008	/* CPU cache coherent */

/* VDC registers and bits */
#define PSB_MSVDX_CLOCKGATING	  0x2064
#define PSB_TOPAZ_CLOCKGATING	  0x2068
#define PSB_HWSTAM		  0x2098
#define PSB_INSTPM		  0x20C0
#define PSB_INT_IDENTITY_R        0x20A4
#define _PSB_IRQ_ASLE		  (1<<0)
#define _MDFLD_PIPEC_EVENT_FLAG   (1<<2)
#define _MDFLD_PIPEC_VBLANK_FLAG  (1<<3)
#define _PSB_DPST_PIPEB_FLAG      (1<<4)
#define _MDFLD_PIPEB_EVENT_FLAG   (1<<4)
#define _PSB_VSYNC_PIPEB_FLAG	  (1<<5)
#define _PSB_DPST_PIPEA_FLAG      (1<<6)
#define _PSB_PIPEA_EVENT_FLAG     (1<<6)
#define _PSB_VSYNC_PIPEA_FLAG	  (1<<7)
#define _MDFLD_MIPIA_FLAG	  (1<<16)
#define _MDFLD_MIPIC_FLAG	  (1<<17)
#define _PSB_IRQ_DISP_HOTSYNC	  (1<<17)
#define _PSB_IRQ_SGX_FLAG	  (1<<18)
#define _PSB_IRQ_MSVDX_FLAG	  (1<<19)
#define _LNC_IRQ_TOPAZ_FLAG	  (1<<20)

#define _PSB_PIPE_EVENT_FLAG	(_PSB_VSYNC_PIPEA_FLAG | \
				 _PSB_VSYNC_PIPEB_FLAG)

/* This flag includes all the display IRQ bits excepts the vblank irqs. */
#define _MDFLD_DISP_ALL_IRQ_FLAG (_MDFLD_PIPEC_EVENT_FLAG | \
				  _MDFLD_PIPEB_EVENT_FLAG | \
				  _PSB_PIPEA_EVENT_FLAG | \
				  _PSB_VSYNC_PIPEA_FLAG | \
				  _MDFLD_MIPIA_FLAG | \
				  _MDFLD_MIPIC_FLAG)
#define PSB_INT_IDENTITY_R	  0x20A4
#define PSB_INT_MASK_R		  0x20A8
#define PSB_INT_ENABLE_R	  0x20A0

#define _PSB_MMU_ER_MASK      0x0001FF00
#define _PSB_MMU_ER_HOST      (1 << 16)
#define GPIOA			0x5010
#define GPIOB			0x5014
#define GPIOC			0x5018
#define GPIOD			0x501c
#define GPIOE			0x5020
#define GPIOF			0x5024
#define GPIOG			0x5028
#define GPIOH			0x502c
#define GPIO_CLOCK_DIR_MASK		(1 << 0)
#define GPIO_CLOCK_DIR_IN		(0 << 1)
#define GPIO_CLOCK_DIR_OUT		(1 << 1)
#define GPIO_CLOCK_VAL_MASK		(1 << 2)
#define GPIO_CLOCK_VAL_OUT		(1 << 3)
#define GPIO_CLOCK_VAL_IN		(1 << 4)
#define GPIO_CLOCK_PULLUP_DISABLE	(1 << 5)
#define GPIO_DATA_DIR_MASK		(1 << 8)
#define GPIO_DATA_DIR_IN		(0 << 9)
#define GPIO_DATA_DIR_OUT		(1 << 9)
#define GPIO_DATA_VAL_MASK		(1 << 10)
#define GPIO_DATA_VAL_OUT		(1 << 11)
#define GPIO_DATA_VAL_IN		(1 << 12)
#define GPIO_DATA_PULLUP_DISABLE	(1 << 13)

#define VCLK_DIVISOR_VGA0   0x6000
#define VCLK_DIVISOR_VGA1   0x6004
#define VCLK_POST_DIV	    0x6010

#define PSB_COMM_2D (PSB_ENGINE_2D << 4)
#define PSB_COMM_3D (PSB_ENGINE_3D << 4)
#define PSB_COMM_TA (PSB_ENGINE_TA << 4)
#define PSB_COMM_HP (PSB_ENGINE_HP << 4)
#define PSB_COMM_USER_IRQ (1024 >> 2)
#define PSB_COMM_USER_IRQ_LOST (PSB_COMM_USER_IRQ + 1)
#define PSB_COMM_FW (2048 >> 2)

#define PSB_UIRQ_VISTEST	       1
#define PSB_UIRQ_OOM_REPLY	       2
#define PSB_UIRQ_FIRE_TA_REPLY	       3
#define PSB_UIRQ_FIRE_RASTER_REPLY     4

#define PSB_2D_SIZE (256*1024*1024)
#define PSB_MAX_RELOC_PAGES 1024

#define PSB_LOW_REG_OFFS 0x0204
#define PSB_HIGH_REG_OFFS 0x0600

#define PSB_NUM_VBLANKS 2


#define PSB_2D_SIZE (256*1024*1024)
#define PSB_MAX_RELOC_PAGES 1024

#define PSB_LOW_REG_OFFS 0x0204
#define PSB_HIGH_REG_OFFS 0x0600

#define PSB_NUM_VBLANKS 2
#define PSB_WATCHDOG_DELAY (HZ * 2)
#define PSB_LID_DELAY (HZ / 10)

#define PSB_MAX_BRIGHTNESS		100

#define MDFLD_PNW_A0 0x00
#define MDFLD_PNW_B0 0x04
#define MDFLD_PNW_C0 0x08

#define MDFLD_DSR_2D_3D_0 	(1 << 0)
#define MDFLD_DSR_2D_3D_2 	(1 << 1)
#define MDFLD_DSR_CURSOR_0 	(1 << 2)
#define MDFLD_DSR_CURSOR_2	(1 << 3)
#define MDFLD_DSR_OVERLAY_0 	(1 << 4)
#define MDFLD_DSR_OVERLAY_2 	(1 << 5)
#define MDFLD_DSR_MIPI_CONTROL	(1 << 6)
#define MDFLD_DSR_DAMAGE_MASK_0	((1 << 0)|(1 << 2)|(1 << 4)|(1 << 6))
#define MDFLD_DSR_DAMAGE_MASK_2	((1 << 1)|(1 << 3)|(1 << 5)|(1 << 6))
#define MDFLD_DSR_2D_3D 	(MDFLD_DSR_2D_3D_0 | MDFLD_DSR_2D_3D_2)

#define MDFLD_DSR_RR		45
#define MDFLD_DPU_ENABLE 	(1 << 31)
#define MDFLD_DSR_FULLSCREEN 	(1 << 30)
#define MDFLD_DSR_DELAY		(HZ / MDFLD_DSR_RR)

#define PSB_PWR_STATE_ON		1
#define PSB_PWR_STATE_OFF		2

#define PSB_PMPOLICY_NOPM		0
#define PSB_PMPOLICY_CLOCKGATING	1
#define PSB_PMPOLICY_POWERDOWN		2

#define PSB_PMSTATE_POWERUP		0
#define PSB_PMSTATE_CLOCKGATED		1
#define PSB_PMSTATE_POWERDOWN		2
#define PSB_PCIx_MSI_ADDR_LOC		0x94
#define PSB_PCIx_MSI_DATA_LOC		0x98

#define MDFLD_PLANE_MAX_WIDTH		2048
#define MDFLD_PLANE_MAX_HEIGHT		2048
#define PANEL_NAME_MAX_LEN	        16

#define MAX_NUM 0xffffffff

struct drm_fb_helper;

struct intel_mid_vbt {
	char signature[4];		/*4 bytes,"$GCT" */
	uint8_t revision;		/*1 byte GCT version*/
	uint8_t checksum;		/*1 byte checksum*/
	uint16_t size;			/*2 byte size of checksumed data*/
	uint8_t num_of_panel_desc;	/*1 byte number of panel descriptor*/
	uint8_t primary_panel_idx;	/*1 byte primary panel descriptor idx*/
	uint8_t secondary_panel_idx;	/*1 byte secondary panel desc idx*/
	uint8_t splash_flag;		/*1 byte bit 0 is to disable splash*/
	uint8_t reserved[4];		/*[0..1] relates to GPU burst for R20*/
	void *panel_descs;
} __packed;

struct psb_intel_opregion {
	struct opregion_header *header;
	struct opregion_acpi *acpi;
	struct opregion_swsci *swsci;
	struct opregion_asle *asle;
	void *vbt;
	u32 __iomem *lid_state;
	struct work_struct asle_work;
};

struct sdvo_device_mapping {
	u8 initialized;
	u8 dvo_port;
	u8 slave_addr;
	u8 dvo_wiring;
	u8 i2c_pin;
	u8 i2c_speed;
	u8 ddc_pin;
};

struct intel_gmbus {
	struct i2c_adapter adapter;
	struct i2c_adapter *force_bit;
	u32 reg0;
};

/* Register offset maps */
struct psb_offset {
	u32	fp0;
	u32	fp1;
	u32	cntr;
	u32	conf;
	u32	src;
	u32	dpll;
	u32	dpll_md;
	u32	htotal;
	u32	hblank;
	u32	hsync;
	u32	vtotal;
	u32	vblank;
	u32	vsync;
	u32	stride;
	u32	size;
	u32	pos;
	u32	surf;
	u32	addr;
	u32	base;
	u32	status;
	u32	linoff;
	u32	tileoff;
	u32	palette;
};

/*
 *User options.
 */

struct drm_psb_uopt {
	int pad; /*keep it here in case we use it in future*/
};

typedef int (*pfn_vsync_handler)(struct drm_device *dev, int pipe);
typedef int(*pfn_screen_event_handler)(struct drm_device *psDrmDevice, int state);

/*
 *	Register save state. This is used to hold the context when the
 *	device is powered off. In the case of Oaktrail this can (but does not
 *	yet) include screen blank. Operations occuring during the save
 *	update the register cache instead.
 */

/* Common status for pipes */
struct psb_pipe {
	u32	fp0;
	u32	fp1;
	u32	cntr;
	u32	conf;
	u32	src;
	u32	dpll;
	u32	dpll_md;
	u32	htotal;
	u32	hblank;
	u32	hsync;
	u32	vtotal;
	u32	vblank;
	u32	vsync;
	u32	stride;
	u32	size;
	u32	pos;
	u32	base;
	u32	surf;
	u32	addr;
	u32	status;
	u32	linoff;
	u32	tileoff;
	u32	palette[256];
};

struct psb_state {
	uint32_t saveVCLK_DIVISOR_VGA0;
	uint32_t saveVCLK_DIVISOR_VGA1;
	uint32_t saveVCLK_POST_DIV;
	uint32_t saveVGACNTRL;
	uint32_t saveADPA;
	uint32_t saveLVDS;
	uint32_t saveDVOA;
	uint32_t saveDVOB;
	uint32_t saveDVOC;
	uint32_t savePP_ON;
	uint32_t savePP_OFF;
	uint32_t savePP_CONTROL;
	uint32_t savePP_CYCLE;
	uint32_t savePFIT_CONTROL;
	uint32_t saveCLOCKGATING;
	uint32_t saveDSPARB;
	uint32_t savePFIT_AUTO_RATIOS;
	uint32_t savePFIT_PGM_RATIOS;
	uint32_t savePP_ON_DELAYS;
	uint32_t savePP_OFF_DELAYS;
	uint32_t savePP_DIVISOR;
	uint32_t saveBCLRPAT_A;
	uint32_t saveBCLRPAT_B;
	uint32_t savePERF_MODE;
	uint32_t saveDSPFW1;
	uint32_t saveDSPFW2;
	uint32_t saveDSPFW3;
	uint32_t saveDSPFW4;
	uint32_t saveDSPFW5;
	uint32_t saveDSPFW6;
	uint32_t saveCHICKENBIT;
	uint32_t saveDSPACURSOR_CTRL;
	uint32_t saveDSPBCURSOR_CTRL;
	uint32_t saveDSPACURSOR_BASE;
	uint32_t saveDSPBCURSOR_BASE;
	uint32_t saveDSPACURSOR_POS;
	uint32_t saveDSPBCURSOR_POS;
	uint32_t saveOV_OVADD;
	uint32_t saveOV_OGAMC0;
	uint32_t saveOV_OGAMC1;
	uint32_t saveOV_OGAMC2;
	uint32_t saveOV_OGAMC3;
	uint32_t saveOV_OGAMC4;
	uint32_t saveOV_OGAMC5;
	uint32_t saveOVC_OVADD;
	uint32_t saveOVC_OGAMC0;
	uint32_t saveOVC_OGAMC1;
	uint32_t saveOVC_OGAMC2;
	uint32_t saveOVC_OGAMC3;
	uint32_t saveOVC_OGAMC4;
	uint32_t saveOVC_OGAMC5;

	/* DPST register save */
	uint32_t saveHISTOGRAM_INT_CONTROL_REG;
	uint32_t saveHISTOGRAM_LOGIC_CONTROL_REG;
	uint32_t savePWM_CONTROL_LOGIC;
};

struct medfield_state {
	uint32_t saveMIPI;
	uint32_t saveMIPI_C;

	uint32_t savePFIT_CONTROL;
	uint32_t savePFIT_PGM_RATIOS;
	uint32_t saveHDMIPHYMISCCTL;
	uint32_t saveHDMIB_CONTROL;
};

struct cdv_state {
	uint32_t saveDSPCLK_GATE_D;
	uint32_t saveRAMCLK_GATE_D;
	uint32_t saveDSPARB;
	uint32_t saveDSPFW[6];
	uint32_t saveADPA;
	uint32_t savePP_CONTROL;
	uint32_t savePFIT_PGM_RATIOS;
	uint32_t saveLVDS;
	uint32_t savePFIT_CONTROL;
	uint32_t savePP_ON_DELAYS;
	uint32_t savePP_OFF_DELAYS;
	uint32_t savePP_CYCLE;
	uint32_t saveVGACNTRL;
	uint32_t saveIER;
	uint32_t saveIMR;
	u8	 saveLBB;
};

struct psb_save_area {
	struct psb_pipe pipe[3];
	uint32_t saveBSM;
	uint32_t saveVBT;
	union {
	        struct psb_state psb;
		struct medfield_state mdfld;
		struct cdv_state cdv;
	};
	uint32_t saveBLC_PWM_CTL2;
	uint32_t saveBLC_PWM_CTL;
};

#define MODE_SETTING_IN_CRTC 	0x1
#define MODE_SETTING_IN_ENCODER 0x2
#define MODE_SETTING_ON_GOING 	0x3
#define MODE_SETTING_IN_DSR 	0x4
#define MODE_SETTING_ENCODER_DONE 0x8
#define GCT_R10_HEADER_SIZE		16
#define GCT_R10_DISPLAY_DESC_SIZE	28
#define GCT_R11_HEADER_SIZE		16
#define GCT_R11_DISPLAY_DESC_SIZE	44
#define GCT_R20_HEADER_SIZE		16
#define GCT_R20_DISPLAY_DESC_SIZE	48

#define PSB_REG_PRINT_SIZE    40960

#define	LOG2_WB_FIFO_SIZE	(5)
#define	WB_FIFO_SIZE		(1 << (LOG2_WB_FIFO_SIZE))

struct psb_validate_buffer;
struct psb_video_ctx;

struct platform_panel_info {
	char name[PANEL_NAME_MAX_LEN+1];
	int  mode;
};

struct psb_ops;

#define PSB_NUM_PIPE		3

struct intel_scu_ipc_dev;

struct drm_psb_private {
	struct work_struct te_work;
	struct work_struct reset_panel_work;

	struct mutex vsync_lock;
	struct work_struct vsync_event_work;
	int vsync_pipe;
	wait_queue_head_t vsync_queue;
	atomic_t *vblank_count;
	int vblank_disable_cnt;
	bool vsync_enabled;
	wait_queue_head_t dbi_fifo_query_queue;
	atomic_t dbi_fifo_query;
	struct hrtimer dbi_fifo_query_hrtimer;
	spinlock_t dbi_fifo_query_timer_lock;

	struct vm_operations_struct *ttm_vm_ops;

	struct drm_device dev;

	struct pci_dev *aux_pdev; /* Currently only used by mrst */
	struct pci_dev *lpc_pdev; /* Currently only used by mrst */
	const struct psb_ops *ops;
	const struct psb_offset *regmap;

	struct child_device_config *child_dev;
	int child_dev_num;

	struct drm_psb_uopt uopt;

	struct psb_gtt gtt;

	/* GTT Memory manager */
	struct psb_gtt_mm *gtt_mm;
	struct page *scratch_page;
	u32 *gtt_map;
	uint32_t stolen_base;
	void *vram_addr;
	unsigned long vram_stolen_size;
	u16 gmch_ctrl;		/* Saved GTT setup */
	u32 pge_ctl;

	struct mutex gtt_mutex;
	struct resource *gtt_mem;	/* Our PCI resource */

	struct mutex mmap_mutex;

	struct psb_mmu_driver *mmu;
	struct psb_mmu_pd *pf_pd;

	/* Moorestown MM backlight cache */
	uint8_t saveBKLTCNT;
	uint8_t saveBKLTREQ;
	uint8_t saveBKLTBRTL;

	/* Register base */
	uint8_t *sgx_reg;
	uint8_t *vdc_reg;
	uint8_t __iomem *aux_reg; /* Auxillary vdc pipe regs */
	uint16_t lpc_gpio_base;
	uint32_t gatt_free_offset;

	spinlock_t video_ctx_lock;
	/* Current video context */
	struct psb_video_ctx *topaz_ctx;
	/* previous vieo context */
	struct psb_video_ctx *last_topaz_ctx;

	/*
	 *MSVDX
	 */
	uint8_t *msvdx_reg;
	atomic_t msvdx_mmu_invaldc;
	void *msvdx_private;

	/*
	 *TOPAZ
	 */
	uint8_t *topaz_reg;
	void *topaz_private;
	uint8_t topaz_disabled;
	uint32_t video_device_fuse;
	atomic_t topaz_mmu_invaldc;

	/* Fencing / irq */
	uint32_t vdc_irq_mask;
	uint32_t pipestat[PSB_NUM_PIPE];
	bool vblanksEnabledForFlips;

	spinlock_t irqmask_lock;

	/* Power */
	bool pm_initialized;

	/* Modesetting */
	struct psb_intel_mode_device mode_dev;
	bool modeset;	/* true if we have done the mode_device setup */

	struct drm_crtc *plane_to_crtc_mapping[PSB_NUM_PIPE];
	struct drm_crtc *pipe_to_crtc_mapping[PSB_NUM_PIPE];
	uint32_t num_pipe;

	/* OSPM info (Power management base) (TODO: can go ?) */
	uint32_t ospm_base;

	/*
	 * CI share buffer
	 */
	unsigned int ci_region_start;
	unsigned int ci_region_size;

	/*
	 * IMR share buffer;
	 */
	unsigned int imr_region_start;
	unsigned int imr_region_size;

	/*
	 *Memory managers
	 */

	int have_imr;
	int have_tt;
	int have_mem_mmu;
	int have_mem_mmu_tiling;

	/*
	 *Relocation buffer mapping.
	 */

	spinlock_t reloc_lock;
	unsigned int rel_mapped_pages;
	wait_queue_head_t rel_mapped_queue;

	/*
	 *SAREA
	 */
	struct drm_psb_sarea *sarea_priv;

	/*
	 * OSPM info (Power management base) (can go ?)
	*/
	uint8_t panel_desc;
	bool early_suspended;

	/* Sizes info */
	u32 fuse_reg_value;

	/* vbt (gct) header information*/
	struct intel_mid_vbt vbt_data;

	int panel_id;

	/* PCI revision ID for B0:D2:F0 */
	uint8_t platform_rev_id;

	/* gmbus */
	struct intel_gmbus *gmbus;
	uint8_t __iomem *gmbus_reg;

	/* Used by SDVO */
	int crt_ddc_pin;
	/* FIXME: The mappings should be parsed from bios but for now we can
		  pretend there are no mappings available */
	struct sdvo_device_mapping sdvo_mappings[2];
	u32 hotplug_supported_mask;
	struct drm_property *broadcast_rgb_property;
	struct drm_property *force_audio_property;

	/* LVDS info */
	int backlight_duty_cycle;	/* restore backlight to this value */
	struct drm_display_mode *panel_fixed_mode;
	struct drm_display_mode *lfp_lvds_vbt_mode;
	struct drm_display_mode *sdvo_lvds_vbt_mode;

	struct bdb_lvds_backlight *lvds_bl; /* LVDS backlight info from VBT */
	struct gma_i2c_chan *lvds_i2c_bus; /* FIXME: Remove this? */

	/* Feature bits from the VBIOS */
	unsigned int int_tv_support:1;
	unsigned int lvds_dither:1;
	unsigned int lvds_vbt:1;
	unsigned int int_crt_support:1;
	unsigned int lvds_use_ssc:1;
	int lvds_ssc_freq;
	bool is_lvds_on;
	bool lvds_enabled_in_vbt;

	/* MRST private date start */
	unsigned int core_freq;
	uint32_t iLVDS_enable;

	/* MRST_DSI private date start */
	struct work_struct dsi_work;

	/*
	 *MRST DSI info
	 */

	/* The DPI panel power on */
	bool dpi_panel_on[3];

	/* The DPI display */
	bool dpi;

	/* Set if MIPI encoder wants to control plane/pipe */
	bool dsi_plane_pipe_control;

	/* status */
	uint32_t videoModeFormat:2;
	uint32_t laneCount:3;
	uint32_t channelNumber:2;
	uint32_t status_reserved:25;

	/* dual display - DPI & DBI */
	bool dual_display;

	/* HS or LP transmission */
	bool lp_transmission;

	/* configuration phase */
	bool config_phase;

	/* first boot phase */
	bool first_boot;

	bool is_mipi_on;
	u32 mipi_ctrl_display;

	/* DSI clock */
	uint32_t RRate;
	uint32_t DDR_Clock;
	uint32_t DDR_Clock_Calculated;
	uint32_t ClockBits;

	/* DBI Buffer pointer */
	u32 DBI_CB_phys;
	u8 *p_DBI_commandBuffer;
	uint32_t DBI_CB_pointer;
	u8 *p_DBI_dataBuffer_orig;
	u8 *p_DBI_dataBuffer;
	uint32_t DBI_DB_pointer;

	uint32_t bpp:5;

	/* MDFLD_DSI private date start */
	/* dual display - DPI & DBI */
	bool dual_mipi;
	uint32_t ksel;
	uint32_t mipi_lane_config;
	/*
	 *MRST DSI info
	 */
	/* The DPI panel power on */
	bool dpi_panel_on2;

	/* The DPI display */
	bool dpi2;

	/* status */
	uint32_t videoModeFormat2:2;
	uint32_t laneCount2:3;
	uint32_t channelNumber2:2;
	uint32_t status_reserved2:25;

	/* HS or LP transmission */
	bool lp_transmission2;

	/* configuration phase */
	bool config_phase2;

	/* DSI clock */
	uint32_t RRate2;
	uint32_t DDR_Clock2;
	uint32_t DDR_Clock_Calculated2;
	uint32_t ClockBits2;

	/* DBI Buffer pointer */
	u32 DBI_CB_phys2;
	u8 *p_DBI_commandBuffer2;
	uint32_t DBI_CB_pointer2;
	u8 *p_DBI_dataBuffer_orig2;
	u8 *p_DBI_dataBuffer2;

	/* DSI panel spec */
	uint32_t pixelClock2;
	uint32_t HsyncWidth2;
	uint32_t HbackPorch2;
	uint32_t HfrontPorch2;
	uint32_t HactiveArea2;
	uint32_t VsyncWidth2;
	uint32_t VbackPorch2;
	uint32_t VfrontPorch2;
	uint32_t VactiveArea2;
	uint32_t bpp2:5;
	uint32_t Reserved2:27;
	struct mdfld_dsi_dbi_output *dbi_output;
	struct mdfld_dsi_dbi_output *dbi_output2;
	struct mdfld_dsi_dpi_output *dpi_output;
	struct mdfld_dsi_dpi_output *dpi_output2;
	/* MDFLD_DSI private date end */

	/* MID specific */
	bool use_msi;
	bool has_gct;
	struct oaktrail_gct_data gct_data;

	/* Oaktrail HDMI state */
	struct oaktrail_hdmi_dev *hdmi_priv;

	/* Medfield specific register save state */
	uint32_t saveHDMIPHYMISCCTL;
	uint32_t saveHDMIB_CONTROL;
	uint32_t saveDSPCCNTR;
	uint32_t savePIPECCONF;
	uint32_t savePIPECSRC;
	uint32_t saveHTOTAL_C;
	uint32_t saveHBLANK_C;
	uint32_t saveHSYNC_C;
	uint32_t saveVTOTAL_C;
	uint32_t saveVBLANK_C;
	uint32_t saveVSYNC_C;
	uint32_t saveDSPCSTRIDE;
	uint32_t saveDSPCSIZE;
	uint32_t saveDSPCPOS;
	uint32_t saveDSPCSURF;
	uint32_t saveDSPCSTATUS;
	uint32_t saveDSPCLINOFF;
	uint32_t saveDSPCTILEOFF;
	uint32_t saveDSPCCURSOR_CTRL;
	uint32_t saveDSPCCURSOR_BASE;
	uint32_t saveDSPCCURSOR_POS;
	uint32_t save_palette_c[256];
	uint32_t saveOV_OVADD_C;
	uint32_t saveOV_OGAMC0_C;
	uint32_t saveOV_OGAMC1_C;
	uint32_t saveOV_OGAMC2_C;
	uint32_t saveOV_OGAMC3_C;
	uint32_t saveOV_OGAMC4_C;
	uint32_t saveOV_OGAMC5_C;

	/* DSI register save */
	uint32_t saveDEVICE_READY_REG;
	uint32_t saveINTR_EN_REG;
	uint32_t saveDSI_FUNC_PRG_REG;
	uint32_t saveHS_TX_TIMEOUT_REG;
	uint32_t saveLP_RX_TIMEOUT_REG;
	uint32_t saveTURN_AROUND_TIMEOUT_REG;
	uint32_t saveDEVICE_RESET_REG;
	uint32_t saveDPI_RESOLUTION_REG;
	uint32_t saveHORIZ_SYNC_PAD_COUNT_REG;
	uint32_t saveHORIZ_BACK_PORCH_COUNT_REG;
	uint32_t saveHORIZ_FRONT_PORCH_COUNT_REG;
	uint32_t saveHORIZ_ACTIVE_AREA_COUNT_REG;
	uint32_t saveVERT_SYNC_PAD_COUNT_REG;
	uint32_t saveVERT_BACK_PORCH_COUNT_REG;
	uint32_t saveVERT_FRONT_PORCH_COUNT_REG;
	uint32_t saveHIGH_LOW_SWITCH_COUNT_REG;
	uint32_t saveINIT_COUNT_REG;
	uint32_t saveMAX_RET_PAK_REG;
	uint32_t saveVIDEO_FMT_REG;
	uint32_t saveEOT_DISABLE_REG;
	uint32_t saveLP_BYTECLK_REG;
	uint32_t saveHS_LS_DBI_ENABLE_REG;
	uint32_t saveTXCLKESC_REG;
	uint32_t saveDPHY_PARAM_REG;
	uint32_t saveMIPI_CONTROL_REG;
	uint32_t saveMIPI;
	uint32_t saveMIPI_C;

	/* DPST register save */
	uint32_t saveHISTOGRAM_INT_CONTROL_REG;
	uint32_t saveHISTOGRAM_LOGIC_CONTROL_REG;
	uint32_t savePWM_CONTROL_LOGIC;

	/* Register state */
	uint32_t saveDSPACNTR;
	uint32_t saveDSPBCNTR;
	uint32_t savePIPEACONF;
	uint32_t savePIPEBCONF;
	uint32_t savePIPEASRC;
	uint32_t savePIPEBSRC;
	uint32_t saveFPA0;
	uint32_t saveFPA1;
	uint32_t saveDPLL_A;
	uint32_t saveDPLL_A_MD;
	uint32_t saveHTOTAL_A;
	uint32_t saveHBLANK_A;
	uint32_t saveHSYNC_A;
	uint32_t saveVTOTAL_A;
	uint32_t saveVBLANK_A;
	uint32_t saveVSYNC_A;
	uint32_t saveDSPASTRIDE;
	uint32_t saveDSPASIZE;
	uint32_t saveDSPAPOS;
	uint32_t saveDSPABASE;
	uint32_t saveDSPASURF;
	uint32_t saveDSPASTATUS;
	uint32_t saveFPB0;
	uint32_t saveFPB1;
	uint32_t saveDPLL_B;
	uint32_t saveDATALANES_B;
	uint32_t saveDPLL_B_MD;
	uint32_t saveHTOTAL_B;
	uint32_t saveHBLANK_B;
	uint32_t saveHSYNC_B;
	uint32_t saveVTOTAL_B;
	uint32_t saveVBLANK_B;
	uint32_t saveVSYNC_B;
	uint32_t saveDSPBSTRIDE;
	uint32_t saveDSPBSIZE;
	uint32_t saveDSPBPOS;
	uint32_t saveDSPBBASE;
	uint32_t saveDSPBSURF;
	uint32_t saveDSPBSTATUS;
	uint32_t saveVCLK_DIVISOR_VGA0;
	uint32_t saveVCLK_DIVISOR_VGA1;
	uint32_t saveVCLK_POST_DIV;
	uint32_t saveVGACNTRL;
	uint32_t saveADPA;
	uint32_t saveLVDS;
	uint32_t saveDVOA;
	uint32_t saveDVOB;
	uint32_t saveDVOC;
	uint32_t savePP_ON;
	uint32_t savePP_OFF;
	uint32_t savePP_CONTROL;
	uint32_t savePP_CYCLE;
	uint32_t savePFIT_CONTROL;
	uint32_t savePaletteA[256];
	uint32_t savePaletteB[256];
	uint32_t saveBLC_PWM_CTL2;
	uint32_t saveBLC_PWM_CTL;
	uint32_t saveCLOCKGATING;
	uint32_t saveDSPARB;
	uint32_t saveDSPATILEOFF;
	uint32_t saveDSPBTILEOFF;
	uint32_t saveDSPAADDR;
	uint32_t saveDSPBADDR;
	uint32_t savePFIT_AUTO_RATIOS;
	uint32_t savePFIT_PGM_RATIOS;
	uint32_t savePP_ON_DELAYS;
	uint32_t savePP_OFF_DELAYS;
	uint32_t savePP_DIVISOR;
	uint32_t saveBSM;
	uint32_t saveVBT;
	uint32_t saveBCLRPAT_A;
	uint32_t saveBCLRPAT_B;
	uint32_t saveDSPALINOFF;
	uint32_t saveDSPBLINOFF;
	uint32_t saveVED_CG_DIS;
	uint32_t saveVEC_CG_DIS;
	uint32_t savePERF_MODE;
	uint32_t saveGL3_CTL;
	uint32_t saveGL3_USE_WRT_INVAL;
	uint32_t saveDSPFW1;
	uint32_t saveDSPFW2;
	uint32_t saveDSPFW3;
	uint32_t saveDSPFW4;
	uint32_t saveDSPFW5;
	uint32_t saveDSPFW6;
	uint32_t saveCHICKENBIT;
	uint32_t saveDSPACURSOR_CTRL;
	uint32_t saveDSPBCURSOR_CTRL;
	uint32_t saveDSPACURSOR_BASE;
	uint32_t saveDSPBCURSOR_BASE;
	uint32_t saveDSPACURSOR_POS;
	uint32_t saveDSPBCURSOR_POS;
	uint32_t save_palette_a[256];
	uint32_t save_palette_b[256];
	uint32_t save_color_coef_a[6];
	uint32_t save_color_coef_b[6];
	uint32_t save_color_coef_c[6];
	uint32_t saveOV_OVADD;
	uint32_t saveOV_OGAMC0;
	uint32_t saveOV_OGAMC1;
	uint32_t saveOV_OGAMC2;
	uint32_t saveOV_OGAMC3;
	uint32_t saveOV_OGAMC4;
	uint32_t saveOV_OGAMC5;
	uint32_t saveOVC_OVADD;
	uint32_t saveOVC_OGAMC0;
	uint32_t saveOVC_OGAMC1;
	uint32_t saveOVC_OGAMC2;
	uint32_t saveOVC_OGAMC3;
	uint32_t saveOVC_OGAMC4;
	uint32_t saveOVC_OGAMC5;
	struct psb_save_area regs;

	/* Hotplug handling */
	struct work_struct hotplug_work;

	/* Scheduling */
	struct mutex cmdbuf_mutex;
	/*uint32_t ta_mem_pages;
	struct psb_ta_mem *ta_mem;
	int force_ta_mem_load;
	atomic_t val_seq;*/

	/* DSI info */
	void * dbi_dsr_info;
	void * dbi_dpu_info;
	void * dsi_configs[2];

	/* LID-Switch */
	spinlock_t lid_lock;
	struct timer_list lid_timer;
	u32 *lid_state;
	struct psb_intel_opregion opregion;
	u32 lid_last_state;

	/* Watchdog */
	spinlock_t watchdog_lock;
	struct timer_list watchdog_timer;
	struct work_struct watchdog_wq;
	struct work_struct msvdx_watchdog_wq;
	struct work_struct topaz_watchdog_wq;
	struct work_struct hdmi_audio_wq;
	int timer_available;

#ifdef OSPM_STAT
	unsigned char graphics_state;
	unsigned long gfx_on_time;
	unsigned long gfx_off_time;
	unsigned long gfx_last_mode_change;
	unsigned long gfx_on_cnt;
	unsigned long gfx_off_cnt;
#endif
	uint32_t apm_reg;
	uint16_t apm_base;

	/*
	 * Used for modifying backlight from
	 * xrandr -- consider removing and using HAL instead
	 */
	struct intel_scu_ipc_dev *scu;
	struct backlight_device *backlight_device;
	struct drm_property *backlight_property;
	bool backlight_enabled;
	int backlight_level;
	uint32_t blc_adj1;
	uint32_t blc_adj2;

	/* DPST and Hotplug state */
	struct dpst_state *psb_dpst_state;
	pfn_vsync_handler psb_vsync_handler;

	bool b_dsr_enable_config;
	bool b_dsr_enable;
	bool b_dsr_enable_status;
	bool b_async_flip_enable;
	bool dsr_fb_update_done_0;
	bool dsr_fb_update_done_2;
	uint32_t dsr_fb_update;
	uint32_t dsr_idle_count;
	bool b_is_in_idle;
	bool dsr_enable;
	void (*exit_idle)(struct drm_device *dev, u32 update_src, void *p_surfaceAddr, bool check_hw_on_only);
	bool b_vblank_enable;
	int (*async_flip_update_fb)(struct drm_device *dev, int pipe);
	int (*async_check_fifo_empty)(struct drm_device *dev);

	bool dsi_device_ready;
	bool um_start;

	uint32_t tmds_clock_khz;
	//had_event_call_back mdfld_had_event_callbacks;
	struct snd_intel_had_interface *had_interface;
	void *had_pvt_data;

	uint32_t hdmi_audio_interrupt_mask;

	struct mdfld_dsi_encoder *encoder0;
	struct mdfld_dsi_encoder *encoder2;

	/*psb fb dev*/
	void *fbdev;
	/* read register value through sysfs. */
	int count;
	char *buf;

	/*for HDMI flip chain*/
#define DRM_PSB_HDMI_FLIP_ARRAY_SIZE 4
	void *flip_array[DRM_PSB_HDMI_FLIP_ARRAY_SIZE];
	unsigned int addr_array[DRM_PSB_HDMI_FLIP_ARRAY_SIZE];
	unsigned int flip_valid_size;
	unsigned int flip_head;
	unsigned int flip_tail;
	unsigned int flip_inited;
	unsigned int head_fliped;
	spinlock_t flip_lock;

	/*hdmi connected status */
	bool bhdmiconnected;
	bool dpms_on_off;
	bool bhdmi_enable;
	struct workqueue_struct *hpd_detect;
	pfn_screen_event_handler pvr_screen_event_handler;
	struct mutex dpms_mutex;

	/* fix Lock screen flip in resume issue */
	unsigned long init_screen_start;
	unsigned long init_screen_offset;
	unsigned long init_screen_size;
	unsigned long init_screen_stride;

	/* gamma and csc setting lock*/
	struct mutex gamma_csc_lock;
	/* overlay setting lock*/
	struct mutex overlay_lock;
	resource_size_t fb_base;

	/* indicate whether IED session is active */
	/* Maximum one active IED session at any given time */
	bool ied_enabled;
	/* indicate which source sets ied_enabled flag */
	struct file *ied_context;
	unsigned long long vsync_te_irq_ts[PSB_NUM_PIPE];
	unsigned long long vsync_te_worker_ts[PSB_NUM_PIPE];
	unsigned long long vsync_te_trouble_ts;
	bool  vsync_te_working[PSB_NUM_PIPE];
	atomic_t mipi_flip_abnormal;
	struct gpu_pvr_ops * pvr_ops;

	int overlay_buf_index;
	struct overlay_ctrl_blk *ov_ctrl_blk;
	struct mutex ov_ctrl_lock;

	struct drm_fb_helper *fb_helper;

	struct platform_panel_info panel_info;
	bool dispstatus;

	u32 pipeconf[3];
	u32 dspcntr[3];

	int mdfld_panel_id;

	bool dplla_96mhz;	/* DPLL data from the VBT */

	struct {
		int rate;
		int lanes;
		int preemphasis;
		int vswing;

		bool initialized;
		bool support;
		int bpp;
		struct edp_power_seq pps;
	} edp;
	uint8_t panel_type;
};

static inline struct drm_psb_private *to_drm_psb_private(struct drm_device *dev)
{
	return container_of(dev, struct drm_psb_private, dev);
}

/* Operations for each board type */
struct psb_ops {
	const char *name;
	int pipes;		/* Number of output pipes */
	int crtcs;		/* Number of CRTCs */
	int sgx_offset;		/* Base offset of SGX device */
	int hdmi_mask;		/* Mask of HDMI CRTCs */
	int lvds_mask;		/* Mask of LVDS CRTCs */
	int sdvo_mask;		/* Mask of SDVO CRTCs */
	int cursor_needs_phys;  /* If cursor base reg need physical address */

	/* Sub functions */
	struct drm_crtc_helper_funcs const *crtc_helper;
	const struct gma_clock_funcs *clock_funcs;

	/* Setup hooks */
	int (*chip_setup)(struct drm_device *dev);
	void (*chip_teardown)(struct drm_device *dev);
	/* Optional helper caller after modeset */
	void (*errata)(struct drm_device *dev);

	/* Display management hooks */
	int (*output_init)(struct drm_device *dev);
	int (*hotplug)(struct drm_device *dev);
	void (*hotplug_enable)(struct drm_device *dev, bool on);
	/* Power management hooks */
	void (*init_pm)(struct drm_device *dev);
	int (*save_regs)(struct drm_device *dev);
	int (*restore_regs)(struct drm_device *dev);
	void (*save_crtc)(struct drm_crtc *crtc);
	void (*restore_crtc)(struct drm_crtc *crtc);
	int (*power_up)(struct drm_device *dev);
	int (*power_down)(struct drm_device *dev);
	void (*update_wm)(struct drm_device *dev, struct drm_crtc *crtc);
	void (*disable_sr)(struct drm_device *dev);

	void (*lvds_bl_power)(struct drm_device *dev, bool on);

	/* Backlight */
	int (*backlight_init)(struct drm_device *dev);
	void (*backlight_set)(struct drm_device *dev, int level);
	int (*backlight_get)(struct drm_device *dev);
	const char *backlight_name;

	int i2c_bus;		/* I2C bus identifier for Moorestown */
};



extern int drm_crtc_probe_output_modes(struct drm_device *dev, int, int);
extern int drm_pick_crtcs(struct drm_device *dev);

/* psb_irq.c */
extern void psb_irq_uninstall_islands(struct drm_device *dev, int hw_islands);
extern int psb_vblank_wait2(struct drm_device *dev, unsigned int *sequence);
extern int psb_vblank_wait(struct drm_device *dev, unsigned int *sequence);

void mid_enable_pipe_event(struct drm_psb_private *dev_priv, int pipe);

extern u32 psb_get_vblank_counter(struct drm_crtc *crtc);

extern int mdfld_enable_te(struct drm_device *dev, int pipe);
extern void mdfld_disable_te(struct drm_device *dev, int pipe);
extern int mid_irq_enable_hdmi_audio(struct drm_device *dev);
extern int mid_irq_disable_hdmi_audio(struct drm_device *dev);
extern void psb_te_timer_func(unsigned long data);
extern void mdfld_te_handler_work(struct work_struct *te_work);
extern enum hrtimer_restart intel_dbi_fifo_query_timer_func
				(struct hrtimer *timer);

extern void mdfld_vsync_event_work(struct work_struct *work);
extern u32 intel_vblank_count(struct drm_device *dev, int pipe);

/*
 * intel_opregion.c
 */
extern int gma_intel_opregion_init(struct drm_device *dev);
extern int gma_intel_opregion_exit(struct drm_device *dev);

/* framebuffer.c */
extern int psbfb_probed(struct drm_device *dev);
extern int psbfb_remove(struct drm_device *dev,
			struct drm_framebuffer *fb);

/* psb_lid.c */
extern void psb_lid_timer_init(struct drm_psb_private *dev_priv);
extern void psb_lid_timer_takedown(struct drm_psb_private *dev_priv);

/* modesetting */
extern void psb_modeset_init(struct drm_device *dev);
extern void psb_modeset_cleanup(struct drm_device *dev);
extern int psb_fbdev_init(struct drm_device *dev);
/* ied session */
extern void psb_cleanup_ied_session(struct drm_psb_private *dev_priv,
			      struct file *filp);

/* backlight.c */
int gma_backlight_init(struct drm_device *dev);
void gma_backlight_exit(struct drm_device *dev);
void gma_backlight_disable(struct drm_device *dev);
void gma_backlight_enable(struct drm_device *dev);
void gma_backlight_set(struct drm_device *dev, int v);

/* oaktrail_crtc.c */
extern const struct drm_crtc_helper_funcs oaktrail_helper_funcs;

/* oaktrail_lvds.c */
extern void oaktrail_lvds_init(struct drm_device *dev,
                   struct psb_intel_mode_device *mode_dev);

/* mrst_crtc.c */
extern const struct drm_crtc_helper_funcs mrst_helper_funcs;

/* psb_intel_display.c */
extern const struct drm_crtc_helper_funcs psb_intel_helper_funcs;

/* psb_intel_lvds.c */
extern const struct drm_connector_helper_funcs
					psb_intel_lvds_connector_helper_funcs;
extern const struct drm_connector_funcs psb_intel_lvds_connector_funcs;

/* gem.c */
extern int psb_gem_dumb_create(struct drm_file *file, struct drm_device *dev,
			struct drm_mode_create_dumb *args);

/* psb_device.c */
extern const struct psb_ops psb_chip_ops;

/* oaktrail_device.c */
extern const struct psb_ops oaktrail_chip_ops;

/* mdfld_device.c */
extern const struct psb_ops mdfld_chip_ops;

/* cdv_device.c */
extern const struct psb_ops cdv_chip_ops;

/*use case control*/
#define PSB_SUSPEND_ENABLE     (1 << 0)
#define PSB_BRIGHTNESS_ENABLE  (1 << 1)
#define PSB_ESD_ENABLE         (1 << 2)
#define PSB_DPMS_ENABLE        (1 << 3)
#define PSB_DSR_ENABLE         (1 << 4)
#define PSB_VSYNC_OFF_ENABLE   (1 << 5)
#define PSB_ALL_UC_ENABLE      0x3F

extern int drm_psb_no_fb;
extern int drm_topaz_sbuswa;

/* Utilities */
static inline u32 MRST_MSG_READ32(int domain, uint port, uint offset)
{
	int mcr = (0xD0<<24) | (port << 16) | (offset << 8);
	uint32_t ret_val = 0;
	struct pci_dev *pci_root = pci_get_domain_bus_and_slot(domain, 0, 0);
	pci_write_config_dword(pci_root, 0xD0, mcr);
	pci_read_config_dword(pci_root, 0xD4, &ret_val);
	pci_dev_put(pci_root);
	return ret_val;
}
static inline void MRST_MSG_WRITE32(int domain, uint port, uint offset,
				    u32 value)
{
	int mcr = (0xE0<<24) | (port << 16) | (offset << 8) | 0xF0;
	struct pci_dev *pci_root = pci_get_domain_bus_and_slot(domain, 0, 0);
	pci_write_config_dword(pci_root, 0xD4, value);
	pci_write_config_dword(pci_root, 0xD0, mcr);
	pci_dev_put(pci_root);
}
static inline u32 MDFLD_MSG_READ32(int domain, uint port, uint offset)
{
	int mcr = (0x10<<24) | (port << 16) | (offset << 8);
	uint32_t ret_val = 0;
	struct pci_dev *pci_root = pci_get_domain_bus_and_slot(domain, 0, 0);
	pci_write_config_dword(pci_root, 0xD0, mcr);
	pci_read_config_dword(pci_root, 0xD4, &ret_val);
	pci_dev_put(pci_root);
	return ret_val;
}
static inline void MDFLD_MSG_WRITE32(int domain, uint port, uint offset,
				     u32 value)
{
	int mcr = (0x11<<24) | (port << 16) | (offset << 8) | 0xF0;
	struct pci_dev *pci_root = pci_get_domain_bus_and_slot(domain, 0, 0);
	pci_write_config_dword(pci_root, 0xD4, value);
	pci_write_config_dword(pci_root, 0xD0, mcr);
	pci_dev_put(pci_root);
}

static inline uint32_t REGISTER_READ(struct drm_device *dev, uint32_t reg)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	return ioread32(dev_priv->vdc_reg + reg);
}

static inline uint32_t REGISTER_READ_AUX(struct drm_device *dev, uint32_t reg)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	return ioread32(dev_priv->aux_reg + reg);
}

#define REG_READ(reg)	       REGISTER_READ(dev, (reg))
#define REG_READ_AUX(reg)      REGISTER_READ_AUX(dev, (reg))

/* Useful for post reads */
static inline uint32_t REGISTER_READ_WITH_AUX(struct drm_device *dev,
					      uint32_t reg, int aux)
{
	uint32_t val;

	if (aux)
		val = REG_READ_AUX(reg);
	else
		val = REG_READ(reg);

	return val;
}

#define REG_READ_WITH_AUX(reg, aux) REGISTER_READ_WITH_AUX(dev, (reg), (aux))

static inline void REGISTER_WRITE(struct drm_device *dev, uint32_t reg,
				  uint32_t val)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	iowrite32((val), dev_priv->vdc_reg + (reg));
}

static inline void REGISTER_WRITE_AUX(struct drm_device *dev, uint32_t reg,
				      uint32_t val)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	iowrite32((val), dev_priv->aux_reg + (reg));
}

#define REG_WRITE(reg, val)	REGISTER_WRITE(dev, (reg), (val))
#define REG_WRITE_AUX(reg, val)	REGISTER_WRITE_AUX(dev, (reg), (val))

static inline void REGISTER_WRITE_WITH_AUX(struct drm_device *dev, uint32_t reg,
				      uint32_t val, int aux)
{
	if (aux)
		REG_WRITE_AUX(reg, val);
	else
		REG_WRITE(reg, val);
}

#define REG_WRITE_WITH_AUX(reg, val, aux) REGISTER_WRITE_WITH_AUX(dev, (reg), (val), (aux))

static inline void REGISTER_WRITE16(struct drm_device *dev,
					uint32_t reg, uint32_t val)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	iowrite16((val), dev_priv->vdc_reg + (reg));
}

#define REG_WRITE16(reg, val)	  REGISTER_WRITE16(dev, (reg), (val))

static inline void REGISTER_WRITE8(struct drm_device *dev,
				       uint32_t reg, uint32_t val)
{
	struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
	iowrite8((val), dev_priv->vdc_reg + (reg));
}

#define REG_WRITE8(reg, val)		REGISTER_WRITE8(dev, (reg), (val))

#define PSB_WVDC32(_val, _offs)		iowrite32(_val, dev_priv->vdc_reg + (_offs))
#define PSB_RVDC32(_offs)		ioread32(dev_priv->vdc_reg + (_offs))

static inline uint32_t SGX_REGISTER_READ(struct drm_device *dev, uint32_t reg)
{
       struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
       return ioread32(dev_priv->sgx_reg + (reg));
}

#define SGX_REG_READ(reg)             SGX_REGISTER_READ(dev, (reg))
static inline void SGX_REGISTER_WRITE(struct drm_device *dev, uint32_t reg,
						uint32_t val)
{
       struct drm_psb_private *dev_priv = to_drm_psb_private(dev);
       iowrite32((val), dev_priv->sgx_reg + (reg));
}

#define SGX_REG_WRITE(reg, val)        SGX_REGISTER_WRITE(dev, (reg), (val))

#define PSB_RSGX32(_offs)		ioread32(dev_priv->sgx_reg + (_offs))
#define PSB_WSGX32(_val, _offs)		iowrite32(_val, dev_priv->sgx_reg + (_offs))

#define OSPM_PUNIT_PORT		0x04

#define OSPM_APMBA		0x7a
/* APM_STS register:
 * 1:0- GPS, 3:2 - VDPS, 5:4 -VEPS, 7:6 -GL3, 9:8 -ISP, 11:10 - IPH */
#define APM_STS			0x04
#define APM_STS_VDPS_SHIFT	2

#define APM_STS_D0		0x0
#define APM_STS_D1		0x1
#define APM_STS_D2		0x2
#define APM_STS_D3		0x3
#define APM_STS_VDPS_MASK	0xC

/* OSPM_PM_SSS register:
 * 1:0 - GFX, 3:2 - DPA, 5:4 - VED, 7:6 - VEC, 9:8 - GL3,
 * 11:10 - IUNIT, 13:12 - Iunit PHY Cache
 * 15:14 - Display B, 17:16 - Display C, 19:18 - MIPI
 */
#define OSPM_OSPMBA		0x78
#define OSPM_PM_SSS		0x30

#define MFLD_MSVDX_FABRIC_DEBUG 0
#define MSVDX_REG_DUMP 0

#define PSB_ALPL(_val, _base)			\
  (((_val) >> (_base ## _ALIGNSHIFT)) << (_base ## _SHIFT))
#define PSB_ALPLM(_val, _base)			\
  ((((_val) >> (_base ## _ALIGNSHIFT)) << (_base ## _SHIFT)) & (_base ## _MASK))

#define IS_POULSBO(drm) 0  /* ((to_pci_dev((drm)->dev)->device == 0x8108) || \
			       (to_pci_dev((drm)->dev)->device == 0x8109)) */

/* Will revisit it after CLOVER TRAIL PO. */
/* pciid: CLV A0 = 0X8C7, CLV B0 = 0X8C8-0X8CB, CLV+ A0/B0 0X8CC-0X8CF.*/
#define IS_MDFLD_OLD(drm) ((to_pci_dev((drm)->dev)->device & 0xfff8) == 0x0130)
#define IS_CTP(drm) (((to_pci_dev((drm)->dev)->device & 0xffff) == 0x08c0) ||	\
		     ((to_pci_dev((drm)->dev)->device & 0xffff) == 0x08c7) ||  \
		     ((to_pci_dev((drm)->dev)->device & 0xffff) == 0x08c8))

#define IS_MRFL(drm) ((to_pci_dev((drm)->dev)->device & 0xFFFC) == 0x1180)
#define IS_MRFLD(drm) ((to_pci_dev((drm)->dev)->device & 0xfff8) == 0x1180)

#define IS_CTP_NEED_WA(drm) ((to_pci_dev((drm)->dev)->device & 0xffff) == 0x08c8)
#define HAS_DISPLAY_IED_CNTRL(drm) ((to_pci_dev((drm)->dev)->device & 0xffff) == 0x08c8)

#define IS_MDFLD(dev) (IS_CTP(dev) || IS_MDFLD_OLD(dev))
#define IS_MID(dev) (IS_MRST(dev) || IS_MDFLD(dev))

#define IS_TOPAZ(drm) ((IS_MRST(drm) && ((to_pci_dev((drm)->dev)->device & 0xfffc) != PCI_ID_TOPAZ_DISABLED)) || IS_MDFLD(dev))

#define IS_MSVDX_MEM_TILE(dev) ((IS_MRFL(dev)) || (IS_CTP(dev)))

extern int drm_psb_ospm;
extern int drm_psb_cpurelax;
extern int drm_psb_udelaydivider;
extern int drm_psb_gl3_enable;
extern int drm_psb_topaz_clockgating;

extern char HDMI_EDID[20];
extern int hdmi_state;
extern void psb_flip_abnormal_debug_info(struct drm_device *dev);
extern struct drm_device *g_drm_dev;
/*
 * set cpu_relax = 1 in sysfs to use cpu_relax instead of udelay bysy loop
 * set udelay_divider to reduce the udelay values,e.g.= 10, reduce 10 times
 */
#define PSB_UDELAY(usec)                        \
do {                                            \
	if (drm_psb_cpurelax == 0)              \
		DRM_UDELAY(usec / drm_psb_udelaydivider);   \
	else                                    \
		cpu_relax();                    \
} while (0)

#endif
